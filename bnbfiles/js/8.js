! function() {
    var e = webpackJsonp([8], { 3096: function(e, t, a) {
            function r(e) { d.default.logEvent({ event_name: "small_search_modal", event_data: { trebuchet: v.default.getBootstrap("react_sm_search_modal_v3"), erf: m.default.findTreatment("react_sm_search_modal_v3"), treatment: m.default.findTreatment("react_sm_search_modal_v3"), page: f.default.get("controller_action_pair"), pageSource: e, sub_event: "searchModalOpen" } }) }
            Object.defineProperty(t, "__esModule", { value: !0 });
            var l = a(0),
                n = babelHelpers.interopRequireDefault(l),
                o = a(16),
                u = babelHelpers.interopRequireDefault(o),
                i = a(56),
                s = babelHelpers.interopRequireDefault(i),
                p = a(15),
                f = babelHelpers.interopRequireDefault(p),
                c = a(11),
                d = babelHelpers.interopRequireDefault(c),
                b = a(53),
                m = babelHelpers.interopRequireDefault(b),
                _ = a(222),
                h = a(44),
                v = babelHelpers.interopRequireDefault(h),
                H = a(6987),
                R = babelHelpers.interopRequireDefault(H),
                D = a(238),
                q = babelHelpers.interopRequireDefault(D),
                g = function(e) {
                    function t() {
                        return babelHelpers.classCallCheck(this, t), babelHelpers.possibleConstructorReturn(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments)) }
                    return babelHelpers.inherits(t, e), babelHelpers.createClass(t, [{ key: "componentDidMount", value: function() {
                            function e() {
                                var e = "(max-width: " + String(_.NAVIGATION_BREAKPOINT) + "px)",
                                    t = window.matchMedia && window.matchMedia(e).matches;
                                t && (this.modal = new R.default({ el: (0, u.default)(this.el) }), (0, q.default)().open_search_modal && s.default.emit("search-modal:open", "auto_open")), s.default.on("search-modal:open", r) }
                            return e }() }, { key: "componentWillUnmount", value: function() {
                            function e() { s.default.removeListener("search-modal:open", r) }
                            return e }() }, { key: "render", value: function() {
                            function e() {
                                var e = this;
                                return n.default.createElement("div", { ref: function() {
                                        function t(t) {
                                            return e.el = t }
                                        return t }() }) }
                            return e }() }]), t }(n.default.Component);
            t.default = g, e.exports = t.default } }); "object" == typeof module && (module.exports = e) }();




		// function r(e) {
  //               var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
  //                   n = arguments[2],
  //                   r = o.default.get;
  //               return u.default.getBootstrap("p1_use_fetch") && (r = o.default.getWithFetch), new Promise(function(i, o) {
  //                   var a = Date.now();
  //                   s.default.logEvent({ operation: "api_fetch", apiPath: e, options: t, datadog_key: "p1_api_fetch", datadog_count: 1, datadog_tags: "api_path:" + String(e) }), r(e, t, Object.assign({}, n, { _intents: "p1" })).then(function(n) {
  //                       var r = Date.now() - a;
  //                       return n ? (s.default.logEvent({ operation: "api_fetch_success", run_time: r, apiPath: e, options: t, datadog_key: "p1_api_fetch_success", datadog_count: 1, datadog_tags: "api_path:" + String(e) + ",rounded_run_time:" + 500 * Math.round(r / 500) }), i(n)) : Promise.reject() }).catch(function(n) {
  //                       var r = Date.now() - a;
  //                       s.default.logEvent({ operation: "api_fetch_fail", run_time: r, error: n, apiPath: e, options: t, datadog_key: "p1_api_fetch_fail", datadog_count: 1, datadog_tags: "api_path:" + String(e) + ",rounded_run_time:" + 500 * Math.round(r / 500) }), o(n) }) }) 
  //           }