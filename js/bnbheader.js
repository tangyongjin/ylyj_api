$(document).ready(function(){

    var signup_click = $('#header_signup'),
    signin_click = $('#header_signin');

    var main = $('.main');
    var box = $('.box');
    var cover = $('.cover'),
    signupcover = $('.signup-cover'),
    signincover = $('.signin-cover');
    var signup=$('.btn-phone'),
    phonesignup=$('.phone-signup'),
    phonesignin=$('.phone-signin');

    var inputMobile = $('.input-mobile'),
    tryMobile = $('.input-mobile-signin'),
    checkMoblie = $('.phone-check-signup'),
    checkTryMoblie = $('.phone-check-signin'),
    inputPassword = $('.input-password'),
    tryPassword = $('.input-password-signin'),
    checkPassword = $('.password-check-signup'),
    checkTryPassword = $('.password-check-signin'),
    randomNum = $('.random_number')
    btnSendCheckNum = $('.btn-send-check-num'),
    btnSignup = $('#btn-signup'),
    btnsignin = $('#btn-signin');


    btnsignin.on('click',function() {
        if(!isPhoneNo(tryMobile.val())){
            checkTryMoblie.css('display','block');
            checkTryMoblie.html('请检查输入的手机号')
        }
        if (!isPwdNo(tryPassword.val())){
            checkTryPassword.css('display','block');
            checkTryPassword.html('请检查输入的手机号')
        }
        checkTryMoblie.css('display','none');
        checkTryPassword.css('display','none');
        var url='http://47.92.72.19/user/login';
        $.ajax({
            cache: true,
            type: "POST",
            url:url,
            data:$('#user_login').serialize(),
            async: false,
            error: function(request) {
                console.log('error')
            },
            success: function(data) {
                switch(data['code']){
                    case 0:
                    checkTryMoblie.css('display','block');
                    checkTryMoblie.html('登录成功')
                    window.location.reload();
                    break;
                    case 1:
                    checkTryMoblie.css('display','block');
                    checkTryMoblie.html('用户名或密码错误')
                    break;
                    case 2:
                    checkTryMoblie.css('display','block');
                    checkTryMoblie.html('用户名或密码错误')
                    break;
                }
                console.log('success')
            }
        });
    });
    inputMobile.on('blur',function(){
                    // console.log(isPhoneNo(inputMobile.val()))
                    if (isPhoneNo(inputMobile.val())){
                        checkMoblie.css('display','none');
                        var url='http://47.92.72.19/user/checkmobile';
                        $.ajax({
                            cache: true,
                            type: "POST",
                            url:url,
                            data:$('#user_new').serialize(),
                            async: false,
                            error: function(request) {
                                console.log('error')
                            },
                            success: function(data) {
                                if(data['exists']==0){
                                    checkMoblie.css('display','block');
                                    checkMoblie.html('手机号可以使用');
                                    btnSignup.removeAttr("disabled");
                                    btnSendCheckNum.removeAttr("disabled");                            
                                }else{
                                    checkMoblie.css('display','block');
                                    checkMoblie.html('手机号已被注册');
                                    checkMoblie.attr({"disabled":"disabled"});
                                    btnSendCheckNum.attr({"disabled":"disabled"});
                                }
                            }
                        });
                    }else{
                        checkMoblie.css('display','block');
                        checkMoblie.html('手机号格式不正确');
                    }
                });
    inputPassword.on('blur',function(){
        if(isPwdNo(inputPassword.val())){
            checkPassword.css('display','none');
            btnSignup.removeAttr("disabled");
            console.log('ok')
        }else{
            checkPassword.css('display','block');
            checkPassword.html('密码格式不正确，请输入6~12位字母和数字')
            btnSignup.attr({"disabled":"disabled"});
            console.log('no')
        }
    });
    btnSendCheckNum.on('click',function(){
        if (inputMobile.val()) {
            time(this);
            randomNum.val(randomString(10));
            var url='http://47.92.72.19/user/sendVerificationCode';
            $.ajax({
                cache: true,
                type: "POST",
                url:url,
                data:$('#user_new').serialize(),
                async: false,
                error: function(request) {
                    console.log('error')
                },
                success: function(data) {
                    console.log('ok')
                }
            });
        }else{
            checkMoblie.css('display','block');
            checkMoblie.html('请输入手机号');
        }

    });
    btnSignup.on('click',function() {
        if(isPwdNo(inputPassword.val())){
            checkPassword.css('display','none');
            btnSignup.removeAttr("disabled");
            console.log('ok')
            var url='http://47.92.72.19/user/checkAndRegister';
            $.ajax({
                cache: true,
                type: "POST",
                url:url,
                data:$('#user_new').serialize(),
                async: false,
                error: function(request) {
                    console.log('error')
                },
                success: function(data) {
                    switch(data['code']){
                        case 0:
                        checkTryMoblie.css('display','block');
                        checkTryMoblie.html('注册成功')
                        window.location.reload();
                        break;
                        case 1:
                        checkTryMoblie.css('display','block');
                        checkTryMoblie.html('遇到了未知的错误');
                        break;
                        case 2:
                        checkTryMoblie.css('display','block');
                        checkTryMoblie.html('验证码错误');
                        break;
                    }
                    console.log(data)
                }
            });
        }else{
            checkPassword.css('display','block');
            checkPassword.html('密码格式不正确，请输入6~12位字母和数字')
            btnSignup.attr({"disabled":"disabled"});
            console.log('no')
        }        
    });

    signup_click.on('click', function () {
        // e.stopPropagation();
        main.css('display','block');
    });


    $('#abcd123').on('click', function (e) {
       e.stopPropagation();
       alert(123)
   });
    signin_click.on('click', function () {
        // e.stopPropagation();
        phonesignin.css('display','block');
    });
    signup.on('click',function(){
        // e.stopPropagation();
        main.css('display','none');
        phonesignin.css('display','none');
        phonesignup.css('display','block');
    });
    cover.on('click',function(){
        // e.stopPropagation();
        main.css('display','none');
    });
    signincover.on('click',function(){
        // e.stopPropagation();
        phonesignin.css('display','none');
    });
    signupcover.on('click',function(){
        // e.stopPropagation();
        phonesignup.css('display','none');
    });
    function isPhoneNo(phone) { 
        var pattern = /^1[34578]\d{9}$/; 
        return pattern.test(phone); 
    }
    function isPwdNo(password){
        var pattern = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/;
        return pattern.test(password);
    }
    var wait = 5;
    function time(o) {
        if (wait == 0) {
            o.removeAttribute("disabled");
            o.value="免费获取验证码";
            wait = 5;
        } else {
            o.setAttribute("disabled", true);
            o.value=wait+"秒后可以重新发送";
            wait--;
            setTimeout(function() {
                time(o)
            },
            1000);
        }
    }
    function randomString(len) {
        　　len = len || 32;
        　　var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    
        　　var maxPos = $chars.length;
        　　var pwd = '';
        　　for (i = 0; i < len; i++) {
            　　　　pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        　　}
        　　return pwd;
    }


    let personalImg = $('#header_img'),
            personalMenu = $('.container_personal_info'),
            contentCover = $('.content_cover');
    personalImg.click(function(){personalMenu.css('display','block');})
    contentCover.click(function(){personalMenu.css('display','none');})
});