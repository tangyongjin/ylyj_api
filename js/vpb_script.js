/*********************************************************************
* This script has been released with the aim that it will be useful.
* Written by Vasplus Programming Blog
* Website: www.vasplus.info
* Email: info@vasplus.info
* All Copy Rights Reserved by Vasplus Programming Blog
***********************************************************************/


//This function is called automatically upon page load

function centerModals(){

  $('.modal').each(function(i){
    var $clone = $(this).clone().css('display', 'block').appendTo('body');
    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
    top = top > 0 ? top : 0;
    $clone.remove();
    $(this).find('.modal-content').css("margin-top", top);
  });
}   

//This function displays the login box when called
function vpb_show_login_box()
{
 
	$("#vpb_pop_up_background").css({
		"opacity": "0.8"
	});
 
	
	$("#vpb_pop_up_background").fadeIn("slow");
	$("#vpb_login_pop_up_box").fadeIn('fast');
	window.scroll(0,0);


     for (var i = 0; i < arguments.length; i++) {
         if(i==0){   $('#login_eid').val(arguments[i]);}
         if(i==1){   $('#login_pwd').val(arguments[i]);}
     }
    // $("#login_btn").click()
}


function vpb_show_register_box(version)
{
$("#myModal").attr("version",version);
$("#field_check_info").html('');
$('#myModal').modal({ keyboard: false,backdrop:'static' })  
$('#myModal').modal('show') 
}

 

 