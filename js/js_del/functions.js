(function($) {
    $(document).ready(function() {
        $(function() {
            $('[data-toggle="popover"]').popover()
        })

        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });

        $('.scrollup').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
 

        $('#login_btn').click(function(e) {
            e.preventDefault();
            login();
        });


        $('#register_trigger').click(function(e) {
            version = $("#register_trigger").attr("version");
            vpb_show_register_box(version);
             e.preventDefault();

            if (version == 'for_enterprise') {
                check_and_reg();
            } else {
                eid = $("#register_trigger").attr("eid");
                register_for_developer(eid);
            }
        });

        $("#vpb_pop_up_background").click(function() {
            $("#vpb_login_pop_up_box").hide(); //Hides the login box when clicked outside the form
            $("#vpb_pop_up_background").fadeOut("slow");
        });

        $('#reg_close_id').click(function() {
                version = $("#myModal").attr("version");
                console.log(version);

                if ($("#reg_success_div").length) 
                {
                    if(version=='for_enterprise'){
                        var regcfg = getRegisterField();
                        vpb_show_login_box(regcfg.eid, regcfg.pwd1);
                    }else
                    {
                      location.reload();    
                    }


                }
                else
                {

                }

            }
        )
    });

})(jQuery);



jQuery.fn.shake = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        $(this).css({
            position: "relative"
        });
        for (var x = 1; x <= intShakes; x++) {
            $(this).animate({
                left: (intDistance * -1)
            }, (((intDuration / intShakes) / 4))).animate({
                left: intDistance
            }, ((intDuration / intShakes) / 2)).animate({
                left: 0
            }, (((intDuration / intShakes) / 4)));
        }
    });
    return this;
};