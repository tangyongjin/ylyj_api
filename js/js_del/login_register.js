if (!console) {
  var console = {};
  console.log = function() {}
}


function check_pwd(regcfg){

  pwd_check=true;
  console.log(regcfg);
  
  if (regcfg.pwd1 == regcfg.pwd2) {
    var pwd_check = true;
  } else {
    var pwd_check = false;
  }
  if (regcfg.pwd1.length < 6) {
    pwd_check = false;
  }
   return pwd_check;
}


function check_eid_format(regcfg){
  eid_check=true;
  var trimed_eid = $.trim(regcfg.eid);
  if(regcfg.eid){
      var eid_check = /^[\da-zA-Z,_]+$/.test(trimed_eid);
  }
  else
  {
        eid_check = false;
  }

   if (trimed_eid.length < 3) {
        eid_check = false;
    }
  return eid_check;
}

function check_eid_forbidden(regcfg){
  eid_check=true;
  var trimed_eid = $.trim(regcfg.eid);
  var eid_forbidden=new Array('nanx_master','nanx_template','admin','wordpress', 'administrator','root','mysql','test','PUBLIC','ultrax','tmp','information_schema','standx');
  if ($.inArray(trimed_eid,eid_forbidden)>=0)
      {
        eid_check=false;
        console.log('inarray')
      }
   return eid_check;
} 

function check_email(regcfg){

  email_check=true;
  if (regcfg.email){
  var email = regcfg.email;
  var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  email_check = emailRegexStr.test(email);
  }else
  {
      email_check = false;
  }
  return email_check;
}


function check_field(regcfg) {
 
  regcfg.eid_format_msg=i18n.eid_format;
  regcfg.eid_forbidden_msg=i18n.eid_forbidden;
  regcfg.email_msg=i18n.email_format;
  regcfg.pwd_msg=i18n.pwd_length_6;
  regcfg.agree_msg=i18n.user_agreement_not_checked;

  eid_check_format=check_eid_format(regcfg);
  eid_check_forbidden=check_eid_forbidden(regcfg);
  email_check=check_email(regcfg);
  pwd_check=check_pwd(regcfg);
  
  console.log(regcfg);

  regcfg.success=regcfg.agree&&email_check && eid_check_format &&eid_check_forbidden && pwd_check;
  
  regcfg.eid_check_format=eid_check_format;
  regcfg.eid_check_forbidden=eid_check_forbidden;
  regcfg.email_check=email_check;
  regcfg.pwd_check=pwd_check;
  
  return regcfg;
}


function getRegisterField()
{
  var regcfg={};
  if($('#register_entprise_id').length>0){regcfg.eid=($('#register_entprise_id'))[0].value}
  if($('#register_entprise_name').length>0){regcfg.ename=($('#register_entprise_name'))[0].value}
  if($('#register_pwd1').length>0){regcfg.pwd1=($('#register_pwd1'))[0].value}
  if($('#register_pwd2').length>0){regcfg.pwd2=($('#register_pwd2'))[0].value}
  if($('#register_pwd1').length>0){regcfg.password=($('#register_pwd1'))[0].value}
  if($('#register_email').length>0){regcfg.email=($('#register_email'))[0].value}

  if(  $("#check_agree").is(":checked") ){regcfg.agree=true}else{regcfg.agree=false}
 


  regcfg.version='for_enterprise';
  return regcfg;
}




function check_and_reg() {
  var regcfg= getRegisterField();
  var check_result = check_field(regcfg);
  if (check_result.success == true) 
  {
    
    register_user(regcfg);
  } 
  else 
  {
    $('#modal-content').shake(2, 13, 250); //
    $("#deploy_title").html(i18n.check_field);
    var errinfo = '<table  class="thin-table">';
    errinfo += check_result.eid_check_format ? '' : '<tr><td>' + check_result.eid_format_msg + '</td></tr>';
    errinfo += check_result.eid_check_forbidden ? '' : '<tr><td>' + check_result.eid_forbidden_msg + '</td></tr>';
    errinfo += check_result.email_check ? '' : '<tr><td>' + check_result.email_msg + '</td></tr>';
    errinfo += check_result.pwd_check ? '' : '<tr><td>' + check_result.pwd_msg + '</td></tr>';
    errinfo += check_result.agree ? '' : '<tr><td>' + check_result.agree_msg + '</td></tr>';


    errinfo += '</table>';
    $("#field_check_info").html(errinfo);
    var img1=BASE_URL+'/img/attention.gif';
    $("#pic_holder").attr({src:img1})
 
  }
}


function register_for_developer(eid)
{
var regcfg={};
regcfg.eid=eid;
regcfg.version='for_developer';
register_user(regcfg);

}
 

  function startRequest(regcfg,status_task) {
    var eid=regcfg.eid
    var cfg = {
      eid:eid
    };
    
    $.ajax({
      type: "POST",
      url: BASE_URL + 'index.php/account/registerStatus',
      data: $.toJSON(cfg),
      contentType: "application/json",
      dataType: "json",
      success: function(data) {
        var backlog = data.log;
        $("#backend_log").html(backlog);
        if (data.num_rows == 8) 
        {
          console.log(8);
          clearInterval(status_task);
          $("#myModalLabel").css("backgroundImage", "none");
          $("#guide_info").show();
          var guide ='<div  id=reg_success_div >'+i18n.register_success+'.</br></div>';
          var eurl = '<a href=' + WEB_ROOT + eid + '>' + WEB_ROOT + eid + '</a>';
          
          if(regcfg.version=='for_developer'){regcfg.pwd1='******';
             content=i18n.dev_env_setted;
            }else
            {
              content=i18n.bbs_account_also_created;
            };

          guide += '<table  id="guide-table" class="thin-table">'+
          '<tr><td>'+i18n.eid+':</td><td>'+eid+'</td></tr>'+
          '<tr><td>'+i18n.login_url+':</td><td>' + eurl +'</td></tr>'+ 
          '<tr><td>'+i18n.login_name+':</td><td>'+eid+'</td></tr>'+
          '<tr><td>'+i18n.pwd+':</td><td>'+ regcfg.pwd1 +  '</td></tr>'+
          '</table>';
          
          $('#guide_info').html(guide);
          $('#guide-table').popover({title:i18n.tips_after_reg,content:content});
          $('#guide-table').popover('show');
        }
      }
    });
  }



function register_user(regcfg) {
   
   $('#reg_close_id').prop("disabled",true);
   $('html,body').css('cursor','wait');

   $("#deploy_title").html(i18n.deploying);
   var loadinggif=BASE_URL+'/img/loading.gif';
   $("#pic_holder").attr({src:loadinggif})

   $("#field_check_info").html('');
   var status_task = setInterval( function(){ startRequest(regcfg,status_task)},1000);
   $.ajax({
    type: "POST",
    url: BASE_URL + 'index.php/account/register',
    data: $.toJSON(regcfg),
    contentType: "application/json",
    dataType: "json",
    success: function(data) {
      if (data.result_code === 0) {
         $('#reg_close_id').prop("disabled",false);
            $('html,body').css('cursor','default');
       
      } else 
       {
        clearInterval(status_task);
        if (data.result_code === 1062) {
          $("#backend_log").html('');
          $('#guide_info').html('');
          $("#field_check_info").html('<table class="thin-table"><tr><td>'+i18n.eid_exists+'</td></tr></table>');
           $('#reg_close_id').prop("disabled",false);
           $('html,body').css('cursor','default');
          } else {
          $("#field_check_info").html('<table class="thin-table"><tr><td>'+i18n.unknow_err+':' + data.result_code + '</td></tr></table>');
        }
      }
    },
    error:function(){ 
     $('#reg_close_id').prop("disabled",false);
     $('html,body').css('cursor','default');
     console.log('register_user error ');
     clearInterval(status_task);
   }
  });
}



function login() {
  var eid = ($('#login_eid'))[0].value;
  var pwd = ($('#login_pwd'))[0].value;
  var login_obj = {
    'eid': eid,
    'password': pwd
  }

  $.ajax({
    type: "POST",
    url: BASE_URL + 'index.php/account/login',
    data: $.toJSON(login_obj),
    contentType: "application/json",
    dataType: "json",
    success: function(data) {
      if (data.code == 0) {
       window.location.replace(BASE_URL +  'index.php/home/dashboard/dashboard_guide');
      } else {
        $('#signup').shake(2, 13, 250); //
      }
    }
  });
}


function logout() {
  $.ajax({
    type: "POST",
    url: BASE_URL + 'index.php/account/logout',
    success: function(data) {
      location.reload(true);
    }
  });
}




function dashboard_pwd()
{

  var regcfg= getRegisterField();
  var check_result = check_pwd(regcfg);
  console.log(regcfg);
 
  if (!check_result)
  {
        $('#modal-content').html(regcfg.pwd_msg);
        $('#reset_pwd_Modal').modal('toggle');
        $('#reset_pwd_Modal').modal('show');
        return;   
  }


  var resetpwd_obj={'eid':regcfg.eid,'password':regcfg.pwd1};
  console.log(resetpwd_obj);
  $.ajax({
      type: "POST",
      url: BASE_URL + 'index.php/account/resetpwd',
      data: $.toJSON(resetpwd_obj),
      contentType: "application/json",
      dataType: "json",
      success: function(data) {
        if(data.result_code==0)
        {
          $("#updated").fadeIn();
          $("#updated").fadeOut(2000);
        }
      }
    });
}
 
 