$(document).ready(function(){
	let userfirstname=$('#user_first_name'),
		usersex=$('#user_sex'),
		userbirthday=$('#user_birthday'),
		nophonenumbers=$('.no-phone-numbers'),
		phonenumberstable=$('.phone-numbers-table'),
		havephonenumber=$('#have-phone-number'),
		encodephone=$('.edit-profile-confirmable-field__label'),
		addphonenumber=$('#add-phone-number'),
		phonenumber=$('#phone_number'),
		phonenumberverifywidget=$('.phone-number-verify-widget'),
		sendaddphonesms=$('#send-addphone-sms'),
		cancelsendaddphonesms=$('#cancel-send-addphone-sms'),
		addphonerandomnumber=$('#addphone_random_number'),
		phonenumberverification=$('#phone_number_verification'),
		userlocation=$('#user_profile_info_current_city'),
		userabout=$('#user_profile_info_about'),
		confirmpersonaldata=$('#confirm_personaldata'),
		divconfirmpassword=$('#div_confirm_password'),
		confirmpassword=$('#confirm_password'),
		btnconfirmpassword=$('#btn_confirm_password')
		confirmpasswordcover=$('.confirm_password-cover'),
		sendaddphonesmsprompt=$('#send-addphone-sms-prompt'),
		checkphonenumberverification=$('#check-phone_number_verification')
		cancelphonenumberverification=$('#cancel-phone_number_verification');

		let pnawstep1=$('.pnaw-step1'),
		pnawstep2 = $('.pnaw-step2')


		checkphone()
		//test()
		addphonerandomnumber.val(randomString(10));
		function test(){
			phonenumberstable.css('display','none')
			phonenumberverifywidget.css('display','none');
			pnawstep2.css('display','none')
		}
		

		addphonenumber.click(()=>{
			//addphonenumber.fadeOut('slow')
			phonenumberverifywidget.fadeIn('slow')
			addphonenumber.css('display','none')
			//phonenumberverifywidget.css('display','block');
			phonenumberstable.css('display','none')
		})
		cancelsendaddphonesms.click(()=>{
			//addphonenumber.css('display','block')
			phonenumberverifywidget.fadeOut('slow')
			addphonenumber.fadeIn(2000)
			//phonenumberverifywidget.css('display','none');
		})


		//验证手机->发送验证码
		sendaddphonesms.click(()=>{
			if(isPhoneNo(phonenumber.val())){
				sendaddphonesmsprompt.css('display','none')
				let sendsmsdata = {}
				sendsmsdata['national_number']=phonenumber.val()
				sendsmsdata['random_number']=addphonerandomnumber.val()
				$.ajax({
					cache: true,
					type: "POST",
					url:'http://47.92.72.19/user/checkmobile',
					data:sendsmsdata,
					async: false,
					error: function(request) {
						console.log('error')
					},
					success: function(data) {
						if(data['exists']==0){
							$.ajax({
								cache: true,
								type: "POST",
								url:'http://47.92.72.19/user/sendVerificationCode',
								data:sendsmsdata,
								async: false,
								error: function(request) {
									sendaddphonesmsprompt.css('display','block')
									sendaddphonesmsprompt.text('请检查网络状况')
								},
								success: function(data) {
									//console.log('ok')
									pnawstep1.css('display','none')
									pnawstep2.css('display','block')
								}
							})
						}else{
							sendaddphonesmsprompt.css('display','block')
							sendaddphonesmsprompt.text('手机号已被注册')
						}
					}
				});
			}else{
				sendaddphonesmsprompt.css('display','block')
				sendaddphonesmsprompt.text('手机号码格式有误')
			}	
		})
		//验证验证码
		checkphonenumberverification.click(()=>{
			let sendcodedata = {}
			sendcodedata['national_number']=phonenumber.val()
			sendcodedata['random_number']=addphonerandomnumber.val()
			sendcodedata['check_num']=phonenumberverification.val()
			$.ajax({
				cache: true,
				type: "POST",
				url:'http://47.92.72.19/user/checkVerificationCode',
				data:sendcodedata,
				async: false,
				error: function(request) {
					console.log('error')
				},
				success: function(data) {
					switch(data['code']){
                        case 0:
                        alert('111')
                        break;
                        case 1:
                        alert('22222')
                        break;
                        case 2:
                        alert('3333')
                        break;
                    }
				}
			});
		})

		//



		function checkphone(){
				if (havephonenumber.attr('data-number')){
					nophonenumbers.css('display','none')
					addphonenumber.css('display','none')
					phonenumberverifywidget.css('display','none');
					encodephone[0].innerText='+86'+' *** **** '+ havephonenumber.attr('data-number').substring(7,11)
				}else{
					nophonenumbers.css('display','block')
					phonenumberstable.css('display','none')
					addphonenumber.css('display','block')
					phonenumberverifywidget.css('display','none');
					//console.log(2)
				}
		}
		confirmpersonaldata.click(()=>{
			
			divconfirmpassword.css('display','block')
		})

		//console.log(new Date(Date.parse(userbirthday.val())).getTime()/1000)
		btnconfirmpassword.click(()=>{
			let updatedata={}
				updatedata['realname']=userfirstname.val()
				updatedata['gender']=usersex.val();
				updatedata['birthday'] = new Date(Date.parse(userbirthday.val())).getTime()/1000
				updatedata['userlocation']=userlocation.val()
				updatedata['userabout']=userabout.val()
			url='http://47.92.72.19/user/updatePersonalData';
			$.ajax({
				cache: true,
				type: "POST",
				url:url,
				data:updatedata,
				async: false,
				error: function(request) {
					console.log('error')
				},
				success: function(data) {
					console.log('ok')
				}
			});
		})
		confirmpasswordcover.click(()=>{
			divconfirmpassword.css('display','none')

		})
		function randomString(len) {
			　　len = len || 32;
			　　var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    
			　　var maxPos = $chars.length;
			　　var pwd = '';
			　　for (i = 0; i < len; i++) {
				　　　　pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
			　　}
			　　return pwd;
		}
		function isPhoneNo(phone) { 
			var pattern = /^1[34578]\d{9}$/; 
			return pattern.test(phone); 
		}

})