<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
   'js' => array(
"//js/config.js",
"//js/jquery-1.8.3.js",
"//royalslider/jquery.royalslider.min_9.5.1.js",
"//royalslider/jquery.easing-1.3.js",
"//js/bootstrap.min.js",
"//js/jquery.json-2.4.min.js",
"//js/Bubble.js",
"//js/jquery.grumble.min.js",
"//js/jquery.reveal.js",
"//js/login_register.js",
"//js/vpb_script.js",
"//js/dashboard.js",
"//js/country.js"
),
 'css'=>array(
"//css/bootstrap.min.css",
"//css/font-awesome.min.css",
 

"//css/bootstrap-responsive.min.css",
"//css/grumble.min.css",
"//css/reveal.css",
"//css/fancy_login_style.css",
"//royalslider/royalslider.css",
"//royalslider/skins/minimal-white/rs-minimal-white.css",
"//css/country.css",
"//css/last.css")
);
?>
