<?php


/*
对一个数组的多条记录,抽取一个key的所有记录,
相当于从表格中选择某一列

From
       Name  Sex    Age
       Alex  male   33
       Tom   male   40

To:
      Name
      Alex
      Tom
*/





function array_retrieve($arr,$key)
{
$result=array();

foreach ($arr as $onearr)
{
 $result[]=$onearr[$key];
}
return $result;

}

/*
把一个json字符串,转为数组
*/
function jsonstr2arr($str)
{
	  $json_str = str_replace("'",'"',$str ); 
	  $json_arr = json_decode($json_str,true); 
	  return $json_arr;
	  
}

/*数组转换pda需要的,|分割字符串*/
 	function array2pdastr($arr)
{
 
	if(sizeof($arr)>0)
  { 
	//数组的第一个记录,来取得key
  $one=$arr[0];
  $keys=array_keys($one);
  
  $pdastr=json_encode($arr);
  //去掉[ ]
  $pdastr=str_replace('[','',$pdastr);
  $pdastr=str_replace(']','',$pdastr);
  //去掉引号 "
  $pdastr=str_replace('"','',$pdastr);
  
  //去掉引号 '
  $pdastr=str_replace("'",'',$pdastr);
  
  
  foreach($keys as $onekey)
  { 
  	//去掉 key:
  	$pdastr=str_replace("$onekey:",'',$pdastr);
  	//每条记录间用|代替,分割
    $pdastr=str_replace("},{",'|',$pdastr);
    //去掉所有的{ }
    $pdastr=str_replace('{','',$pdastr);
  	$pdastr=str_replace('}','',$pdastr);
  }
  return 'PDA'.$pdastr;
  
  }else
  {
  return '';
  }
}

/*数组转换为GroupString,给Extjs Group Grid使用*/



 function array2groupstr($arr)
 {
  if(sizeof($arr)>0)
 {
      
   $one=$arr[0];
   $keys=array_keys($one);
   $str=json_encode($arr);
   $str=str_replace('{','[',$str);
   $str=str_replace('}',']',$str);
   foreach($keys as $onekey)
  { 
  	//去掉 key:
  	$kstr='"'.$onekey.'":';
  	$str=str_replace($kstr,'',$str);
  }
   $str=str_replace('"',"'",$str);
   return $str;
  }
  else
  {return '';}  
 }

 
?>