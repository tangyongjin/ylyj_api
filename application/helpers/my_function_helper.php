 <?php


  
 

function getargs()
{ 
    
    
   
    header("Access-Control-Allow-Origin: * "); 
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept"); 


    error_reporting(E_ALL);
    ini_set('display_errors', 0);
    register_shutdown_function('shutdown_error');
    set_error_handler('myErrorHandler');  
    
    $callstack = debug_backtrace();
    $file      = $callstack[0]['file'];
    $fname     = $callstack[1]['function'];
    $class     = $callstack[1]['class'];
    
    
    $header = getallheaders();
    $args   = array_merge($_GET, $_POST);

    

    write_log($file, $fname, $class, $args);
  
    return $args;
    
    
}


function write_log($controller, $function, $class, $para)
{
    
    $fname = get_log();
    $log   = fopen($fname, 'a+');
    fwrite($log, "<hr/>");
    $time = strftime("%Y-%m-%d %H:%M:%S", time());
    fwrite($log, "<div><span class =functionname>" . $controller . '/' . $function . '</span></div>');
    fwrite($log, "<div>" . $time . "</div>");
    
    
    if (gettype($para) == 'string') {
        fwrite($log, $para);
    } else {
        fwrite($log, json_encode($para, JSON_UNESCAPED_UNICODE));
    }
    reset($para);
    fclose($log);
}




function logtext($para)
{
    $fname = get_log();
    $log   = fopen($fname, 'a+');
    fwrite($log, "<hr/>");
    $time = strftime("%Y-%m-%d %H:%M:%S", time());
    ob_start();
    debug($para);
    $browser_string = ob_get_contents();
    ob_end_clean();
    fwrite($log, "<div>" . $time . "</div>");
    fwrite($log, $browser_string);
    fclose($log);
 }

function logerroinfo($para)
{
    $fname = get_log();
    $log   = fopen($fname, 'a+');
    ob_start();
    debug($para);
    $browser_string = ob_get_contents();
    ob_end_clean();
    fwrite($log, $browser_string);
    fclose($log);
 }


function shutdown_error()
{
    
    $error = error_get_last();
    if ($error['type'] === E_ERROR) {
        logtext($error);
    }
    
}


function myErrorHandler($errno, $errstr, $errfile, $errline)
{

    $errinfo='<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">';
    $errinfo.="Message: [$errno] $errstr<br/>";
    $errinfo.="Line Number: [$errline]<br/>";
    $errinfo.="Filename: [$errfile]</div>  ";
   
    logerroinfo($errinfo);

}


function get_log()
{
    return '/var/www/html/application/logs/room.log';
}






function jsonoutput($data)
{
    
    header('Content-Type: application/json');
    echo json_encode($data);
    die;
}



function db_exits($table, $col, $value)
{
    
    $CI =& get_instance();
    $sql = $CI->db->select($col)->from($table)->where($col, $value)->get();
    if ($sql->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}


function db_not_exits($table, $col, $value)
{
    
    $CI =& get_instance();
    $sql = $CI->db->select($col)->from($table)->where($col, $value)->get();
    if ($sql->num_rows() > 0) {
        return false;
    } else {
        return true;
    }
}





function db_col_by_col($table, $col1, $value,$col2 )
{
    
    $CI =& get_instance();
    $row = $CI->db->select($col2)->from($table)->where($col1, $value)->get()->row_array();
    
    if($row){
        return $row[$col2];
    }
    else{
        return null;
    }
}




//公共的随机串.

function randstr($length = 16)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str   = "";
    for ($i = 0; $i < $length; $i++) {
        $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
}



function randpwd($length = 6)
{
    $chars = "0123456789";
    $str   = "";
    for ($i = 0; $i < $length; $i++) {
        $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
}




function getOs($uagent)
{
    // the order of this array is important
    
    $oses = array(
        'Win311' => 'Win16',
        'Win95' => '(Windows 95)|(Win95)|(Windows_95)',
        'WinME' => '(Windows 98)|(Win 9x 4.90)|(Windows ME)',
        'Win98' => '(Windows 98)|(Win98)',
        'Win2000' => '(Windows NT 5.0)|(Windows 2000)',
        'WinXP' => '(Windows NT 5.1)|(Windows XP)',
        'WinServer2003' => '(Windows NT 5.2)',
        'WinVista' => '(Windows NT 6.0)',
        'Win7' => '(Windows NT 6.1)',
        'Win8' => '(Windows NT 6.2)',
        'WinNT' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
        'OpenBSD' => 'OpenBSD',
        'SunOS' => 'SunOS',
        'Ubuntu' => 'Ubuntu',
        'Android' => 'Android',
        'Linux' => '(Linux)|(X11)',
        'iPhone' => 'iPhone',
        'iPad' => 'iPad',
        'MacOS' => '(Mac_PowerPC)|(Macintosh)',
        'QNX' => 'QNX',
        'BeOS' => 'BeOS',
        'OS2' => 'OS/2',
        'SearchBot' => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
    );
    
    $uagent = strtolower($uagent ? $uagent : $_SERVER['HTTP_USER_AGENT']);
    foreach ($oses as $os => $pattern)
        if (preg_match('/' . $pattern . '/i', $uagent))
            return $os;
    return 'Unknown';
}



?> 