<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Zfbpay extends CI_Controller
{
    
    
    public function __construct()
    
    {
        
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        include FCPATH . 'alipay/config.php';
        $this->zhfcfg = $config;
        
        parent::__construct();
    }
    



    
    // 支付宝付款
    public function zhifb_pay()
    {
        $args = getargs();
        header("Content-type: text/html; charset=utf-8");

        $seqno= $args['seqno'] ;

        $order=$this->xorder->orderdetailbyseqno( $seqno ) ;
        
        $order_zfb = array(
            'WIDout_trade_no' => $seqno,
            'WIDsubject' => $order['roomtitle'], //商品名称
            'WIDtotal_amount' => $order['ordermoney'],  //支付宝用元为单位
            'WIDbody' => $order['roomdesc'] //商品描述
        );
        
        $this->xzhfb->go2zhifb($order_zfb);
    }
    
    

    //支付宝通知接口,收到后要回传"success"给支付宝,否则分钟后支付宝会再次发送通知.
    public function notify()
    {

      
 

        $arr = getargs();

        ob_clean(); //防止前面出现的字符串
        echo "success";
        
        $row['gmt_payment']=$arr['gmt_payment']; 
        $row['notify_time']=$arr['notify_time']; 
        $row['subject']=$arr['subject']; 
        $row['buyer_id']=$arr['buyer_id']; 
        $row['body']=$arr['body']; 
        $row['invoice_amount']=$arr['invoice_amount']; 
        $row['version']=$arr['version']; 
        $row['notify_id']=$arr['notify_id'];
        $row['fund_bill_list']=$arr['fund_bill_list'];
        $row['notify_type']=$arr['notify_type']; 
        $row['trade_status']=$arr['trade_status'];  //'TRADE_FINISHED', 'TRADE_SUCCESS'
        $row['out_trade_no']=$arr['out_trade_no']; 
        $row['total_amount']=$arr['total_amount']; 
        $row['trade_no']=$arr['trade_no']; 
        $row['auth_app_id']=$arr['auth_app_id']; 
        $row['receipt_amount']=$arr['receipt_amount']; 
        $row['point_amount']=$arr['point_amount']; 
        $row['app_id']=$arr['app_id'];
        $row['buyer_pay_amount']=$arr['buyer_pay_amount'];
        $row['sign_type']=$arr['sign_type']; 
        $row['seller_id']=$arr['seller_id']; 
        $this->db->insert('room_zfb_payinfo',$row);


        $dbcheckresult=$this->dbcheck($arr);
        $orderpid=$this->xorder->getorderidbyseqno($arr['out_trade_no']);
         
        //修改支付宝付款检查字段 
        $this->db->where('out_trade_no',$arr['out_trade_no']);
        $this->db->update('room_zfb_payinfo',array('doublecheckresult'=>$dbcheckresult));
        

        if($dbcheckresult='success'){
            if($arr['trade_status']=='TRADE_SUCCESS'){
                $orderstate='success';
            }
            if($arr['trade_status']=='TRADE_FINISHED'){
                $orderstate='TRADE_FINISHED';
            }
        }else{
                $orderstate='fail';
        }
        $this->xorder->orderPostProcess(   $orderpid,$orderstate);
    
      
    }


    
    public function dbcheck($arr)
    {
        $alipaySevice = new AlipayTradeService($this->zhfcfg);
        $result = $alipaySevice->check($arr);
        if ($result) 
        {
            return 'success';
        }else{
            return 'fail';
        }
    }
    
    
    public function callback()
    {
        //http://47.92.72.19/order/20170905-UjeQkP6cd5U

        $args = getargs();

        
        $seqno= $args['out_trade_no'] ;
        $servername=$_SERVER['SERVER_NAME'];
        $url= "http://".$servername."/order/".$seqno;
        
        // echo $url;die;  

        header("Location:  $url");
        die();

        // debug($args);die;

    }
    
}

?> 