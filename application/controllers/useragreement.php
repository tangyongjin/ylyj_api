<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Useragreement extends CI_Controller
{
   
    function index()
    {
        $this->load->model('i18n'); 
        $lang = $this->i18n->get_current_locale();
        $useragreement   = $this->load->view($lang . '/' . 'useragreement', '', true);
        echo  $useragreement;
       
    }
}

?>
