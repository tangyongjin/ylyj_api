<?php


    /**
     * 刷新Token
     * @param SmartLockSupplier $supplier
     * @return array
     */
    
     function refreshToken(SmartLockSupplier $supplier)
    {
        if (!$supplier) {
            return ['errcode' => -1, 'errmsg' => '找不到火河门锁配置文件'];
        }
        $sId = GenerateHelper::guid();
        $request = [
            'header' => $this->generateHeader(null, $sId),
            'params' => [
                'account' => $supplier->account,
                'password' => SecurityHelper::javaDesEncrypt($supplier->password, $supplier->security_key)
            ]
        ];
        try {
            $responseResult = SmartCurl::huoHePost($supplier->token_url, $request['header'], $request['params']);
            $responseArr = $this->jsonToArr($responseResult);
            $supplier->access_token = $responseArr['access_token'];
            if (!$supplier->save()) {
                return ['errcode' => -1, 'errmsg' => 'Token保存失败'];
            }
            return ['errcode' => 0, 'errmsg' => 'ok'];
        } catch (\Exception $e) {
            return ['errcode' => -1, 'errmsg' => $e->getMessage()];
        }
    }
    
    
    /**
     * 火河POST
     * @param string $url
     * @param array $header
     * @param array $params
     * @return mixed
     * @throws Exception
     */
      function huoHePost($url, array $header, array $params)
    {
        $data = json_encode($params, JSON_UNESCAPED_UNICODE);
        $header[] = 'Content-Type: application/json; charset=utf-8';
        $header[] = 'Content-Length: ' . strlen($data);
        $ch = curl_init();
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HEADER => false, // 启用时会将头文件的信息作为数据流输出
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, // 强制使用 HTTP/1.1
            CURLOPT_RETURNTRANSFER => true, //将curl_exec()获取的信息以字符串返回
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
        ];
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        if ($response !== false) {
            curl_close($ch);
            return $response;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new Exception("curl出错，错误码:{$error}");
        }
    }
    
    
    /**
     * DES加密（java兼容模式，密文转为十六进制字符串）
     * @param $dat
     * @param $key
     * @return string
     */
     function javaDesEncrypt($dat, $key)
    {
        $block = mcrypt_get_block_size(MCRYPT_DES, MCRYPT_MODE_ECB);
        $len = strlen($dat);
        $padding = $block - ($len % $block);
        $dat .= str_repeat(chr($padding),$padding);
        return bin2hex(mcrypt_encrypt(MCRYPT_DES, $key, $dat, MCRYPT_MODE_ECB));
    }

    /**
     * DES解密（java兼容模式，十六进制字符串转为密文再解密）
     * @param $dat
     * @param $key
     * @return string
     */
     function javaDesDecrypt($dat, $key)
    {
        $str = hex2bin($dat);
        $str = mcrypt_decrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB);
        $pad = ord($str[($len = strlen($str)) - 1]);
        return substr($str, 0, strlen($str) - $pad);
    }


?>
