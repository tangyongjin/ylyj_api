<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comment extends CI_Controller
{
   
 public function oneRoomComment(){


   error_reporting(E_ALL);
   ini_set('display_errors', 1);
  
  
  $args=getargs();
  header("Access-Control-Allow-Origin: * "); 
  $roomid=$args['roomid'];

  $idx=$args['idx'];
   
  $pagesize=10;
   
  $sql="select  room_comments.pid  as commentid, content,room_comments.createtime  ,
realname ,  covername as avatr 
from room_comments ,room_user
where roomid =$roomid and   room_comments.userid=room_user.pid";

  $rows = $this->db->query($sql)->result_array();

  
  $total          = count($rows);
  $pages          = ceil($total / $pagesize);
  $offset         = $pagesize * ($idx - 1);
  $sql            = $sql . " limit $offset, $pagesize  ";


  
  $comments=$this->db->query($sql)->result_array();



   $data      = array(
            'total' => $total,
            'idx'=>$idx,
            'pages'=>$pages,
            'detail' => $comments

        );


 
  echo  json_encode($data);


}


public function get_game_watchers($gameid, $groupid, $idx, $pagesize)
    {
        $sys_path   = SERVER_PIC_DIR;
        $sql_public = "select distinct t_user.id,t_user.nickname,concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as cover ";
        $sql_public .= "from t_game_watch,t_user where  t_user.id=t_game_watch.userid  ";
        
        if ($groupid > 1) {
            $watch_type = WATCH_GAME_GROUP;
            $sql        = $sql_public . " and   t_game_watch.groupid=$groupid and t_game_watch.type=$watch_type";
            
        } else {
            $watch_type = WATCH_GAME;
            $sql        = $sql_public . " and   t_game_watch.gameid=$gameid and t_game_watch.type=$watch_type";
        }
        
        $rows           = $this->db->query($sql)->result_array();
        $total          = count($rows);
        $pages          = ceil($total / $pagesize);
        $offset         = $pagesize * ($idx - 1);
        $sql            = $sql . " order by t_game_watch.create_time desc limit $offset, $pagesize  ";
        
        $user_info_list = $this->db->query($sql)->result_array();
        $data           = array(
            'watchs_num' => $total,
            'watchs_user_info_list' => $user_info_list
        );
        return $data;
    }
  
//房源评价
 public function  house(){
   
  //参考  http://www.mayi.com/room/850628400
  //lodge_id 房源id

  // debug($_GET);

  $ret=array('status'=>1);
  $page=1;
  $totalPage=15;
  

  $roomid=$_GET['roomid'];
  $pagesize=$_GET['pageSize'];
  $idx=$_GET['page'];
 
  $sql="select room_comments.pid as id,userid as authorid,userid as tenantid,  roomid as lodgeunitid ,0 as bookorderid,
   content ,createtime,createtime as timeString,room_comments.roomid,roomtitle
   from room_comments ,room_source
   where room_source.pid=room_comments.roomid and  roomid=$roomid";
  

  $rows           = $this->db->query($sql)->result_array();
  $total          = count($rows);
  $pages          = ceil($total / $pagesize);
  $offset         = $pagesize * ($idx - 1);
  $sql            = $sql . "   limit $offset, $pagesize  ";
        
  $rows= $this->db->query($sql)->result_array();

  $fixrows=array();
  

  foreach ($rows as $key => $one_comment) {
     $one_comment['lodgeunit']['id']=$one_comment['roomid'];
     $one_comment['lodgeunit']['title']=$one_comment['roomtitle'];
     $authorid=$one_comment['authorid'];
     $one_comment['tenant']= $this->xguest->guestdetail($authorid);
     $one_comment['recomment']= $this->getreply($one_comment['id']);
     

     $fixrows[]=$one_comment;

  }


  $ret['data']['comments']=$fixrows;
  $ret['data']['total']=$total;
  
  $ret['totalCount']=$total;
  $ret['pageNo']=$idx;
  $ret['totalPage']=$pages;



  header('Content-type:text/json'); 
  echo  json_encode($ret);

  die;




    $json='{"status":1,"data":{"comments":[{"id":850759380,"authorid":853488152,"lodgeunitid":850628400,"bookorderid"
:851211255,"landlordid":851053756,"tenantid":853488152,"appraise":{"sanitation":3,"security":3,"description"
:3,"location":3,"performance":3},"content":"房间干净整洁 装修风格也是我喜欢的 家人很满意 小区环境也特别好 很有家的感觉  基本日常生活用品都比较全 厨房
也不错 房东特别热情 孩子到了就开始发烧 房东还介绍药店和医院给我 帮了很大忙 非常感谢  下次去还会再去住的","createtime":"2017-03-25 21:58","timeString"
:"2017-03-25 21:58","tenant":{"id":853488152,"username":"m#_853488152","nickname":"用户7257","realname"
:"13821847257","mobile":"13821847257","headimageurl":"https://i1.mayi.com/gjfs21/M04/B2/C1/CgEHFVcxdFX
,HwoFAABhlK8CvOk554.jpg_54x54c.jpg","serverid":1,"estate":"valid","createtime":"2017-03-10 21:14","lastvisittime"
:"2017-03-10 21:14","fastpaystate":0,"shownickname":"138****7257","areacode":"86","otherareacode":"86"
},"recomment":{"id":850782455,"authorid":851053756,"lodgeunitid":850628400,"bookorderid":851211255,"landlordid"
:851053756,"tenantid":853488152,"prarentcommentid":850759380,"state":2,"createtime":"2017-03-29 22:55"
,"lastaudittime":"2017-03-30 11:42","lastupdatetime":"2017-03-29 22:55","property":1,"lastauditid":1170
,"lastauditremark":{"common":""},"appraise":{"sanitation":3,"security":3,"description":3,"location":3
,"performance":3},"title":"谢谢，都是力所能及的事，欢迎.........","content":"谢谢，都是力所能及的事，欢迎您下次再来"},"lodgeunit":{"id"
:850628400,"title":"近厦门北站 机场 园博苑大三居整租"},"checkindayStr":"2017年03月"},{"id":850675301,"authorid":851335063
,"lodgeunitid":850628400,"bookorderid":851152686,"landlordid":851053756,"tenantid":851335063,"appraise"
:{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"content":"房子大，交通方便，主要是厨
房用起来非常方便，调味料具全，公务出差＋旅游","createtime":"2017-03-04 18:54","timeString":"2017-03-04 18:54","tenant":{"id"
:851335063,"username":"m#_851335063","nickname":"用户5898","realname":"13815275898","mobile":"13815275898"
,"headimageurl":"https://i1.mayi.com/gjfs15/M06/C7/19/CgEHQFcxdRfgpEk7AABlcbtch,g688.jpg_54x54c.jpg"
,"serverid":1,"estate":"valid","createtime":"2016-03-13 11:28","lastvisittime":"2017-03-28 22:28","fastpaystate"
:0,"shownickname":"138****5898","areacode":"86","otherareacode":"86"},"recomment":{"id":850676707,"authorid"
:851053756,"lodgeunitid":850628400,"bookorderid":851152686,"landlordid":851053756,"tenantid":851335063
,"prarentcommentid":850675301,"state":2,"createtime":"2017-03-05 20:13","lastaudittime":"2017-03-06 09
:34","lastupdatetime":"2017-03-05 20:13","property":1,"lastauditid":1170,"lastauditremark":{"common"
:""},"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"title":"
感谢，你住的舒适方便最重要，.........","content":"感谢，你住的舒适方便最重要，欢迎下次再来厦门旅游。"},"lodgeunit":{"id":850628400,"title":"
近厦门北站 机场 园博苑大三居整租"},"checkindayStr":"2017年02月"},{"id":850660836,"authorid":851500842,"lodgeunitid":850628400
,"bookorderid":851144896,"landlordid":851053756,"tenantid":851500842,"appraise":{"sanitation":3,"security"
:3,"description":3,"location":3,"performance":3},"content":"房间很好.干净大方.小区建设很棒.厨房都能用.还和朋友们做了顿饭吃.房东先生人很
好.落下东西了还帮快递.有机会来还会住！","createtime":"2017-02-20 22:18","timeString":"2017-02-20 22:18","tenant":{"id"
:851500842,"username":"m#_851500842","nickname":"用户4775","realname":"18635594775","mobile":"18635594775"
,"headimageurl":"https://i1.mayi.com/gjfs15/M06/C7/19/CgEHQFcxdRfgpEk7AABlcbtch,g688.jpg_54x54c.jpg"
,"serverid":1,"estate":"valid","createtime":"2016-04-13 20:16","lastvisittime":"2017-02-11 20:39","fastpaystate"
:0,"shownickname":"186****4775","areacode":"86","otherareacode":"86"},"recomment":{"id":850670838,"authorid"
:851053756,"lodgeunitid":850628400,"bookorderid":851144896,"landlordid":851053756,"tenantid":851500842
,"prarentcommentid":850660836,"state":2,"createtime":"2017-03-01 12:42","lastaudittime":"2017-03-01 15
:04","lastupdatetime":"2017-03-01 12:42","property":1,"lastauditid":2490,"lastauditremark":{"common"
:""},"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"title":"
应该的，欢迎下次再来厦门旅游。","content":"应该的，欢迎下次再来厦门旅游。"},"lodgeunit":{"id":850628400,"title":"近厦门北站 机场 园博苑大三居整租"
},"checkindayStr":"2017年02月"},{"id":850650057,"authorid":853206808,"lodgeunitid":850628400,"bookorderid"
:851109731,"landlordid":851053756,"tenantid":853206808,"appraise":{"sanitation":3,"security":3,"description"
:3,"location":3,"performance":3},"content":"小区很美，交通非常方便。房子不错，调料齐全，布置也很温馨。房东很耐心，帮忙解决不少问题。","createtime"
:"2017-02-10 15:29","timeString":"2017-02-10 15:29","tenant":{"id":853206808,"username":"#o_853206808"
,"nickname":"Anna","realname":"Anna","mobile":"13611269912","sex":"women","headimageurl":"https://i1
.mayi.com/mayi47/M46/GC/CP/AV27MP7KS84PSMFSPJJPPKAGEZ64N4.jpg_54x54c.jpg","serverid":1,"estate":"valid"
,"createtime":"2017-01-25 12:48","lastvisittime":"2017-02-06 19:35","fastpaystate":0,"shownickname":"Anna"
,"areacode":"86","otherareacode":"86"},"recomment":{"id":850650635,"authorid":851053756,"lodgeunitid"
:850628400,"bookorderid":851109731,"landlordid":851053756,"tenantid":853206808,"prarentcommentid":850650057
,"state":2,"createtime":"2017-02-10 22:56","lastaudittime":"2017-02-11 13:45","lastupdatetime":"2017-02-10
 22:56","property":1,"lastauditid":1170,"lastauditremark":{"common":""},"appraise":{"sanitation":3,"security"
:3,"description":3,"location":3,"performance":3},"title":"咱们家正处于进岛桥头位置，去.........","content":"咱们家正处于
进岛桥头位置，去岛内在都很方便。帮忙都是一些力所能及的事，欢迎您下次再来厦门旅游。"},"lodgeunit":{"id":850628400,"title":"近厦门北站 机场 园博苑大三居整租"}
,"checkindayStr":"2017年02月"},{"id":850649854,"authorid":852545387,"lodgeunitid":850628400,"bookorderid"
:851063220,"landlordid":851053756,"tenantid":852545387,"appraise":{"sanitation":3,"security":3,"description"
:3,"location":3,"performance":3},"content":"房间很大很干净 所有用具齐全，属于可以空手入住的","createtime":"2017-02-10 11:05"
,"timeString":"2017-02-10 11:05","tenant":{"id":852545387,"username":"m#_852545387","nickname":"用户0781"
,"realname":"17740850781","mobile":"17740850781","headimageurl":"https://i1.mayi.com/gjfs15/M05/C7/30
/CgEHP1cxdOXUq,Q-AABd0owykF8682.jpg_54x54c.jpg","serverid":1,"estate":"valid","createtime":"2016-10-01
 11:15","lastvisittime":"2017-01-28 21:08","fastpaystate":0,"shownickname":"177****0781","areacode":"86"
,"otherareacode":"86"},"recomment":{"id":850650636,"authorid":851053756,"lodgeunitid":850628400,"bookorderid"
:851063220,"landlordid":851053756,"tenantid":852545387,"prarentcommentid":850649854,"state":2,"createtime"
:"2017-02-10 22:59","lastaudittime":"2017-02-11 13:45","lastupdatetime":"2017-02-10 22:59","property"
:1,"lastauditid":1170,"lastauditremark":{"common":""},"appraise":{"sanitation":3,"security":3,"description"
:3,"location":3,"performance":3},"title":"感谢，帅哥对我房子认可，连续.........","content":"感谢，帅哥对我房子认可，连续入住2天，房屋保
持很干净，欢迎下次再来厦门旅游"},"lodgeunit":{"id":850628400,"title":"近厦门北站 机场 园博苑大三居整租"},"checkindayStr":"2017年01月"
},{"id":850649852,"authorid":852545387,"lodgeunitid":850628400,"bookorderid":851053196,"landlordid":851053756
,"tenantid":852545387,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance"
:3},"content":"房间很大很干净 所有用具齐全，属于可以空手入住的","createtime":"2017-02-10 11:03","timeString":"2017-02-10 11
:03","tenant":{"id":852545387,"username":"m#_852545387","nickname":"用户0781","realname":"17740850781"
,"mobile":"17740850781","headimageurl":"https://i1.mayi.com/gjfs15/M05/C7/30/CgEHP1cxdOXUq,Q-AABd0owykF8682
.jpg_54x54c.jpg","serverid":1,"estate":"valid","createtime":"2016-10-01 11:15","lastvisittime":"2017-01-28
 21:08","fastpaystate":0,"shownickname":"177****0781","areacode":"86","otherareacode":"86"},"lodgeunit"
:{"id":850628400,"title":"近厦门北站 机场 园博苑大三居整租"},"checkindayStr":"2017年01月"},{"id":850644440,"authorid"
:853186483,"lodgeunitid":850628400,"bookorderid":851102286,"landlordid":851053756,"tenantid":853186483
,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"content":"不错
！房子很干净！比酒店强！！","createtime":"2017-02-06 11:49","timeString":"2017-02-06 11:49","tenant":{"id":853186483
,"username":"#o_853186483","nickname":"山不在高，有你则名！","realname":"山不在高，有你则名！","mobile":"18067592799","sex"
:"man","headimageurl":"https://i1.mayi.com/mayi10/M14/OA/QK/9HZF5UJ2ZNE2CFV9VD38W5K3CJKKQH.jpg_54x54c
.jpg","serverid":1,"estate":"valid","createtime":"2017-01-20 17:58","lastvisittime":"2017-03-09 22:06"
,"fastpaystate":0,"shownickname":"山不在高，有你则名！","areacode":"86","otherareacode":"86"},"recomment":{"id"
:850644526,"authorid":851053756,"lodgeunitid":850628400,"bookorderid":851102286,"landlordid":851053756
,"tenantid":853186483,"prarentcommentid":850644440,"state":2,"createtime":"2017-02-06 14:52","lastaudittime"
:"2017-02-08 12:13","lastupdatetime":"2017-02-06 14:52","property":1,"lastauditid":1170,"lastauditremark"
:{"common":""},"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3}
,"title":"感谢，你住的舒适是最重要，欢.........","content":"感谢，你住的舒适是最重要，欢迎下次旅行在来我们家。祝你旅途愉快"},"lodgeunit":{"id":850628400
,"title":"近厦门北站 机场 园博苑大三居整租"},"checkindayStr":"2017年02月"},{"id":850640952,"authorid":853076167,"lodgeunitid"
:850628400,"bookorderid":851068113,"landlordid":851053756,"tenantid":853076167,"appraise":{"sanitation"
:3,"security":3,"description":3,"location":3,"performance":3},"content":"房间整洁床单被套都是洗过的，价位合理性价比高","createtime"
:"2017-02-03 22:46","timeString":"2017-02-03 22:46","tenant":{"id":853076167,"username":"m#_853076167"
,"nickname":"用户5214","realname":"13861695214","mobile":"13861695214","headimageurl":"https://i1.mayi
.com/gjfs15/M06/C7/19/CgEHQFcxdRfgpEk7AABlcbtch,g688.jpg_54x54c.jpg","serverid":1,"estate":"valid","createtime"
:"2017-01-04 20:47","lastvisittime":"2017-04-11 18:19","fastpaystate":0,"shownickname":"138****5214"
,"areacode":"86","otherareacode":"86"},"recomment":{"id":850642281,"authorid":851053756,"lodgeunitid"
:850628400,"bookorderid":851068113,"landlordid":851053756,"tenantid":853076167,"prarentcommentid":850640952
,"state":2,"createtime":"2017-02-04 22:06","lastaudittime":"2017-02-06 10:33","lastupdatetime":"2017-02-04
 22:06","property":1,"lastauditid":1170,"lastauditremark":{"common":""},"appraise":{"sanitation":3,"security"
:3,"description":3,"location":3,"performance":3},"title":"感谢，祝你旅途愉快。","content":"感谢，祝你旅途愉快。"},"lodgeunit"
:{"id":850628400,"title":"近厦门北站 机场 园博苑大三居整租"},"checkindayStr":"2017年02月"},{"id":850640497,"authorid"
:851767344,"lodgeunitid":850628400,"bookorderid":851049663,"landlordid":851053756,"tenantid":851767344
,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"content":"小区
环境很好，房东热情，房间干净，设备齐全，下次来还住这里。","createtime":"2017-02-03 18:55","timeString":"2017-02-03 18:55","tenant"
:{"id":851767344,"username":"m#_851767344","nickname":"用户2689","realname":"18621292689","mobile":"18621292689"
,"headimageurl":"https://i1.mayi.com/gjfs21/M04/B2/C1/CgEHFVcxdFX,HwoFAABhlK8CvOk554.jpg_54x54c.jpg"
,"serverid":1,"estate":"valid","createtime":"2016-06-02 22:51","lastvisittime":"2017-03-11 22:55","fastpaystate"
:0,"shownickname":"186****2689","areacode":"86","otherareacode":"86"},"recomment":{"id":850642282,"authorid"
:851053756,"lodgeunitid":850628400,"bookorderid":851049663,"landlordid":851053756,"tenantid":851767344
,"prarentcommentid":850640497,"state":2,"createtime":"2017-02-04 22:08","lastaudittime":"2017-02-06 10
:33","lastupdatetime":"2017-02-04 22:08","property":1,"lastauditid":1170,"lastauditremark":{"common"
:""},"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"title":"
谢谢，很好相处的一家人，也非.........","content":"谢谢，很好相处的一家人，也非常爱干净，欢迎你明年假期在来我家住。"},"lodgeunit":{"id":850628400,"title"
:"近厦门北站 机场 园博苑大三居整租"},"checkindayStr":"2017年01月"},{"id":850634231,"authorid":853126690,"lodgeunitid"
:850628400,"bookorderid":851075180,"landlordid":851053756,"tenantid":853126690,"appraise":{"sanitation"
:3,"security":3,"description":2,"location":3,"performance":2},"content":"房间还挺新，房东不错。设施还有点新装修的味道。没有烧水
壶，建议增加！","createtime":"2017-01-30 20:52","timeString":"2017-01-30 20:52","tenant":{"id":853126690,"username"
:"m#_853126690","nickname":"用户5630","realname":"13851815630","mobile":"13851815630","headimageurl":"https
://i1.mayi.com/gjfs21/M00/B2/D6/CgEHFVcxdQeR0,YLAABZ61lXbWE653.jpg_54x54c.jpg","serverid":1,"estate"
:"valid","createtime":"2017-01-07 21:25","lastvisittime":"2017-01-07 21:25","fastpaystate":0,"shownickname"
:"138****5630","areacode":"86","otherareacode":"86"},"recomment":{"id":850634817,"authorid":851053756
,"lodgeunitid":850628400,"bookorderid":851075180,"landlordid":851053756,"tenantid":853126690,"prarentcommentid"
:850634231,"state":2,"createtime":"2017-01-31 10:51","lastaudittime":"2017-02-03 13:36","lastupdatetime"
:"2017-01-31 10:51","property":1,"lastauditid":1170,"lastauditremark":{"common":""},"appraise":{"sanitation"
:3,"security":3,"description":2,"location":3,"performance":2},"title":"感谢宝贵意见，不过我房子是开.........","content"
:"感谢宝贵意见，不过我房子是开发商统一精装修，已经空置一年多了，真得不是新装修房子。水壶我尽快添置，祝你旅途愉快。"},"lodgeunit":{"id":850628400,"title":"近厦
门北站 机场 园博苑大三居整租"},"checkindayStr":"2017年01月"}],"total":15},"totalCount":15,"pageNo":1,
"totalPage":2.0}';

  
  $json_arr= json_decode($json, true); 
  header('Content-type:text/json'); 
  echo  json_encode($json_arr);

 }    

  
public function getreply($commentid){

  $sql="select pid as id,userid as authorid, $commentid as prarentcommentid ,2 as state,
  createtime, createtime as lastaudittime,createtime as lastupdatetime,1 as property
  from room_comments_reply where commentid=$commentid 
  ";

  $row=$this->db->query($sql)->row_array();
  return $row;


}


//房东评价
 public function  owner(){
  
  //参考  http://www.mayi.com/room/850628400
  //lodge_id 房源id
 


  $json='{"status":1,"data":{"total":22,"comments":[{"id":850862503,"content":"可以说都很好，环境好，房子干净，房东也很好说话，下次来厦门还
要住这里","landlordid":851053756,"createtime":"2017-04-15 10:05","bookorderid":851416668,"tenantid":853320477
,"lodgeunitid":850753910,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance"
:3},"timeString":"2017-04-15 10:05","tenant":{"id":853320477,"nickname":"用户0209","headimageurl":"https
://i1.mayi.com/gjfs21/M04/B2/C1/CgEHFVcxdFX,HwoFAABhlK8CvOk554.jpg_54x54c.jpg","mobile":"13822517693"
,"shownickname":"用户0209","realname":"13138980209","createtime":"2017-02-16 22:01","username":"m#_853320477"
,"areacode":"86","fastpaystate":0,"serverid":1,"otherareacode":"86","lastvisittime":"2017-04-15 10:05"
,"estate":"valid"},"lodgeunit":{"id":850753910,"title":"中式复式三居150平 整租近机场厦门北"},"authorid":853320477,"checkindayStr"
:"2017年04月"},{"id":850826592,"content":"退房的时候给我索要50块清洁费，第一次遇到这种情况，跟我说酒店跟民房不一样，有什么不一样的，都是花钱入住，弄的心情都不好
了，差评","landlordid":851053756,"createtime":"2017-04-08 15:01","bookorderid":851405444,"tenantid":851371316
,"lodgeunitid":850753910,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance"
:3},"timeString":"2017-04-08 15:01","tenant":{"id":851371316,"nickname":"用户0282","headimageurl":"https
://i1.mayi.com/gjfs21/M00/B2/D6/CgEHFVcxdQeR0,YLAABZ61lXbWE653.jpg_54x54c.jpg","mobile":"15959220282"
,"shownickname":"159****0282","realname":"15959220282","createtime":"2016-03-21 14:21","username":"m
#_851371316","areacode":"86","fastpaystate":0,"serverid":1,"otherareacode":"86","lastvisittime":"2017-04-01
 19:56","estate":"valid"},"recomment":{"property":1,"id":850829293,"state":2,"content":"入住前，我已经提前告知你
，厨房和调料都免费用，要注意一下卫生，不然要收清洁费。退房时候你把每个房间，客厅，卫生间，厨房，电视全开启，不清楚你这是何用意。我只能说房子没有十全十美，你要住好的大房子，又想要酒店价格，住的那样随便
。我相信每个名宿房东让你这样住，都会收你清洁费。","landlordid":851053756,"lastaudittime":"2017-04-09 15:42","createtime":"2017-04-09
 12:26","bookorderid":851405444,"tenantid":851371316,"lodgeunitid":850753910,"title":"入住前，我已经提前告知你，厨
.........","lastupdatetime":"2017-04-09 12:26","appraise":{"sanitation":3,"security":3,"description"
:3,"location":3,"performance":3},"lastauditid":2472,"prarentcommentid":850826592,"authorid":851053756
,"lastauditremark":{"common":""}},"lodgeunit":{"id":850753910,"title":"中式复式三居150平 整租近机场厦门北"},"authorid"
:851371316,"checkindayStr":"2017年04月"},{"id":850812120,"content":"环境很不错！出去岛内也有公交直达！挺方便的","landlordid"
:851053756,"createtime":"2017-04-04 21:53","bookorderid":851333778,"tenantid":853794794,"lodgeunitid"
:850753910,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"timeString"
:"2017-04-04 21:53","tenant":{"id":853794794,"nickname":"用户1233","headimageurl":"https://i1.mayi.com
/gjfs15/M05/C7/30/CgEHP1cxdOXUq,Q-AABd0owykF8682.jpg_54x54c.jpg","mobile":"15920441233","shownickname"
:"159****1233","realname":"15920441233","createtime":"2017-03-28 01:21","username":"m#_853794794","fastpaystate"
:0,"sex":"women","age":4,"housetown_city":392,"housetown_province":19,"serverid":1,"otherareacode":"86"
,"profession":"会计","lastvisittime":"2017-04-04 21:52","estate":"valid"},"recomment":{"property":1,"id"
:850820209,"state":2,"content":"正好处于进岛桥头，等9月份地铁通车会更方便。","landlordid":851053756,"lastaudittime":"2017-04-06
 15:39","createtime":"2017-04-06 14:45","bookorderid":851333778,"tenantid":853794794,"lodgeunitid":850753910
,"title":"正好处于进岛桥头，等9月份地.........","lastupdatetime":"2017-04-06 14:45","appraise":{"sanitation":3,"security"
:3,"description":3,"location":3,"performance":3},"lastauditid":2561,"prarentcommentid":850812120,"authorid"
:851053756,"lastauditremark":{"common":""}},"lodgeunit":{"id":850753910,"title":"中式复式三居150平 整租近机场厦门北"
},"authorid":853794794,"checkindayStr":"2017年04月"},{"id":850797793,"content":"忘记了评价，房子非常非常的棒，有机会再来厦门
还会继续入住！","landlordid":851053756,"createtime":"2017-04-02 08:45","bookorderid":851319284,"tenantid":853760249
,"lodgeunitid":850753910,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance"
:3},"timeString":"2017-04-02 08:45","tenant":{"id":853760249,"nickname":"用户1100","headimageurl":"https
://i1.mayi.com/gjfs21/M04/B2/C1/CgEHFVcxdFX,HwoFAABhlK8CvOk554.jpg_54x54c.jpg","mobile":"15241651100"
,"shownickname":"152****1100","realname":"15241651100","createtime":"2017-03-26 08:10","username":"m
#_853760249","fastpaystate":0,"serverid":1,"otherareacode":"86","lastvisittime":"2017-04-03 12:44","estate"
:"valid"},"recomment":{"property":1,"id":850819911,"state":2,"content":"感谢，对我房子认可，续租一天","landlordid"
:851053756,"lastaudittime":"2017-04-06 15:38","createtime":"2017-04-06 14:23","bookorderid":851319284
,"tenantid":853760249,"lodgeunitid":850753910,"title":"感谢，对我房子认可，续租一天","lastupdatetime":"2017-04-06 14
:23","appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"lastauditid"
:2561,"prarentcommentid":850797793,"authorid":851053756,"lastauditremark":{"common":""}},"lodgeunit"
:{"id":850753910,"title":"中式复式三居150平 整租近机场厦门北"},"authorid":853760249,"checkindayStr":"2017年03月"},{"id"
:850778202,"content":"房东很热情，小区周边环境优美，复式三居三卫非常适合一大家人居住，喜欢房子的装修风格，家具设施齐全且全新，最满意的入住体验！","landlordid":851053756
,"createtime":"2017-03-29 10:00","bookorderid":851325619,"tenantid":853760249,"lodgeunitid":850753910
,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"timeString":"2017-03-29
 10:00","tenant":{"id":853760249,"nickname":"用户1100","headimageurl":"https://i1.mayi.com/gjfs21/M04/B2
/C1/CgEHFVcxdFX,HwoFAABhlK8CvOk554.jpg_54x54c.jpg","mobile":"15241651100","shownickname":"152****1100"
,"realname":"15241651100","createtime":"2017-03-26 08:10","username":"m#_853760249","fastpaystate":0
,"serverid":1,"otherareacode":"86","lastvisittime":"2017-04-03 12:44","estate":"valid"},"recomment":
{"property":1,"id":850782762,"state":2,"content":"谢谢您像爱护自己家一样的，退房时打扫的一尘不染。","landlordid":851053756,"lastaudittime"
:"2017-03-30 11:42","createtime":"2017-03-29 23:06","bookorderid":851325619,"tenantid":853760249,"lodgeunitid"
:850753910,"title":"谢谢您像爱护自己家一样的，退.........","lastupdatetime":"2017-03-29 23:06","appraise":{"sanitation"
:3,"security":3,"description":3,"location":3,"performance":3},"lastauditid":1170,"prarentcommentid":850778202
,"authorid":851053756,"lastauditremark":{"common":""}},"lodgeunit":{"id":850753910,"title":"中式复式三居150
平 整租近机场厦门北"},"authorid":853760249,"checkindayStr":"2017年03月"},{"id":850759380,"content":"房间干净整洁 装修风格
也是我喜欢的 家人很满意 小区环境也特别好 很有家的感觉  基本日常生活用品都比较全 厨房也不错 房东特别热情 孩子到了就开始发烧 房东还介绍药店和医院给我 帮了很大忙 非常感谢  下次去还会再去住的"
,"landlordid":851053756,"createtime":"2017-03-25 21:58","bookorderid":851211255,"tenantid":853488152
,"lodgeunitid":850628400,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance"
:3},"timeString":"2017-03-25 21:58","tenant":{"id":853488152,"nickname":"用户7257","headimageurl":"https
://i1.mayi.com/gjfs21/M04/B2/C1/CgEHFVcxdFX,HwoFAABhlK8CvOk554.jpg_54x54c.jpg","mobile":"13821847257"
,"shownickname":"138****7257","realname":"13821847257","createtime":"2017-03-10 21:14","username":"m
#_853488152","areacode":"86","fastpaystate":0,"serverid":1,"otherareacode":"86","lastvisittime":"2017-03-10
 21:14","estate":"valid"},"recomment":{"property":1,"id":850782455,"state":2,"content":"谢谢，都是力所能及的事，
欢迎您下次再来","landlordid":851053756,"lastaudittime":"2017-03-30 11:42","createtime":"2017-03-29 22:55","bookorderid"
:851211255,"tenantid":853488152,"lodgeunitid":850628400,"title":"谢谢，都是力所能及的事，欢迎.........","lastupdatetime"
:"2017-03-29 22:55","appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance"
:3},"lastauditid":1170,"prarentcommentid":850759380,"authorid":851053756,"lastauditremark":{"common"
:""}},"lodgeunit":{"id":850628400,"title":"近厦门北站 机场 园博苑大三居整租"},"authorid":853488152,"checkindayStr":"2017
年03月"},{"id":850757588,"content":"最爱复式，不错房子很大，三房三卫配置齐全，厨房调料齐全，性价比超高。离我上班杏林运营中心非常进，下此有朋友要来厦门旅游还祝这里。","landlordid"
:851053756,"createtime":"2017-03-25 12:34","bookorderid":851279239,"tenantid":853649835,"lodgeunitid"
:850753910,"appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"timeString"
:"2017-03-25 12:34","tenant":{"id":853649835,"nickname":"用户7852","headimageurl":"https://i1.mayi.com
/gjfs15/M06/C7/19/CgEHQFcxdRfgpEk7AABlcbtch,g688.jpg_54x54c.jpg","mobile":"17748547852","shownickname"
:"177****7852","realname":"17748547852","createtime":"2017-03-20 09:17","username":"m#_853649835","areacode"
:"86","fastpaystate":0,"serverid":1,"otherareacode":"86","lastvisittime":"2017-03-25 12:30","estate"
:"valid"},"recomment":{"property":1,"id":850788744,"state":2,"content":"感谢，对我房子认可,欢迎下次再来","landlordid"
:851053756,"lastaudittime":"2017-03-31 14:40","createtime":"2017-03-30 23:31","bookorderid":851279239
,"tenantid":853649835,"lodgeunitid":850753910,"title":"感谢，对我房子认可,欢迎下次.........","lastupdatetime":"2017-03-30
 23:31","appraise":{"sanitation":3,"security":3,"description":3,"location":3,"performance":3},"lastauditid"
:2170,"prarentcommentid":850757588,"authorid":851053756,"lastauditremark":{"common":""}},"lodgeunit"
:{"id":850753910,"title":"中式复式三居150平 整租近机场厦门北"},"authorid":853649835,"checkindayStr":"2017年03月"},{"id"
:850754569,"content":"不懂在哪里发图啊啊啊，不过看房东展示的图片也一样啦，真的是太太太赞了。出来旅游这么多天，这是住的最好的一套了。从进门说起，客厅宽敞，厨房里的锅，案板，筷子等
等，全新。电视很大，沙发很软，全新。房子是复式的，楼下一个卧室，楼上两个卧室，每个卧室都配有一个卫生间，全新。全新。全新。去之前房东发了定位，挺好找的。全家旅行的必选房屋啊！性价比太高了！好像就一直住
在这里…重点是，房东人还超级超级好，还请我们吃饭？！！我真不是托，推荐推荐推荐，是一套绝对不会让你失望的房子。","landlordid":851053756,"createtime":"2017-03-24
 18:27","bookorderid":851295072,"tenantid":853427681,"lodgeunitid":850753910,"appraise":{"sanitation"
:3,"security":3,"description":3,"location":3,"performance":3},"timeString":"2017-03-24 18:27","tenant"
:{"id":853427681,"nickname":"用户6321","headimageurl":"https://i1.mayi.com/gjfs15/M05/C7/30/CgEHP1cxdOXUq
,Q-AABd0owykF8682.jpg_54x54c.jpg","mobile":"15860706321","shownickname":"158****6321","realname":"15860706321"
,"createtime":"2017-03-06 23:01","username":"m#_853427681","areacode":"86","fastpaystate":0,"serverid"
:1,"otherareacode":"86","lastvisittime":"2017-04-14 21:34","estate":"valid"},"recomment":{"property"
:1,"id":850782761,"state":2,"content":"蚂蚁没办法发图，感谢对我房子的认可，您住的舒适是最重要的，欢迎下次再来。","landlordid":851053756,"lastaudittime"
:"2017-03-30 11:42","createtime":"2017-03-29 23:03","bookorderid":851295072,"tenantid":853427681,"lodgeunitid"
:850753910,"title":"蚂蚁没办法发图，感谢对我房子.........","lastupdatetime":"2017-03-29 23:03","appraise":{"sanitation"
:3,"security":3,"description":3,"location":3,"performance":3},"lastauditid":1170,"prarentcommentid":850754569
,"authorid":851053756,"lastauditremark":{"common":""}},"lodgeunit":{"id":850753910,"title":"中式复式三居150
平 整租近机场厦门北"},"authorid":853427681,"checkindayStr":"2017年03月"},{"id":850675301,"content":"房子大，交通方便，主要
是厨房用起来非常方便，调味料具全，公务出差＋旅游","landlordid":851053756,"createtime":"2017-03-04 18:54","bookorderid":851152686
,"tenantid":851335063,"lodgeunitid":850628400,"appraise":{"sanitation":3,"security":3,"description":3
,"location":3,"performance":3},"timeString":"2017-03-04 18:54","tenant":{"id":851335063,"nickname":"
用户5898","headimageurl":"https://i1.mayi.com/gjfs15/M06/C7/19/CgEHQFcxdRfgpEk7AABlcbtch,g688.jpg_54x54c
.jpg","mobile":"13815275898","shownickname":"138****5898","realname":"13815275898","createtime":"2016-03-13
 11:28","username":"m#_851335063","areacode":"86","fastpaystate":0,"serverid":1,"otherareacode":"86"
,"lastvisittime":"2017-03-28 22:28","estate":"valid"},"recomment":{"property":1,"id":850676707,"state"
:2,"content":"感谢，你住的舒适方便最重要，欢迎下次再来厦门旅游。","landlordid":851053756,"lastaudittime":"2017-03-06 09:34","createtime"
:"2017-03-05 20:13","bookorderid":851152686,"tenantid":851335063,"lodgeunitid":850628400,"title":"感谢
，你住的舒适方便最重要，.........","lastupdatetime":"2017-03-05 20:13","appraise":{"sanitation":3,"security":3,"description"
:3,"location":3,"performance":3},"lastauditid":1170,"prarentcommentid":850675301,"authorid":851053756
,"lastauditremark":{"common":""}},"lodgeunit":{"id":850628400,"title":"近厦门北站 机场 园博苑大三居整租"},"authorid"
:851335063,"checkindayStr":"2017年02月"},{"id":850660836,"content":"房间很好.干净大方.小区建设很棒.厨房都能用.还和朋友们做了顿饭吃.
房东先生人很好.落下东西了还帮快递.有机会来还会住！","landlordid":851053756,"createtime":"2017-02-20 22:18","bookorderid":851144896
,"tenantid":851500842,"lodgeunitid":850628400,"appraise":{"sanitation":3,"security":3,"description":3
,"location":3,"performance":3},"timeString":"2017-02-20 22:18","tenant":{"id":851500842,"nickname":"
用户4775","headimageurl":"https://i1.mayi.com/gjfs15/M06/C7/19/CgEHQFcxdRfgpEk7AABlcbtch,g688.jpg_54x54c
.jpg","mobile":"18635594775","shownickname":"186****4775","realname":"18635594775","createtime":"2016-04-13
 20:16","username":"m#_851500842","areacode":"86","fastpaystate":0,"serverid":1,"otherareacode":"86"
,"lastvisittime":"2017-02-11 20:39","estate":"valid"},"recomment":{"property":1,"id":850670838,"state"
:2,"content":"应该的，欢迎下次再来厦门旅游。","landlordid":851053756,"lastaudittime":"2017-03-01 15:04","createtime"
:"2017-03-01 12:42","bookorderid":851144896,"tenantid":851500842,"lodgeunitid":850628400,"title":"应该
的，欢迎下次再来厦门旅游。","lastupdatetime":"2017-03-01 12:42","appraise":{"sanitation":3,"security":3,"description"
:3,"location":3,"performance":3},"lastauditid":2490,"prarentcommentid":850660836,"authorid":851053756
,"lastauditremark":{"common":""}},"lodgeunit":{"id":850628400,"title":"近厦门北站 机场 园博苑大三居整租"},"authorid"
:851500842,"checkindayStr":"2017年02月"}]},"totalCount":22,"pageNo":1,"totalPage":3.0}';

  $json_arr= json_decode($json, true); 
  header('Content-type:text/json'); 
  echo  json_encode($json_arr);





 }    
 
}

?>