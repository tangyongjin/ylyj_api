<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller
{
    function test()
    {
        $user  = $this->db->get_where('user', array('eid' => 'hello57'))->row_array();
        debug($user);

        
    }


   function test_deploy()
    {
     

        $this->clean('test_deploy');
       
        $eid='test_deploy';

        $session_id           = uniqid('', 1);
        $regcfg['session_id'] = $session_id;
         

        $row               = array();
        $row['session_id'] = $session_id;
        $row['eid']        = $eid;
        $row['password']   = 'password';
        $row['email']      = 'tang@a.com';
        $row['ename']      = 'ename';
        $row['ip']      = $this->input->ip_address();
      

       // debug($row);

        $this->deploy($row);
    }    


   function clean($eid) {

    
    
        $sql="drop database $eid";
        $this->db->query($sql);
 

        $sql="delete from deploy_log where  eid='$eid' ";
        $this->db->query($sql);

        
        $sql="delete from mysql.user where user='$eid' ";
        $this->db->query($sql);



        $sql="delete from user where eid='$eid' ";
        $this->db->query($sql);




        $sql = "FLUSH PRIVILEGES ";
        $this->db->query($sql);
        
        
    }    


    function sync_to_bbs($row){

        $bbs_database = $this->config->item('bbs_database');
        $use_sql   = "use $bbs_database";
        $this->db->query($use_sql);

        $username        =   $row['eid'];
        $password   = $row['password'];
        $email      = $row['email'];
        $salt      = $row['salt'];
        $ip=$row['ip'];
        $regdate= time();

        $sql_sync="INSERT INTO pre_ucenter_members(username,password,email,regip,regdate,lastloginip,lastlogintime,salt) ";
        $sql_sync.="values ( '$username','$password','$email','$ip',$regdate,0,0,'$salt' ) ";  
        $this->db->query($sql_sync);
    }



    function simple_deploy($row)
    {

        $this->session->unset_userdata('taskid');
        $this->session->unset_userdata('task_appfolder');
        $this->session->unset_userdata('task_db');
        $this->session->unset_userdata('task_config');
        $this->session->unset_userdata('task_table_tpl');



        
        $cloudfolder = $this->config->item('cloudfolder');
        $eid         = $row['eid'];
        $ename       = $row['eid'];
        $db_password = substr(uniqid(rand()), -6);
        $this->createAppFolder($cloudfolder, $row['eid']);
        $sql_db = $this->createNewDb($row['eid'], $db_password);
        
        $this->createTables($row['eid'], $row['eid']);

        $dbname = $row['eid'];
        $sql    = "GRANT ALL PRIVILEGES ON $dbname.* TO $dbname";
        $this->db->query($sql);
        $sql = "flush PRIVILEGES ";
        $this->db->query($sql);
        
        $this->write_config($cloudfolder, $row['eid'], $db_password);
        $ret = array(
            'result_code' => 0
        );
        echo json_encode($ret);
    }
    




    function deploy($row)
    {

        $this->session->unset_userdata('taskid');
        $this->session->unset_userdata('task_appfolder');
        $this->session->unset_userdata('task_db');
        $this->session->unset_userdata('task_config');
        $this->session->unset_userdata('task_table_tpl');
        $app_admin_pwd   = $row['password'];
        $r               = $this->salt_pwd($app_admin_pwd);
        $row['password'] = $r['password'];
        $row['salt']     = $r['salt'];

        $this->db->insert('user', $row);
        
        $code = mysql_errno();
        $msg  = mysql_error();
        
        if ($code == 0) {
 


            
            $cloudfolder = $this->config->item('cloudfolder');
            $eid         = $row['eid'];
            $ename       = $row['ename'];
            $db_password = substr(uniqid(rand()), -6);
            $this->createAppFolder($cloudfolder, $row['eid']);
            $sql_db = $this->createNewDb($row['eid'], $db_password);
            $this->createTables($row['eid'], $row['ename']);
            $dbname = $row['eid'];
            $sql    = "GRANT ALL PRIVILEGES ON $dbname.* TO $dbname";
            $this->db->query($sql);
            $sql = "flush PRIVILEGES ";
            $this->db->query($sql);
            
            $this->write_config($cloudfolder, $row['eid'], $db_password);
            $ret = array(
                'result_code' => 0
            );
 
            $this->sync_to_bbs($row);
            echo json_encode($ret);
        } else 
        {

            $ret = array(
                'result_code' => $code,   // 1062, 已经存在
                'errmsg' => $msg,
                'session_id' => $row['session_id']
            );
            echo json_encode($ret);
        }
        
    }
    
    
    function salt_pwd($pwd)
    {
        $salt          = substr(uniqid(rand()), -6);
        $pwd_with_salt = md5(md5($pwd) . $salt);
        return array(
            'salt' => $salt,
            'password' => $pwd_with_salt
        );
    }
    
    function createAppFolder($cloudfolder, $enterprise_id)
    {
        date_default_timezone_set('PRC');
        $session_id = $this->session->userdata('session_id');
        
        
        $this->load->model('i18n');
        $lang = $this->i18n->get_current_locale();
        $this->lang->load('messages', $lang);
        
        $row = array(
            'eid' => $enterprise_id,
            'session_id' => $session_id,
            'task_id' => 1,
            'task_name' => $this->lang->line('task_appfolder'),
            'task_msg' => $this->lang->line('create_folder_and_unziping'),
            'status' => false,
            'ts' => date('Y-m-d H:i:s:ms'),
            'complete_percent' => '12.5%'
        );
        $this->db->insert('deploy_log', $row);
        
        $webroot = $this->config->item('webroot');
        $zipfile = $this->config->item('zipfile');
        unzip($webroot . $zipfile, $cloudfolder . '/' . $enterprise_id . "/", true, true);
        $row['status']           = true;
        $row['ts']               = date('Y-m-d H:i:s:ms');
        $row['complete_percent'] = '25%';
        $this->db->insert('deploy_log', $row);
        
    }
    
    
    function getSqlFromTemplate($db)
    {
        $this->load->model('i18n');
        $lang = $this->i18n->get_current_locale();
        $this->lang->load('messages', $lang);
        
        
        $sessid = $this->session->userdata('session_id');
        $row    = array(
            'eid' => $db,
            'session_id' => $sessid,
            'task_id' => 2,
            'task_name' => $this->lang->line('task_table_tpl'),
            'task_msg' => $this->lang->line('get_tpl_sql'),
            'status' => false,
            'complete_percent' => '37.5%',
            'ts' => date('Y-m-d H:i:s:ms')
        );
        $this->db->insert('deploy_log', $row);
        
        
        $this->db = $this->load->database('nanx_template', true);
       
        $this->load->dbutil();
        $prefs = array(
            'format' => 'txt',
            'filename' => 'nanx_template.sql',
            'add_drop' => true,
            'add_insert' => true,
            'newline' => "\n"
        );
        
        $backup =& $this->dbutil->backup($prefs);
        
        $this->db                = $this->load->database('default', true);
        $row['status']           = true;
        $row['ts']               = date('Y-m-d H:i:s:ms');
        $row['complete_percent'] = '50%';
        
        $this->db->insert('deploy_log', $row);
        
        return $backup;
    }
    
    
    
     function createTables($eid, $title)
    {
        
        $this->load->model('i18n');
        $lang = $this->i18n->get_current_locale();
        $this->lang->load('messages', $lang);
        
        $dbname='cloud_'.$eid;
        
        
        $sql    = $this->getSqlFromTemplate($eid);
        $sessid = $this->session->userdata('session_id');
        $row    = array(
            'eid' => $eid,
            'session_id' => $sessid,
            'task_id' => 3,
            'task_name' => $this->lang->line('task_db'),
            'complete_percent' => '62.5%',
            'task_msg' => $this->lang->line('create_db_and_tables'),
            'status' => false,
            'ts' => date('Y-m-d H:i:s:ms')
        );
        $this->db->insert('deploy_log', $row);
    

        $user  = $this->db->get_where('user', array('eid' => $eid))->row_array();
        $password_salted=$user['password'];
        $salt=$user['salt']; 



        $sql_array = explode(";", $sql);

        $use_sql   = "use $dbname";
        $this->db->query($use_sql);
        foreach ($sql_array as $sql_line) {
            if (strlen($sql_line) > 10) {
                $this->db->query($sql_line);
            }
        }


        $sql_update1 = "update nanx_system_cfg set config_value='" . $title . "'  where `config_key`='BANNER_TITLE'";
        $this->db->query($sql_update1);
        
        $sql_update2 = "update nanx_system_cfg set config_value='" . $eid . "'  where `config_key`='APP_PREFIX'";
        $this->db->query($sql_update2);
        
 
        
        $this->db->insert('nanx_user', array( 'user'=>$eid,'password'=>$password_salted,'salt'=>$salt,'active'=>'Y'   ));
                

        $this->db->insert('nanx_user_role_assign', array( 'user'=>$eid,'role_code'=>'admin'   ));
        
        
        
        $row['status']           = true;
        $row['ts']               = date('Y-m-d H:i:s:ms');
        $row['complete_percent'] = '75%';
        $this->db                = $this->load->database('default', true);
        $this->db->insert('deploy_log', $row);
        
    }
    
    function write_config($cloudfolder, $enterprise_id, $db_password)
    {
        
        $this->load->model('i18n');
        $lang = $this->i18n->get_current_locale();
        $this->lang->load('messages', $lang);
        
        
        
        $sessid = $this->session->userdata('session_id');
        $row    = array(
            'eid' => $enterprise_id,
            'session_id' => $sessid,
            'complete_percent' => '87.5%',
            'task_id' => 4,
            'task_name' => $this->lang->line('task_config'),
            'task_msg' => $this->lang->line('write_cfg_file'),
            'status' => false,
            'ts' => date('Y-m-d H:i:s:ms')
        );
        $this->db->insert('deploy_log', $row);
        
        
        
        $this->write_configdotphp($cloudfolder, $enterprise_id);
        $this->write_databasedotphp($cloudfolder, $enterprise_id, $db_password);
        $this->write_globaldontjs($cloudfolder, $enterprise_id);
        $row['status']           = true;
        $row['ts']               = date('Y-m-d H:i:s:ms');
        $row['complete_percent'] = '100%';
        $this->db->insert('deploy_log', $row);
        
    }
    
    
    
     
    function createNewDb($eid, $db_pwd)
    {
        
        $dbname='cloud_'.$eid;

        $sql = "   drop  DATABASE  IF  EXISTS $dbname ; ";
        $this->db->query($sql);
        
        $sql = "   CREATE DATABASE  IF NOT EXISTS $dbname default charset utf8 COLLATE utf8_general_ci; ";
        $this->db->query($sql);
        
        $sql="delete from mysql.user where user='$eid' ";
        $this->db->query($sql);

        $sql = "FLUSH PRIVILEGES ";
        $this->db->query($sql);


        $sql = "CREATE USER '" . $eid . "'@'localhost' IDENTIFIED BY '" . $db_pwd . "';";
        $this->db->query($sql);


        $code = mysql_errno();
        $msg  = mysql_error();
        
        $create_result=array(
             'opcode'=>'create user',
             'code'=>$code,
             'msg'=>$msg

            );
       

        $sql = "FLUSH PRIVILEGES ";
        $this->db->query($sql);
        

        
        $code = mysql_errno();
        $msg  = mysql_error();
        
        $sql = "GRANT ALL PRIVILEGES ON $dbname.* TO $eid";
        $this->db->query($sql);
        
        $code = mysql_errno();
        $msg  = mysql_error();
        
        $sql = "flush PRIVILEGES ";
        $this->db->query($sql);
        
        $code = mysql_errno();
        $msg  = mysql_error();
        return  array( 'code' =>$code, 'errmsg' => $msg );
    }
    
    
  

    function write_configdotphp($cloudfolder, $eid)
    {
       
    
            $config=array(); 

            $config['eidfolder']   =$eid;
            $config['subfolder']='cloud';
            $config['webroot']= $_SERVER['DOCUMENT_ROOT'].'/'.$config['subfolder'].'/'.$config['eidfolder'].'/';
            $config['base_url']   ='http://cloud.nan-x.com/'.$eid;
            $config['index_page'] = 'index.php';
            $config['uri_protocol'] = 'AUTO';
            $config['url_suffix'] = '';
            $config['language'] = 'english';
            $config['charset'] = 'UTF-8';
            $config['enable_hooks'] = 'TRUE';
            $config['subclass_prefix'] = 'MY_';
            $config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-,|';

            $config['allow_get_array']      = 'TRUE';
            $config['enable_query_strings'] = 'FALSE';
            $config['controller_trigger']   = 'c';
            $config['function_trigger']     = 'm';
            $config['directory_trigger']    = 'd'; 
            $config['log_threshold'] =0;
            $config['log_path'] = '';
            $config['log_date_format'] = 'Y-m-d H:i:s';
            $config['cache_path'] = '';
            $config['encryption_key'] = 'key_'.$eid; 
            $config['sess_cookie_name']     ='cookie_'.$eid; 
            $config['sess_expiration']      = 7200;
            $config['sess_expire_on_close'] = 'FALSE';
            $config['sess_encrypt_cookie']  = 'FALSE';
            $config['sess_use_database']    = 'TRUE';
            $config['sess_table_name']      = 'ci_sessions';
            $config['sess_match_ip']        = 'FALSE';
            $config['sess_match_useragent'] = 'TRUE';
            $config['sess_time_to_update']  = 300;
            $config['cookie_prefix']    = "";
            $config['cookie_domain']    = "cloud.nan-x.com";
            $config['cookie_path']      = "/";
            $config['cookie_secure']    = 'FALSE';
            $config['global_xss_filtering'] = 'FALSE';
            $config['csrf_protection'] = 'FALSE';
            $config['csrf_token_name'] = 'csrf_test_name';
            $config['csrf_cookie_name'] = 'csrf_cookie_name';
            $config['csrf_expire'] = 7200;
            $config['compress_output'] = 'FALSE';
            $config['time_reference'] = 'local';
            $config['rewrite_short_tags'] = 'FALSE';
            $config['proxy_ips'] = '';
            $config['debug'] = 'FALSE';
  
            $this->load->helper('file');
            $webroot    = $this->config->item('webroot');
            $config_php = $cloudfolder . '/' . $eid . "/application/config/config.php";
             
            $keys=array_keys($config);
           
            $data="<?php \n";

            $arr_no_comma=array('enable_hooks','allow_get_array','enable_query_strings','debug','sess_expire_on_close',
                'sess_encrypt_cookie','sess_use_database','sess_match_ip','sess_match_useragent','cookie_secure',
                'global_xss_filtering','csrf_protection','compress_output','rewrite_short_tags',
                'log_threshold','sess_expiration','sess_time_to_update','csrf_expire');
            foreach ($keys  as $key) {

                if(  in_array($key,  $arr_no_comma)  ){
                       $data.='$config[\''.$key.'\']='. $config[$key] . ";\n";
                }
                    else
                {
                         $data.='$config[\''.$key.'\']=\''. $config[$key] . "';\n";
                }
                
             }
             $data.='?>';

            write_file($config_php, $data, 'w');
      

    }
    
    
    function write_globaldontjs($cloudfolder, $enterprise_id)
    {
        
        $webroot  = $this->config->item('webroot');
        $webroot  = $this->config->item('webroot');
        $base_url = $this->config->item('base_url');
        $jsfile   = $cloudfolder . '/' . $enterprise_id . '/js/globalvars.js';
        $handle   = fopen($jsfile, "r");
        $content  = '';
        while (($line = fgets($handle, 4096)) !== false) {
            $line = str_replace(' ', '', $line);
            $kv   = explode('=', $line);
            if ($kv[0] == "APP_PREFIX") {
                $line = "APP_PREFIX='" . $enterprise_id . "';".PHP_EOL;
            }
            
            if ($kv[0] == 'BASE_URL') {
                $line = "BASE_URL='".'http://cloud.nan-x.com/'.$enterprise_id . "/';".PHP_EOL;
            }
            
            $content .= $line;
        }
        fclose($handle);
        write_file($jsfile, $content, 'w+');
    }
    
    
    function write_databasedotphp($cloudfolder, $enterprise_id, $db_password)
    {
        $dbname='cloud_'.$enterprise_id;
        $this->load->helper('file');
        $webroot    = $this->config->item('webroot');
        $config_php = $cloudfolder . '/' . $enterprise_id . "/application/config/database.php";
        $handle     = fopen($config_php, "r");
        $output     = '';
        while (($line = fgets($handle, 4096)) !== false) {
            if (strpos($line, "['default']['database']")) {
                $line = "\$db['default']['database']='" . $dbname . "';\n";
            }
            
            if (strpos($line, "['default']['username']")) {
                $line = "\$db['default']['username']='" . $enterprise_id . "';\n";
            }
            
            if (strpos($line, "['default']['password']")) {
                $line = "\$db['default']['password']='" . $db_password . "';\n";
            }
            $output .= $line;
        }
        
        fclose($handle);
        write_file($config_php, $output, 'w');
    }
    
    
    
    
    function registerStatus()
    {
        
        $this->load->model('i18n');
        $lang = $this->i18n->get_current_locale();
        $this->lang->load('messages', $lang);


        $post = file_get_contents('php://input');
        $x    = (array ) json_decode($post);
        $eid  = $x['eid'];
        $start=  $this->lang->line('start');
        $end=    $this->lang->line('end');


        $sql  = "select task_name   ,task_msg,IF(status=0,'$start','$end')  as status,complete_percent  from deploy_log where eid='" . $eid . "' order by pid";
        
        $this->load->library('table');
         
        $this->table->set_heading(array(
            $this->lang->line('task'),
            $this->lang->line('task_msg'),
            $this->lang->line('state'),
            $this->lang->line('percent_completed')
        ));
        
        $query = $this->db->query($sql);
        $tmpl  = array(
            'table_open' => '<table class= "thin-table"  >'
        );
        $this->table->set_template($tmpl);
        $table_html = $this->table->generate($query);
        $ret        = array(
            'log' => $table_html,
            'num_rows' => $query->num_rows
        );
        echo json_encode($ret);
    }
    
    
    function getSaltPwd($pwd)
    {
        $salt              = substr(uniqid(rand()), -6);
        $new_pwd_with_salt = md5(md5($pwd) . $salt);
        return array(
            'salt' => $salt,
            'password' => $new_pwd_with_salt
        );
    }
    
    
    function resetpwd()
    {
        $post              = file_get_contents('php://input');
        $p                 = (array) json_decode($post);
        $eid               = $p['eid'];
        $new_pwd           = $p['password'];
        $r                 = $this->getSaltPwd($new_pwd);
        $salt              = $r['salt'];
        $new_pwd_with_salt = $r['password'];
        $this->db->where('eid', $eid);
        $this->db->update('user', array(
            'password' => $new_pwd_with_salt,
            'salt' => $salt
        ));
        $code = mysql_errno();
        $ret  = array(
            'result_code' => $code
        );
        echo json_encode($ret);
    }
    
    
    function login()
    {
        $post = file_get_contents('php://input');
        $p    = (array) json_decode($post);
        $u    = array(
            'eid' => $p['eid']
        );
        
        
        $result = array();
        $db_row = $this->db->select('eid,salt,comment,password')->get_where('user', $u)->result_array();
        if (sizeof($db_row) == 1) {
            $salt          = $db_row[0]['salt'];
            $pwd_db        = $db_row[0]['password'];

            $comment       = $db_row[0]['comment'];
            if($comment=='UC'){
             $user_type='bbs';
            }else
            {
             $user_type='enterprise';
            }


            $pwd_try       = $p['password'];
            $pwd_with_salt = md5(md5($pwd_try) . $salt);
            if ($pwd_with_salt == $pwd_db) {
                $result['code'] = 0;
                $result['msg']  = 'Success';
                $result['eid']  = $p['eid'];
                $this->session->set_userdata('portal_eid', $p['eid']);
                $this->session->set_userdata('user_type', $user_type);
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
            } else {
                $result['code'] = -1;
                $result['msg']  = 'Fail';
                $result['eid']  = $p['eid'];
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
            }
            
            
        } else {
            $result['code'] = -1;
            $result['msg']  = 'Fail';
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
        }
    }
    
    
    function register()
    {
        $post                 = file_get_contents('php://input');
        $regcfg               = (array ) json_decode($post);
        $session_id           = uniqid('', 1);
        $regcfg['session_id'] = $session_id;
        $ip=$this->input->ip_address();
        $row               = array();
        $deploy_type=$regcfg['version'];
       

        if( $deploy_type=="for_developer"){

            $row['session_id'] = $regcfg['session_id'];
            $row['eid']        = $regcfg['eid'];
            $row['ip']      = $this->input->ip_address();
            $this->simple_deploy($row);
        }
            else
        {
            $row['session_id'] = $regcfg['session_id'];
            $row['eid']        = $regcfg['eid'];
            $row['password']   = $regcfg['password'];
            $row['email']      = $regcfg['email'];
            $row['ename']      = $regcfg['ename'];
            $row['ip']      = $this->input->ip_address();
            $this->deploy($row);
        }
    }


    
    function logout()
    {
        $this->session->unset_userdata('portal_eid');
    }
}

?>