<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Matrix extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function debug($r)
    {
        echo "<pre>";
        print_r($r);
        echo "</pre>";
    }

    public function matrix($rows, $extract_col = 'prod', $val_col = 'salenum', $key_col ='sales', $crosstext = 'PROD/NAME')
    {

        if (count($rows) == 0)
        {
            return false;
        }

        $register = array();
        $one_row = $rows[0];
        $all_cols = array_keys($one_row);


        $extract_col_value_list = array();
        foreach ($rows as $one_row)
        {
            $extract_col_value = $one_row[$extract_col];
            $extract_col_value_list[] = $extract_col_value;
        }
        $extract_col_value_list = array_unique($extract_col_value_list);
        array_unshift($extract_col_value_list, $crosstext);
        $new_rows = array();
        foreach ($rows as $one_row)
        {
            $new_row = array();
            foreach ($extract_col_value_list as $one_key)
            {
                if ($one_key == $crosstext)
                {
                    $new_row[$crosstext] = $one_row[$key_col];
                } else
                {
                    if ($one_key == $one_row[$extract_col])
                    {
                        $used_key = $one_key;
                        $used_value= $one_row[$val_col];
                        $new_row[$used_key] = $used_value;
                    
                    } else
                    {
                        $new_row[$one_key] = '';
                    }
                }
            }
            
            $find_exists = array_search($one_row[$key_col],$register);
            if (false === $find_exists)
            {
                $new_rows[] = $new_row;
                $register[] = $one_row[$key_col];
            } else
            {
                $new_rows[$find_exists][$used_key] = $used_value;
            }
        }

        $this->debug($new_rows);
    }

    public function index()
    {
        $rows = $this->db->get('salerecord')->result_array();
        $this->matrix($rows, $extract_col = 'prod', $val_col = 'salenum', $key_col ='sales', $crosstext = 'PROD/NAME');
    }


}

?>