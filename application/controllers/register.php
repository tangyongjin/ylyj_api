<?php 
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register extends CI_Controller {
	  
    function index() 
    {
      $this->load->library('parser');     
      $html['header']= $this->load->view('header','',true );
      $html['content']= $this->load->view('reg_form','',true );
      $html['footer']= $this->load->view('footer','',true );
	    $this->parser->parse('template' ,$html);
    }
}
?>

