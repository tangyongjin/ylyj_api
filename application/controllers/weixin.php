 <?php

class Weixin extends CI_Controller
{
    
    public function __construct()
    {
        
      
        
        $this->appid  = "wxcff17cde4c7b60f8";
        $this->appsec = "81d9a2a4e6a37bfd8e59d0952d93c629";
        
        
        $this->token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&";
        
        // 根据code取token
        
        $this->oath_token_url  = "https://api.weixin.qq.com/sns/oauth2/access_token?";
        $this->qrcode_get_url  = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=";
        $this->getuserinfo_url = 'https://api.weixin.qq.com/sns/userinfo?';
        /*以下为微信支付用*/

        require_once APPPATH . "../weixin/pay/lib/WxPay.Api.php";
        require_once APPPATH . "../weixin/pay/" . "log.php";
        require_once APPPATH . "../weixin/pay/" . "WxPay.NativePay.php";

        

        parent::__construct();
        
    }
    

    //原样返回微信来的echostr,
    //公众平台--开发----基本配置----服务器配置用 
    public function  echotoken(){


       $args = getargs();
       $echostr =$args['echostr'];
       ob_clean();
       echo $echostr;
       die;


    }

   public function abc(){
    echo 'abc';
   }


    //微信登录二维码
    public function auth()
    {


        // echo 111;die;

        header("Access-Control-Allow-Origin: * ");
        ob_clean(); //必须加上,否则有可能出不来图像
        $args = getargs();

        
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $wx_transaction_id = $args['wx_transaction_id'];
        $cfg = array(
            'appid' => 'wxcff17cde4c7b60f8',
            'redirect_uri' => 'http://www.yiluyiju.com/api/weixin/auth_callback',
            'response_type' => 'code',
            'state' => $wx_transaction_id,
            'scope' => 'snsapi_userinfo'
        );


        
        
        $query = http_build_query($cfg);
        $wxurl = "https://open.weixin.qq.com/connect/oauth2/authorize?" . $query . '#wechat_redirect';
        header('Content-type: image/png');
        QRcode::png($wxurl);
        // ob_end_clean()  
        die;
    }
    
   
  
     

    public function auth_callback()
    {
        
        // 如果用户同意授权，页面将跳转至 redirect_uri/?code=CODE&state=STATE。
        $args = getargs();
        if (!array_key_exists('code', $args)) {
            // 用户不同意授权.
        }
        
        
        
        $autho_code = $args['code'];
        $transaction_id = $args['state'];
        $oauth_res = $this->xweixin->gettoken_by_code($autho_code);
        $oauth_access_token = $oauth_res['access_token'];
        $openid             = $oauth_res['openid'];
        $uerifo = $this->getuserinfo($oauth_access_token, $openid);
    
        
        if (!db_exits('room_user', 'wx_unionid', $uerifo['unionid'])) 
        {
            $this->xuser->save_double_user($uerifo);
        }
        
     
        $userinfo = $this->db->get_where('room_user', array(
            'wx_unionid' => $uerifo['unionid']
        ))->row_array();
        
        //帮助用户登录
        $ret = $this->xuser->setsession($userinfo, $transaction_id);
    }
    
    
     
    
    
    public function getuserinfo($access_token, $openid)
    {
        $weixin_ret     = file_get_contents($this->getuserinfo_url . "access_token=$access_token&openid=$openid&lang=zh_CN");
        $weixin_ret_arr = (array) (json_decode($weixin_ret));
        return $weixin_ret_arr;
    }
    
    
    public function checkauth()
    {   
        echo 111;
        getargs();
    }
    
    
    public function gettoekn()
    {
        
        $token_try = $this->get_wx_db_token();
        
        if ($token_try == 'blank' || $token_try == 'expired') {
            $this->refreshtoekn();
            return $this->get_wx_db_token();
        } else {
            return $token_try;
        }
    }
    
    public function createNonceStr($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str   = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }
    
    
    //从数据库查找weixin token,如果过期则返回false
    public function get_wx_db_token()
    {
        
        $sql  = "select * from  room_weixin_token  order by id desc limit 1";
        $data = $this->db->query($sql)->row_array();
        if (empty($data)) {
            return 'blank';
        }
        
        /*
        判断 access_token是否过期,给出1分钟缓冲
        */
        if (($data['expires_in'] + $data['inserttime'] + 60) < time()) {
            return 'expired';
        }
        
        return $data['access_token'];
        
    }
    
    public function refreshtoekn()
    {


        
        
        $appid          = $this->appid;
        $secret         = $this->appsec;
        $weixin_ret     = file_get_contents($this->token_url . "appid=$appid&secret=$secret");
        $weixin_ret_arr = (array) (json_decode($weixin_ret));
        debug($weixin_ret);
        debug((array) $weixin_ret_arr);
        $inserttime     = time();
        $inserttime_str = date("Y-m-d H:i:s", time());
        $this->db->insert('room_weixin_token', array(
            'access_token' => $weixin_ret_arr['access_token'],
            'expires_in' => $weixin_ret_arr['expires_in'],
            'inserttime' => time(),
            'inserttime_str' => date("Y-m-d H:i:s", time())
        ));
        
    }
    
    
    function http_post_data($url, $data_string)
    {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Content-Length: ' . strlen($data_string)
        ));
        ob_start();
        curl_exec($ch);
        if (curl_errno($ch)) {
            $this->ErrorLogger('curl falied. Error Info: ' . curl_error($ch));
        }
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        return array(
            $return_code,
            $return_content
        );
    }
    
    
    
    /*********微信支付*******/


     public function testpay()
    {

 
      
        $orderid=23;
         
        $order=$this->xorder->orderdetail($orderid) ;
         
        $this->xweixin->paypic($order);
    }
    


    //生成微信扫码支付二维码
    public function wxqrcode()
    {


        header("Access-Control-Allow-Origin: * "); 
        $args=getargs();

        $seqno=$args['seqno'];

        $this->db->where('seqno',$seqno);
        $row= $this->db->get('room_order_history',1 )->row_array();
        $orderpid=$row['pid'];
 
        $order=$this->xorder->orderdetail($orderpid) ;
         
        $this->xweixin->paypic($order);
    }

   
   //扫码回调连接
   public function wxqrcodecallback()
    {

        echo "";
        $args=getargs();

    }
   
   public function myErrorHandler(){

   }

   public function  test1(){
         $args=getargs();
         debug($xyz);
         $this->xorder->orderPostProcess_2222();
       
   }
 


    
    //微信扫码支付通知接口.
    public function wxpay_notify(){
     
         $args=getargs();
     
    
         $payresult       = new WxPayDataBase();
         $xml_from_weixin = $GLOBALS['HTTP_RAW_POST_DATA'];
         logtext( htmlspecialchars($xml_from_weixin));
         $retarr          = $payresult->FromXml($xml_from_weixin);
         logtext($retarr);

         //告诉微信,已经收到通知,不需要再次发送

     
         ob_clean(); //防止前面出现的字符串
         $xml_to_weixin = '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
         echo $xml_to_weixin;
         
        
         ob_start();
         //根据微信传来的数据更改订单信息  
         //step1: 先查看 transaction_id 对应的支付是否真正成功.
         $doublecheck=$this->doublecheck($retarr['transaction_id']);
         $retarr['doublecheckresult'] = $doublecheck;
         
         //然后根据 out_trade_no 修改微信支付数据.
         $this->xweixin->update_payinfo($retarr);

         if($doublecheck=='yes'){
            $orderstate='success' ;
         }else
         {
            $orderstate='fail' ;
         }
     
         //订单后期处理 , 只能发一次!
         //$retarr['attach']为订单 pid ,在生成订单时候送给微信,微信原样返回. 

          $this->xorder->orderPostProcess( $retarr['attach'],$orderstate);
          $run_result = ob_get_contents();
          logtext($run_result);
          ob_end_clean();
    }
    

    //检查是否真正的付款成功.
    public function doublecheck($transaction_id)
    {
        
        $input = new WxPayOrderQuery();
        $input->SetTransaction_id($transaction_id);
        $result = WxPayApi::orderQuery($input);
        
        if (array_key_exists("return_code", $result) && array_key_exists("result_code", $result) && $result["return_code"] == "SUCCESS" && $result["result_code"] == "SUCCESS") {
            return 'yes';
        }else
        {
            return 'no';
        }
    }

    //退款

    public function  wxrefund(){
      
      $args=getargs();

      $seqno=$args['seqno'];

 
      $payinfo = $this->db->select()->from('room_wx_order_pay')->where('room_order_serial', $seqno)->get()->row_array();
    
     
       
      $out_trade_no=$payinfo['out_trade_no'];

      $fee=$payinfo['total_fee'];
      

      $refund_reuslt=$this->xweixin->wxrefund($out_trade_no,$fee);
      
      logtext($refund_reuslt);

      
      if( ('SUCCESS'==$refund_reuslt['result_code'])&&('SUCCESS'==$refund_reuslt['return_code'] )){
           $this->db->where('seqno',$seqno);
           $this->db->update('room_order_history',array('orderstate'=>'refunded' ));

           // $msg = $this->db->_error_message();
           // $num = $this->db->_error_number();
          // echo $msg;

       

      } 

          $ret['code'] = 1;
          $ret['msg']=$refund_reuslt['return_msg'];
          jsonoutput($ret);

      



    }

    
}
?> 