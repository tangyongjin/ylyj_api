<?php
class Order extends CI_Controller
{


   
   function check_room_aviable($roomid){
       //检查房间是否可用,防止中间已经被定走.


      return true;
       
   }



   function send_room_pwd(){

         error_reporting(E_ALL);
         ini_set('display_errors', 1);
         $mob='18600089281';
         $args=array('room_pwd'=>"908971",'start'=>'2017年8月23日12:21','end'=>'2017年8月25日12:21'); 
         $msg=$this->xmobile->get_tpl('featurepwd',$args);
         $this->xmobile->send_text_sms($mob,$msg,null);


   }


   function cancel(){

        header("Access-Control-Allow-Origin: * "); 
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept"); 

        $args=getargs();
        $seqno=$args['seqno'];
        $this->db->where('seqno', $seqno);
 
        $this->db->update('room_order_history',array('orderstate'=>'cancel'));
        $ret=array(
                    'code'=>0,
                    'errmsg'=>'订单取消成功',
                    'seqno'=>$seqno
                  );

        jsonoutput($ret);
   }
   


   function listorder(){

         
          $args=getargs();
          $userid=$args['userid'];
          $sql="select  room_order_history.*,roomtitle from room_order_history,room_source where userid=$userid
                and sourceid=room_source.pid order by  room_order_history.pid desc";
          
          $orders=$this->db->query($sql)->result_array();
          foreach ($orders as $key => $value) { 
             $orderdetail =$this->xorder->orderdetail( $orders[$key]['pid']  );
             $orderdetail=$this->xorder->fix_orderdetail($orderdetail); 
             $orders[$key]=$orderdetail;
          }

          jsonoutput($orders);
   }


   // 生成订单
   function addorder(){

        
        $args=getargs();
       
        //获取订单价格
        $price = $this->xorder->getroomprice($args);
        $order=array( 
          'userid'=>$args['userid'],
          'sourceid'=>$args['roomid'],
          'startdate'=>    $args['starttime'], 
          'enddate'=>      $args['endtime'], 
          'createtime'=>date("Y-m-d H:i:s", time()),
          'ordermoney' =>$price,
          'orderstate'=>'ini',
          'seqno'=>date("Ymd", time()).'-'.randstr(11)
        );
        

        if( $this->check_room_aviable($args['roomid'])  ){
                 $this->db->insert( 'room_order_history' ,$order );

                  $orderdetail=$this->xorder->infoByseqno($order['seqno']);
                  $ret=array(
                    'code'=>0,
                    'errmsg'=>'房间预订成功,请尽快付款',
                    'seqno'=>$order['seqno'],
                    'orderdetail'=>$orderdetail

                  );

                  jsonoutput($ret);
        } 
        else
        {
                 $ret=array(
                    'code'=>1,
                    'errmsg'=>'房间已经被别人抢走,请重新选择你心仪的房间'
                  );
                 jsonoutput($ret);
        }
   }

  function refund(){
     header("Access-Control-Allow-Origin: * "); 
     $args=getargs();
  }

 

  function resendpwd(){

      $args=getargs();
      $seqno=$args['seqno'];

      $orderpid=db_col_by_col('room_order_history','seqno',$seqno,'pid') ; 

      $sql="select * from room_sms where orderpid=$orderpid order by pid desc  limit 1";
      $row=$this->db->query($sql)->row_array();
      $msg=$row['msg'];
      $mobile=$row['mobile'];
      $this->xmobile->send_text_sms($mobile, $msg,$orderpid);
  }

  function evaluate(){
               error_reporting(E_ALL);
               ini_set('display_errors', 1);
               header("Access-Control-Allow-Origin: * "); 

               $args=getargs();
               $order=array( 
                    'roomid'=>$args['roomid'],
                    'userid'=>$args['userid'],
                    'createtime'=>date("Y-m-d H:i:s", time()),
                    'content' =>$args['evaluate'],
                    'rank'=>$args['rate']
                  );
               
               $this->db->insert( 'room_comments' ,$order );
               $ret=array(
                    'code'=>0,
                    'errmsg'=>'评价提交成功'
                  );
                jsonoutput($ret);
  }

 
 
   function info(){
       
           header("Access-Control-Allow-Origin: * "); 
           $args=getargs();
           $seqno=$args['seqno'];
           $order_detail=$this->xorder->infoByseqno($seqno);  
           jsonoutput($order_detail);

   }

     
     
}


?>
