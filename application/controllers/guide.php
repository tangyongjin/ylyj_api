<?php 
/*------------------------------------------
/ Author   :
/ DateTime :2012-09-24
/ Desc     :Home Controller 初始页面
------------------------------------------*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Guide	 extends CI_Controller {
	  
    function listsess() 
    {
    	
    	$sess=$this->uri->segment(3);
    	
    	
  
      $this->load->helper('my_pinyin'); 
      $sql="select company from user where session_id='".$sess."' limit 1";
      $res=$this->db->query($sql)->result_array();
      $name=$res[0]['company'];
      $pinyin=pinyin($name);
      
      
      $root=$this->config->item('root_url');
      
      
      $access_url=$root.$pinyin;
          
      
      $this->load->library('parser');     
      $html['header']= $this->load->view('header','',true );
      $guide=$this->load->view('welcome','',true );
      
      $guide=str_replace('{ACCESS_URL}',$access_url,$guide);
      
      $html['content']=$guide;
      
      $html['footer']= $this->load->view('footer','',true );
	    $this->parser->parse('template' ,$html);
    }
    
    
}
?>

