 <?php

class Wxportal extends CI_Controller
{
    
    public function __construct()
    {
        
        
        
        $this->appid  = "wxcff17cde4c7b60f8";
        $this->appsec = "81d9a2a4e6a37bfd8e59d0952d93c629";
        $this->token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&";
        
        // 根据code取token
        $this->oath_token_url  = "https://api.weixin.qq.com/sns/oauth2/access_token?";
        $this->qrcode_get_url  = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=";
        $this->getuserinfo_url = 'https://api.weixin.qq.com/sns/userinfo?';
        /*以下为微信支付用*/
        
        require_once APPPATH . "../weixin/pay/lib/WxPay.Api.php";
        require_once APPPATH . "../weixin/pay/" . "log.php";
        require_once APPPATH . "../weixin/pay/" . "WxPay.NativePay.php";
        require_once APPPATH . "../weixin/pay/" . "WxPay.JsApiPay.php";
        require_once APPPATH . "../weixin/pay/lib/WxPay.Data.php";
        parent::__construct();
        
    }
    
    

    public function getPromotionRooms(){
    
          $args = getargs();
          $rooms=  $this->xroom->allrooms('001001001','1','6');
          debug($rooms);
    }

      
    public function showPromotionRooms(){
          $args = getargs();
          header("Content-type: text/html; charset=utf-8");
          $rooms=  $this->xroom->allrooms('001001001','1','6');
        


          $data = array('rooms' =>  $rooms );
          $html = $this->zaq->parse('weixin/roomlist', $data, TRUE);
          echo $html;

    }
 


   public function tryorder(){
     
       header("Content-type: text/html; charset=utf-8");
       session_start();
       $args = getargs();
       
       $roomid=$args['roomid'];

       
       if (isset($_SESSION['unionid'])) {
                
                $unionid = $_SESSION['unionid'];
                $sql  = "select * from room_user where wx_unionid ='$unionid'";
                $user = $this->db->query($sql)->row_array();

                $userid=$user['pid'];
                $room=$this->xroom->roomdetail($roomid);
                $room_occupied_data=array(); 
     
                $data = array('room' =>  $room ,'userid'=>$userid, 'roomid'=>$roomid, 'room_occupied_data'=>$room_occupied_data);
                $this->load->view('weixin/tryorder', $data);
       }else{
               $this->refresh_wx_session("http://www.yiluyiju.com/api/wxportal/tryorder?roomid=$roomid");

       }
   
   }
    
    public function showorder()
    {

       
        $userpair = $this->xuser->getpairuidbyunionid($_SESSION['unionid']);
        $sql="select  * from room_order_history o, room_source s where o.sourceid=s.pid and userid in ($userpair) order by o.pid desc";
        $orders = $this->db->query($sql)->result_array();
        $data = array('orders' =>  $orders );
        $html = $this->zaq->parse('weixin/orderlist', $data, TRUE);
        echo $html;
        die;
    }
    
    public function showdetailorder()
    {

        header("Access-Control-Allow-Origin: * ");
        header("Content-Type: text/html;charset=utf-8"); 
        $args = getargs();
        debug($args);
        $seqno=$args['seqno'];
        $sql = "select  * from room_order_history o, room_source s where o.sourceid=s.pid and seqno='$seqno'";
        $order = $this->db->query($sql)->row_array();
        $data = array('order' =>  $order );
        $html = $this->zaq->parse('weixin/orderdetail', $data, TRUE);
        echo $html;
        die;
    }

    

    public function chackmobile(){

        header("Access-Control-Allow-Origin: * ");
        header("Content-Type: text/html;charset=utf-8"); 
        $args = getargs();
       
        $unionid = "o1NaZwra_8OtOV2tQiwx28ogLskw";

        $sql  = "select * from room_user where wx_unionid ='$unionid'";
        $user = $this->db->query($sql)->row_array();
        //debug($user);
         $date =array(
                    
                    'pid'=>'175'
                    );
        if($user['mobile']== null || $user['mobile']==""){
            $html = $this->zaq->parse('weixin/bindmobile', $date, TRUE);
            echo $html;
            die;
        }
       
        
    }


    public function roomdetails(){

        $args = getargs();
        $data = array('rooms' =>  'ooms' );
        $html = $this->zaq->parse('weixin/roomdetail', $data, TRUE);
        echo $html;
        die;


    }

    
    
    //用户同意授权
    //文档来源: 
    // https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1419316518&token=&lang=zh_CN

    public function myorder()
    {
        
        header("Access-Control-Allow-Origin: * ");
        $args = getargs();
       
      
        
        session_start();
        if (isset($_SESSION['unionid'])) {
                $this->showorder();
                die;
        }else{
                $this->refresh_wx_session('http://www.yiluyiju.com/api/wxportal/myorder');
        }
    }



    public function getWxuserinfo(){

         header("Access-Control-Allow-Origin: * ");
         $args = getargs();
       

        if (array_key_exists('code', $_GET)) {

            $autho_code     = $args['code'];
            $transaction_id = $args['state'];
            $oauth_res      = $this->xweixin->gettoken_by_code($autho_code);

            if(  array_key_exists('access_token', $oauth_res)  ){
              
                $access_token = $oauth_res['access_token'];
                $openid       = $oauth_res['openid'];
                $userinfo     = file_get_contents($this->getuserinfo_url . "access_token=$access_token&openid=$openid&lang=zh_CN");
                $userinfo_arr = (array) (json_decode($userinfo));
               
                //如果用户第一次使用该系统,则生成客户档案  
                if (!db_exits('room_user', 'wx_unionid', $userinfo_arr['unionid'])) {
                    $this->xuser->save_double_user($userinfo_arr);
                }
                session_start();
                $_SESSION['unionid'] = $userinfo_arr['unionid'];
                //die;
            } 
        }
    }
   
     

    public  function refresh_wx_session($redir_url){

            if (isset($_SESSION['unionid'])) {
                header("Location: $redir_url");
            }

            $wx_transaction_id = randstr(16);
            $getWxuserinfo="http://www.yiluyiju.com/api/wxportal/getWxuserinfo";
            $cfg = array(
                'appid' => $this->appid,
                'redirect_uri' => $getWxuserinfo,
                'response_type' => 'code',
                'state' => $wx_transaction_id,
                'scope' => 'snsapi_userinfo'
            );
            
            $query = http_build_query($cfg);
            $wxurl = "https://open.weixin.qq.com/connect/oauth2/authorize?" . $query . '#wechat_redirect';
            header("Location: $wxurl");
    }



   public function getWxJspayArgs($preorder){
         
            $JsApiPay=new JsApiPay();
            
            $openid=$JsApiPay->GetOpenid();
            $input=new WxPayUnifiedOrder();

            $input->SetBody("一路一居");
            $input->SetAttach( $preorder['pid'] );

            $out_trade_no = WxPayConfig::MCHID.date("YmdHis");
            $out_trade_no.='_wxjs'; 

            $input->SetOut_trade_no($out_trade_no);

            $input->SetTotal_fee(100*$preorder['ordermoney']);
            $input->SetTime_start(date("YmdHis"));
            $input->SetTime_expire(date("YmdHis", time() + 600));
            $input->SetGoods_tag("ylyj");
           
            $input->SetNotify_url("http://www.yiluyiju.com/api/weixin/wxpay_notify");
            $input->SetTrade_type("JSAPI");
            $input->SetOpenid($openid);
            
            $uniorder = WxPayApi::unifiedOrder($input);
           
            
            $jsApiParameters = $JsApiPay->GetJsApiParameters($uniorder);

    

            $pay=array();
            $pay['appid']=$uniorder['appid'];
            $pay['code_url']='jsnourl';
            $pay['mch_id']=$uniorder['mch_id'];
            $pay['prepay_id']=$uniorder['prepay_id'];
            $pay['trade_type'] = $uniorder['trade_type'];

            $pay['room_order_serial'] = $preorder['seqno'];
                
            $pay['insert_date']=date("Y-m-d H:i:s", time());
                  
            $pay['attach'] =$preorder['pid'];    //orderid
            $pay['orderid'] =$preorder['pid'];   //orderid
            $pay['total_fee'] =100*$preorder['ordermoney'];
            $pay['out_trade_no'] = $out_trade_no;
            $pay['product_id'] = $preorder['sourceid']; //roomid

            //保存用户支付前的付款信息
            $this->db->insert('room_wx_order_pay',$pay);
  

            return  $jsApiParameters;
             

    }

      public  function preorderinfo(){

            header("Access-Control-Allow-Origin: * ");
            $args = getargs();

            $segs = $this->uri->segment_array();
            $seqno=$segs[3];
            $orderinfo=$this->xorder->infoByseqno($seqno);
            $wxjsarg=$this->getWxJspayArgs($orderinfo);
            $data = array('orderinfo' =>  $orderinfo,'jsApiParameters'=>$wxjsarg );
            $this->load->view('weixin/wxjspay', $data);
    }
    

    
}
?> 