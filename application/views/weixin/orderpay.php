<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
  


               <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
              <title>订单详情</title>
              <link rel="stylesheet" href="http://www.yiluyiju.com/php/css/weixin/bootstrap.min.css" crossorigin="anonymous">
              <script src="http://www.yiluyiju.com/php/js/weixin/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
              <script src="http://www.yiluyiju.com/php/js/weixin/popper.min.js"      crossorigin="anonymous"></script>
              <script src="http://www.yiluyiju.com/php/js/weixin/bootstrap.min.js"   crossorigin="anonymous"></script>
    

    <script type="text/javascript">
        //调用微信JS api 支付
        function jsApiCall()
        {
             // 建议使用这中方式发起支付.
            
            WeixinJSBridge.invoke(
			'getBrandWCPayRequest',
			{$jsApiParameters},
			function(res){
			var msg = res.err_msg; 
 
			    if (msg == "get_brand_wcpay_request:ok") { 
			        alert("支付成功，跳转到订单详情页"); 
			        //location.href = "/php/index.php/wxportal/showdetailorder"; 
			    } else { 
			        if (msg == "get_brand_wcpay_request:cancel") { 
			            var err_msg = "您取消了微信支付"; 
			            //location.href = "/php/index.php/wxportal/showdetailorder?seqno={$seqno}";
			        } else if (res.err_code == 3) { 
			            var err_msg = "您正在进行跨号支付正在为您转入扫码支付......"; 
			        } else if (msg == "get_brand_wcpay_request:fail") { 
			            var err_msg = "微信支付失败错误信息：" + res.err_desc; 
			        } else { 
			            var err_msg = msg + "" + res.err_desc; 
			        } 
			        alert(err_msg); 
			    } 
						
		    }
		);
        }




        function callpay()
	{
		if (typeof WeixinJSBridge == "undefined"){
		    if( document.addEventListener ){
		        document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
		    }else if (document.attachEvent){
		        document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
		        document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
		    }
		}else{
		    jsApiCall();
		}
	}
    </script>

    <script type="text/javascript">
	//获取共享地址
	function editAddress()
	{
		WeixinJSBridge.invoke(
			'editAddress',
			 {$editAddress},
			function(res){
				var value1 = res.proviceFirstStageName;
				var value2 = res.addressCitySecondStageName;
				var value3 = res.addressCountiesThirdStageName;
				var value4 = res.addressDetailInfo;
				var tel = res.telNumber;
				
				//alert(value1 + value2 + value3 + value4 + ":" + tel);
			}
		);
	}
	
	window.onload = function(){
		if (typeof WeixinJSBridge == "undefined"){
		    if( document.addEventListener ){
		        document.addEventListener('WeixinJSBridgeReady', editAddress, false);
		    }else if (document.attachEvent){
		        document.attachEvent('WeixinJSBridgeReady', editAddress); 
		        document.attachEvent('onWeixinJSBridgeReady', editAddress);
		    }
		}else{
			editAddress();
		}
	};
	
	</script>




</head>
<body>

  

<ul class="list-group">
  <li class="list-group-item"></li>


 <li class="list-group-item list-group-item-primary"> <h4 class="list-group-item-heading">订单状态 :{order['orderstate']}</h4></li>
  <li class="list-group-item list-group-item-light"><h2>{order['ordermoney']}</h2></li>
  <li class="list-group-item list-group-item-light"><h3>{order['roomtitle']}</h3></li>
  <li class="list-group-item list-group-item-light"> {order['address']}</li>
  <li class="list-group-item list-group-item-light"><h3>入住时间</h3></li>
  <li class="list-group-item list-group-item-light"> {order['startdate']}->{order['enddate']}</li>
  <li class="list-group-item">
  <button type="button" class="btn btn-outline-primary">修改订单</button>
  <button type="button" class="btn btn-outline-danger">取消订单</button>
  <c:if></c:if>
  <button type="button" class="btn btn-outline-primary" onclick="callpay()" >支付订单</button>
</li>
</ul>
 <br/>



</body>

</html>
