<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <title>精选房间</title>


    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
     <!--[if IE]>
      <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
      <![endif]-->
    
    <link href="http://www.yiluyiju.com/assets/css/googlefonts.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    
    
    <link href="http://www.yiluyiju.com/assets/css/essentials.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/layout.css" rel="stylesheet" type="text/css" />


    <link href="http://www.yiluyiju.com/assets/css/header-1.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
    <link href="http://www.yiluyiju.com/assets/css/reset.css" rel="stylesheet" type="text/css" />
  </head>

<body>
 <section  style="background: rgba(97, 103, 109, 0.1);" >
        <div class="container">
 

          <div class="row">


             {foreach rooms as room}
                <div class="col-md-4">
                  <div class="item-box"  style="background:white;border: 2px solid #D0D6DF;">
                    <figure>
                       <a href="http://www.yiluyiju.com/room/{room['pid']}?device=mobile"> 
                            <img alt="" class="img-fluid" src="{$room['roompic']}" width="263" height="147">
                       </a>
                    </figure>

                    <div class="item-box-desc">
                      <h4> {room['cityname']}  {$room['roomtitle']}</h4>
                      
                       <p>
                        {$room['roomdesc']}
                      </p>

                       <ul class="pricetable-items  borded" style="padding-left:0px;padding-top: 10px;">
                              <li class="list-group-item">价格:RMB {room['price_day']}元/天</li>
                              <li class="list-group-item">房间面积:{room['roomsize']}</li>
                              <li class="list-group-item">评分:{room['rank']}</li>
                             
                      
                      </ul>
                       <div class="text-center">
                         <a href="http://www.yiluyiju.com/room/{room['pid']}?device=mobile" class="btn btn-primary  btn-sm">房间详情</a>
                        </div>
                        <br/>
                        <div class="text-center">
                        

                       
                         <a href="http://www.yiluyiju.com/api/wxportal/tryorder?roomid={room['pid']}" class="btn btn-primary  btn-sm">
                         现在订房</a>
                        </div>


                    </div>
                  </div>
                </div>
              {/foreach}

           </div>
 
        </div>
      </section>
  
 
</body>
<html>

 