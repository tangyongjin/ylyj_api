<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <title>付款</title>


    
     <!--[if IE]>
      <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
      <![endif]-->
    
    <link href="http://www.yiluyiju.com/assets/css/googlefonts.css" rel="stylesheet" type="text/css" />

    <link href="http://www.yiluyiju.com/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/essentials.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/layout.css" rel="stylesheet" type="text/css" />

    <link href="http://www.yiluyiju.com/assets/css/header-1.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
    <link href="http://www.yiluyiju.com/assets/css/reset.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/bootstrap.min.js"></script> 
    <script type="text/javascript">

  //调用微信JS api 支付
  function jsApiCall()
  {
    WeixinJSBridge.invoke(
      'getBrandWCPayRequest',
      <?php echo $jsApiParameters; ?>,
      function(res){
        WeixinJSBridge.log(res.err_msg);
        alert("返回结果:"+res.err_msg);

         window.location.href=window.location.origin+'/api/wxportal/myorder/';  
       


      }
    );
  }

  function callpay()
  {
    if (typeof WeixinJSBridge == "undefined"){
        if( document.addEventListener ){
            document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
        }else if (document.attachEvent){
            document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
            document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
        }
    }else{
        jsApiCall();
    }
  }
  </script>
  <script type="text/javascript">
  //获取共享地址
  function editAddress()
  {
  
    // WeixinJSBridge.invoke(
    //   'editAddress',
    //   <?php //echo $editAddress; ?>,
    //   function(res){
    //     var value1 = res.proviceFirstStageName;
    //     var value2 = res.addressCitySecondStageName;
    //     var value3 = res.addressCountiesThirdStageName;
    //     var value4 = res.addressDetailInfo;
    //     var tel = res.telNumber;
        
    //     alert(value1 + value2 + value3 + value4 + ":" + tel);
    //   }
    // );
  
  }
  
  window.onload = function(){
    if (typeof WeixinJSBridge == "undefined"){
        if( document.addEventListener ){
            document.addEventListener('WeixinJSBridgeReady', editAddress, false);
        }else if (document.attachEvent){
            document.attachEvent('WeixinJSBridgeReady', editAddress); 
            document.attachEvent('onWeixinJSBridgeReady', editAddress);
        }
    }else{
      // editAddress();
    }
  };
  
  </script>
    

  

  </head>
<body>


      


<section  style="background: rgba(97, 103, 109, 0.1);" >
        <div class="container">
 

          <div class="row">


          
                <div class="col-md-4">
                  <div class="item-box"  style="background:white;border: 2px solid #D0D6DF;">
                    
                   

                    <div class="item-box-desc">
                      <h4>订单号:<?php  echo $orderinfo['seqno']; ?> </h4>
                      
                      

                       <ul class="pricetable-items  borded" style="padding-left:0px;padding-top: 10px;">
                              <li class="list-group-item">房间:<?php echo $orderinfo['roomtitle'] ?></li>
                              <li class="list-group-item">创建时间: <?php echo $orderinfo['createtime'] ?></li>
                              <li class="list-group-item">订单价格: <?php echo $orderinfo['ordermoney'] ?></li>
                              <li class="list-group-item">开始时间: <?php echo $orderinfo['startdate'] ?></li>
                              <li class="list-group-item">结束时间: <?php echo $orderinfo['enddate'] ?></li>
                      </ul>
                    
                        <br/>
                        <div class="text-center">
                          <button type="button" onclick="callpay()"     class="btn btn-xs btn-primary">微信付款</button>
                         
                          
                        </div>


                    </div>
                  </div>
                </div>
            

           </div>
 
        </div>
      </section>
      
 
    
</body>
<html>

