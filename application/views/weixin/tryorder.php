<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <title>预订房间</title>


    
     <!--[if IE]>
      <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
      <![endif]-->
    
    <link href="http://www.yiluyiju.com/assets/css/googlefonts.css" rel="stylesheet" type="text/css" />

    <link href="http://www.yiluyiju.com/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/essentials.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/layout.css" rel="stylesheet" type="text/css" />

    <link href="http://www.yiluyiju.com/assets/css/header-1.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
    <link href="http://www.yiluyiju.com/assets/css/reset.css" rel="stylesheet" type="text/css" />



    <link href="http://www.yiluyiju.com/assets/js/pickadate.js/lib/themes/default.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/js/pickadate.js/lib/themes/default.date.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/js/pickadate.js/lib/themes/default.time.css" rel="stylesheet" type="text/css" />



    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/pickadate.js/lib/picker.js"></script>
    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/pickadate.js/lib/picker.date.js"></script>
    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/pickadate.js/lib/picker.time.js"></script>
    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/pickadate.js/lib/translations/zh_CN.js"></script>



  </head>
<body>

     
 <section style="padding-top:20">
        <div class="container">

            <div class="heading-title heading-line-single text-center">
            <h3>预订房间</h3>
          </div>
         

          <div class="row pricetable-container">
            <div class="col-md-4 col-sm-4 price-table" style="border: 1px solid #D0D6DF;">
              <h3><?php echo  $room['cityname'] ; echo $room['roomtitle'] ?>   </h3>
              <p> 
                <?php echo $room['price_day']; ?>  
                元/天
                
              </p>
             

               

               <div class="col-md-4 col-sm-4" style="text-align:left;">
                <span>  <?php echo $room['roomdesc']; ?>     <br/></span>
                <br/>                
                <div class="fieldset__wrapper">
                  <label>起始日期</label>  
                  <input id="sd1" name="sd1" class="fieldset__input" type="text" placeholder="入住日期" />
                </div>
                 <br/>

                <div class="fieldset__wrapper">
                   <label>结束日期</label>  
                  <input id="sd2" name="sd2" class="fieldset__input" type="text" placeholder="离开日期" />
                </div>
             </div>

                      
             
             <br/>

              <a href="#" id="orderPreaction"  class="btn btn-primary  btn-md">现在预订</a>
               
            </div>

             
          </div>
                

        </div>
      </section>
      
        <script type="text/javascript">
           var  $input_start=   $('#sd1').pickadate({
            formatSubmit: 'yyyy/mm/dd',
            hiddenPrefix: 'prefix__',
            hiddenSuffix: '__suffix'
          })

           var  $input_end=   $('#sd2').pickadate({
            formatSubmit: 'yyyy/mm/dd',
            hiddenPrefix: 'prefix__',
            hiddenSuffix: '__suffix'
          })


            


         


            $( "#orderPreaction" ).click(function() {
             
                 
                 var roomid= <?php  echo $roomid; ?>;
                 var userid= <?php  echo $userid; ?>;
                 
                 

                 var picker_start = $input_start.pickadate('picker')
                 var picker_end = $input_end.pickadate('picker')


                 starttime=picker_start.get('select', 'yyyy-mm-dd')
                 endtime=picker_end.get('select', 'yyyy-mm-dd')

                 var order_url = window.location.origin+'/api/order/addorder';
                
                 
                 $.post(order_url, { starttime: starttime, endtime: endtime,roomid:roomid,userid:userid })
                .done(function( data ) {

                  if(data.code==0){
                      alert( data.errmsg );
                      window.location.href=window.location.origin+'/api/wxportal/preorderinfo/'+data.seqno;  
                  }

                });
                              



            })

        </script>

    
</body>
<html>

