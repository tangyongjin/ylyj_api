<div class="panel no-border">
    <div class="page-container-responsive space-2">
        <div class="row">
            <div class="col-lg-8">
                <div class="review-wrapper">
                    <div>
                        <div class="row space-2 space-top-8 row-table">
                            <div class="col-md-8 review-header">
                                <div class="va-container va-container-v va-container-h">
                                    <h4 class="col-middle va-bottom review-header-text">
                                        <span class="text_5mbkop-o_O-size_large_16mhv7y-o_O-inline_g86r3e">
                                            <span>{comment_counter}条评价</span>
                                        </span>
                                        <div class="overallStarRating_stars_1cuccsz">
                                            <div class="star-rating-wrapper" aria-label="平均评分是5星（满分为5 星）" role="img">
                                                <div class="star-rating">
                                                    <div class="foreground" aria-hidden="true">
                                                        <span aria-hidden="true">
                                                            <span>
                                                                <i class="icon-star icon icon-babu icon-star-big"></i>
                                                            </span>
                                                            <span>
                                                                <i class="icon-star icon icon-babu icon-star-big"></i>
                                                            </span>
                                                            <span>
                                                                <i class="icon-star icon icon-babu icon-star-big"></i>
                                                            </span>
                                                            <span>
                                                                <i class="icon-star icon icon-babu icon-star-big"></i>
                                                            </span>
                                                            <span>
                                                                <i class="icon-star icon icon-babu icon-star-big"></i>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="background" aria-hidden="true">
                                                        <span aria-hidden="true">
                                                            <span>
                                                                <i class="icon-star icon icon-light-gray icon-star-big"></i>
                                                            </span>
                                                            <span>
                                                                <i class="icon-star icon icon-light-gray icon-star-big"></i>
                                                            </span>
                                                            <span>
                                                                <i class="icon-star icon icon-light-gray icon-star-big"></i>
                                                            </span>
                                                            <span>
                                                                <i class="icon-star icon icon-light-gray icon-star-big"></i>
                                                            </span>
                                                            <span>
                                                                <i class="icon-star icon icon-light-gray icon-star-big"></i>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <span class="h6 hide">
                                                    <small>
                                                        <span></span>
                                                    </small>
                                                </span>
                                            </div>
                                        </div>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-4 review-header">
                                <div class="va-container va-container-v va-container-h">
                                    <div class="va-bottom">
                                        <div class="review-search">
                                            <span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">
                                                <label class="input-placeholder-group review-search-label">
                                                    <span class="input-placeholder-label screen-reader-only">搜索评价</span>
                                                    <div class="input-addon">
                                                        <input class="input-stem review-search-box" placeholder="搜索评价" value="" type="search">
                                                            <i name="remove" class="icon icon-remove input-suffix btn rounded-suffix-icon hide" title="返回全部评价"></i>
                                                        </div>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <hr>
                                </div>
                            </div>
                            <div class="review-main">
                                <div class="review-inner space-top-2 space-2">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="row space-2 col-sm-12">
                                                        <div class="col-sm-6 review-category-label">
                                                            <span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">如实描述</span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="star-rating-wrapper" aria-label="平均评分是5星（满分为5 星）" role="img">
                                                                <div class="star-rating">
                                                                    <div class="foreground" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                    <div class="background" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <span class="h6 hide">
                                                                    <small>
                                                                        <span></span>
                                                                    </small>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row space-2 col-sm-12">
                                                        <div class="col-sm-6 review-category-label">
                                                            <span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">沟通交流</span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="star-rating-wrapper" aria-label="平均评分是5星（满分为5 星）" role="img">
                                                                <div class="star-rating">
                                                                    <div class="foreground" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                    <div class="background" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <span class="h6 hide">
                                                                    <small>
                                                                        <span></span>
                                                                    </small>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row space-2 col-sm-12">
                                                        <div class="col-sm-6 review-category-label">
                                                            <span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">干净指数</span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="star-rating-wrapper" aria-label="平均评分是5星（满分为5 星）" role="img">
                                                                <div class="star-rating">
                                                                    <div class="foreground" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                    <div class="background" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <span class="h6 hide">
                                                                    <small>
                                                                        <span></span>
                                                                    </small>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="row space-2 col-sm-12">
                                                        <div class="col-sm-6 review-category-label">
                                                            <span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">位置便利指数</span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="star-rating-wrapper" aria-label="平均评分是5星（满分为5 星）" role="img">
                                                                <div class="star-rating">
                                                                    <div class="foreground" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                    <div class="background" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <span class="h6 hide">
                                                                    <small>
                                                                        <span></span>
                                                                    </small>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row space-2 col-sm-12">
                                                        <div class="col-sm-6 review-category-label">
                                                            <span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">办理入住</span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="star-rating-wrapper" aria-label="平均评分是5星（满分为5 星）" role="img">
                                                                <div class="star-rating">
                                                                    <div class="foreground" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                    <div class="background" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <span class="h6 hide">
                                                                    <small>
                                                                        <span></span>
                                                                    </small>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row space-2 col-sm-12">
                                                        <div class="col-sm-6 review-category-label">
                                                            <span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">性价比</span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="star-rating-wrapper" aria-label="平均评分是5星（满分为5 星）" role="img">
                                                                <div class="star-rating">
                                                                    <div class="foreground" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-babu icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                    <div class="background" aria-hidden="true">
                                                                        <span aria-hidden="true">
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                            <span>
                                                                                <i class="icon-star icon icon-light-gray icon-star-small"></i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <span class="h6 hide">
                                                                    <small>
                                                                        <span></span>
                                                                    </small>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="review-content">
                                    <div>
                                        <div>
                                            <div id='review-group'>
{comments_html}
</div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="pagination pagination-responsive">
                                                        <ul class="list-unstyled">
                                                            <?php
                            $list='';
                            for ($i=1; $i<=$totalpages; $i++) {
                                if($i==1){$class='active';}else{$class='';}
                                $list.="
                                                            <li class='{$class}'>
                                                                <a  class='reviewpage'   roomid={$roomid}  href='#'>$i</a>
                                                            </li>";
                            }

                            echo $list;
                        ?>
                                                            <li class="next next_page">
                                                                <a href="#" data-prevent-default="true">
                                                                    <span class="screen-reader-only">
                                                                        <span>下一步</span>
                                                                    </span>
                                                                    <i class="icon icon-caret-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row space-top-2">
                                        <div class="col-lg-12">
                                            <p>
                                                <span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">
                                                    <span>该房东的其他房源收到了356条评价。</span>
                                                </span>
                                            </p>
                                            <a href="/users/show/26439805" class="btn view-other-reviews" target="blank">
                                                <span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_bold_153t78d-o_O-inline_g86r3e">
                                                    <span>查看其他评价</span>
                                                </span>
                                            </a>
                                        </div>
                                        <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>