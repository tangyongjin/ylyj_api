
                    <div class="page-container-responsive">
                        <div style="margin-top:48px;margin-bottom:48px;margin-left:0;margin-right:0;">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="row_2h22gn">
                                        <div class="column_1rnz84d-o_O-column-sm-8_er1jl5">
                                            <div class="host-info pull-left">
                                                <div style="margin-top:0;margin-bottom:8px;margin-left:0;margin-right:0;"><span class="text_5mbkop-o_O-size_large_16mhv7y-o_O-inline_g86r3e"><a class="link-reset" href="/users/show/26439805" rel="nofollow"><span>房东：Shanghigh</span></a>
                                                    </span>
                                                </div>
                                                <div>
                                                    <div><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><!-- react-text: 1177 -->上海, 中国<!-- /react-text --><span> · </span><span>注册时间：2015年1月</span></span>
                                                    </div>
                                                    <div>
                                                        <div style="margin-top:16px;margin-bottom:0;margin-left:0;margin-right:0;">
                                                            <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;">
                                                                <div class="inlineBlock_36rlri" style="margin-top:0;margin-bottom:24px;margin-left:0;margin-right:25px;">
                                                                    <a class="link-reset" target="_blank" rel="noopener noreferrer nofollow" href="/users/show/26439805#reviews">
                                                                        <div class="badge_1lhqvjz">
                                                                            <div class="badgePill_186vx4j"><span class="badgePillCount_e296pg">544</span></div>
                                                                            <div class="badgeLabel_1iti0ju"><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><span class="badgePillLabel_mstzcu">条评价</span></span>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <div class="inlineBlock_36rlri" style="margin-top:0;margin-bottom:24px;margin-left:0;margin-right:25px;">
                                                                    <div>
                                                                        <!-- react-empty: 1193 -->
                                                                        <a class="link-reset" target="_blank" rel="noopener noreferrer nofollow">
                                                                            <div class="badge_1lhqvjz" id="verified-id-icon"><img alt="已验证" src="https://z1.muscache.com/airbnb/static/badges/verified_badge-6ee370f5ca86a52ed6198fac858ac1f4.png" width="32" height="32">
                                                                                <div class="badgeLabel_1iti0ju"><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><span class="badgePillLabel_mstzcu">已验证</span></span>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column_1rnz84d-o_O-column-sm-4_bgpauh">
                                            <div class="pull-right">
                                                <div class="media-photo-badge">
                                                    <a href="/users/show/26439805" rel="nofollow" class="media-photo media-round"><img alt="Shanghigh的用户个人资料" class="media-photo media-round" data-pin-nopin="true" src="https://z2.muscache.com/im/pictures/9224a699-7ee6-4d9e-8bda-81749d627652.jpg?aki_policy=profile_x_medium" title="Shanghigh的用户个人资料" width="90" height="90"></a><img src="/static/badges/superhost_photo_badge-a38e6a7d2afe0e01146ce910da3915f5.png" class="superhost-photo-badge" alt="Shanghigh是超赞房东。"></div>
                                            </div>
                                        </div>
                                        <div class="column_1rnz84d-o_O-column-sm-12_1wbopx2">
                                            <div class="react-expandable expanded">
                                                <div class="expandable-content expandable-content-long expandable-content--hasDlsReducedText" style="">
                                                    <div class="simple-format-container">
                                                        <p><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><span>Hi! We are a group of designers living, working and enjoying Shanghai. The best thing about this city is diversity making the views from our windows breathtaking. If you are passing by, we are happy to host you and make sure that you have the authentic ShangHIGH experience. All of our listings are in high buildings so you won't lack any daylight or landscape. See you soon! </span></span>
                                                        </p>
                                                    </div>
                                                    <div class="expandable-indicator"></div>
                                                </div><span class="react-expandable-trigger-more"><button class="btn-link btn-link--bold" type="button"><span>查看更多</span></button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="column_1rnz84d-o_O-column-sm-12_1wbopx2">
                                            <div style="margin-top:8px;margin-bottom:16px;margin-left:0;margin-right:0;">
                                                <div class="">
                                                    <button aria-disabled="false" type="button" class="container_71fudf-o_O-container_rounded_sa8zo9-o_O-container_notBlock_1xdomts-o_O-container_sizeSmall_l9yuiv-o_O-container_w9ia0s"><span class="text_veo4lx-o_O-text_sizeSmall_1gb9qjw-o_O-text_1ku16io"><span>联系房东</span></span>
                                                    </button>
                                                    <!-- react-empty: 1223 -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column_1rnz84d-o_O-column-sm-12_1wbopx2">
                                            <div class="row row-condensed space-2">
                                                <div class="col-md-8"><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><span>回复率：</span>
                                                    <!-- react-text: 1229 -->
                                                    <!-- /react-text --><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-inline_g86r3e">100%</span>
                                                    <div class="show-inline-block response-details hide"><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-inline_g86r3e"><!-- react-text: 1233 -->(<!-- /react-text --><!-- react-text: 1234 -->过去30天<!-- /react-text --><!-- react-text: 1235 -->)<!-- /react-text --></span></div>
                                                    </span>
                                                    <div><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><span>回复时间:</span></span>
                                                        <!-- react-text: 1239 -->
                                                        <!-- /react-text --><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-inline_g86r3e">1小时内</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                