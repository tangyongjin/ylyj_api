<meta itemprop="image" content="{roompic}?aki_policy=large">
<div data-hypernova-key="p3hero_and_slideshowbundlejs">
    <div data-react-checksum="628880123">
        <!-- react-empty: 2 -->
        <!-- react-empty: 3 -->
        <div>
            <div>
                <div id="photos" class="with-photos with-modal">
                    <span class="cover-photo">
                        <img alt="" class="hide" src="{roompic}?aki_policy=large" srcset="{roompic}?aki_policy=large 639w,
                        {roompic}?aki_policy=xx_large 1440w" width="0">
                        <span class="cover-img-container" data-hook="cover-img-container">
                            <div class="cover-img" style="background-image:url({roompic}?aki_policy=xx_large);">
                                <div class="link-reset panel-overlay-bottom-left panel-overlay-label panel-overlay-listing-label show-sm">
                                    <div>
                                        <span class="h3 text-contrast price-amount">
                                            <span>
                                                ￥552
                                            </span>
                                        </span>
                                        <span class="listing-price__per-night hide-sm">
                                            <span>
                                                每晚
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <div class="slideshow-inline-preload hide">
                            <img class="carousel-image img-responsive-height" src="https://z2.muscache.com/im/pictures/82bf7935-acde-46a0-a932-adcda6f7f451.jpg?aki_policy=x_large"
                            alt="">
                            <img class="carousel-image img-responsive-height" src="https://z2.muscache.com/im/pictures/e6bf5503-84ae-4f29-a065-06270ec0eb9c.jpg?aki_policy=x_large"
                            alt="">
                        </div>
                        <div class="hero__view-photos">
                            <button type="button" class="btn">
                                <span>
                                    查看照片
                                </span>
                            </button>
                        </div>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>