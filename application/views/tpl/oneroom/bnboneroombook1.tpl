
<style type="text/css">
    .place_an_order{
        border: 1px solid #dce0e0;
        width: 315px;
        height: 346px;
        float: right;
        position: absolute;
        left: 943px;
        background-color: #fff;
        z-index: 999
    }

    #order_top{
        background-color: rgba(60,63,64,0.9);
        color: #fff;
        height: 50px;
        padding: 13px 20px 5px;
    }
    #order_top>span{
        font-weight: 700;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif;
        font-size: 22px;
        line-height: 28px;
    }
    .btn_place_order{
        border-color: #ff5a5f;
        background-color: #ff5a5f;
        color: #fff;
        display: block;
        white-space: normal;
        width: 100%;
        padding: 9px 27px;
        font-size: 16px;
        margin-bottom: 0;
        border-radius: 4px;
        border: 1px solid;
        text-align: center;
        vertical-align: middle;
        font-weight: bold;
        line-height: 1.43;
    }
    .btn_place_order:hover{
        border-color: #ff7e82;
        background-color: #ff7e82;
        color: #fff;
    }
    .btn_place_order:active{
        border-color: #d52b2a;
        background-color: #d52b2a;
    }
</style>

<div class="place_an_order">
    <div id="order_top">
        <span>最低</span><span>￥552一天</span>
    </div>
    <div style="margin: 10px auto;padding: 20px">
        <span style="font-size:15px;line-height: 18px">时间</span>
        <!-- <span style="flex: 1">退房</span>  -->
        <input type="text" name="datefilter" value=""  style="border:border: 1px solid #dce0e0 ;outline:none;color: #767676;margin:20px auto" placeholder="入住日期 - 退房日期" />
        <span style="font-size:15px;line-height: 18px">房客</span>
        
        <input  class="input_guest_number" type="text" name="" placeholder="1位房客" style="border:border: 1px solid #dce0e0;outline:none;color: #767676;margin:20px auto" onKeyUp="value=value.replace(/[^\d]/g,'') " onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))">
        
        <button type="submit" class="btn_place_order" data-reactid="468" ><div class="text_5mbkop-o_O-size_regular_x6m9oi-o_O-weight_bold_153t78d-o_O-color_inverse_1lslapz" data-reactid="469">闪订</div>
        </button>
    </div>
    
</div>
<script>
    $(document).ready(function() {
        let rowToplist = $('#row_toplist');
        let myCarousel = $('#myCarousel');
        let line = $('#sTop');
        let placeOrder = $('.place_an_order');
        $(document).scroll(function() {
            let rTop = rowToplist[0].getBoundingClientRect().top
            //console.log(rowToplist[0].getBoundingClientRect())
            let lineTop = line[0].getBoundingClientRect().top
            let myCarouselTop = myCarousel[0].getBoundingClientRect().top
            let placeOrderTop = placeOrder[0].getBoundingClientRect().top
            //console.log(lineTop)
            let sTop = document.documentElement.clientHeight
                //console.log(sTop)
                if (rTop <= 0) {
                    rowToplist.css({
                        'position': 'fixed',
                        'top': '0',
                        'z-index': '999',
                        'background-color': '#fff',
                        'width': '708px'
                    })
                    placeOrder.css({
                        'position': 'fixed',
                        'top': '0'
                    })
                }
                if (lineTop >= 0) {
                    rowToplist.css({
                        'position': '',
                        'top': '0',
                    })
                    placeOrder.css({
                        'position': 'absolute',
                        'top': ''
                    })
                }
            })
        let datefilter = $('input[name="datefilter"]')
        let some_date_range = [
                '08/11/2017',
                '08/12/2017',
                '08/13/2017',
                '08/14/2017'
                ]
        datefilter.daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            isInvalidDate: function(date) {
                for (var ii = 0; ii < some_date_range.length; ii++) {
                    if (date.format('MM/DD/YYYY') == some_date_range[ii]) {
                        return true;
                    }
                }
            },
            locale: {
                cancelLabel: '取消',
                daysOfWeek: [
                "日",
                "一",
                "二",
                "三",
                "四",
                "五",
                "六"
                ],
                monthNames: [
                "一月",
                "二月",
                "三月",
                "四月",
                "五月",
                "六月",
                "七月",
                "八月",
                "九月",
                "十月",
                "十一月",
                "十二月"
                ]
            }
        });
        datefilter.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        });
        datefilter.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        let btnPlaceOrder = $('.btn_place_order'),
        guestNumber = $('.input_guest_number')
        btnPlaceOrder.click(()=>{
            let orderData = {}

            datefilter.val()==''?searchData.startDate='0':searchData.startDate=new Date(Date.parse(datefilter.val().split(' ')[0].replace(/-/g,  "/"))).getTime()/1000;
            datefilter.val()==''?searchData.endDate='0':searchData.endDate=new Date(Date.parse(datefilter.val().split(' ')[2].replace(/-/g,  "/"))).getTime()/1000;
            guestNumber.val() ===''?searchData.guestNumber = 1:searchData.guestNumber = guestNumber.val()




        })

    })
</script>