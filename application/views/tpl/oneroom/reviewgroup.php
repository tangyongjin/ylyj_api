{foreach reviews as review}
<div class="row review">
            <div class="">
                <div class="col-12 va-container">
                    <div class="va-middle">
                        <div class="show-inline-block">
                            <div class="va-middle">
                                <div class="media-photo-badge">
                                    <a href="/users/show/{review['userid']}" rel="nofollow" class="media-photo media-round">
                                    <img data-pin-nopin="true" src="{review['avatr']}" width="48" height="48"></a>
                                </div>
                            </div>
                            <div class="va-middle">
                                <div style="margin-top:0;margin-bottom:0;margin-left:16px;margin-right:0;">
                                    <div class="name"><a aria-hidden="true" class="text-muted link-reset" href="/users/show/{review['userid']}" rel="nofollow" target="_blank" tabindex="-1"><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-inline_g86r3e">{review['realname']}</span></a><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e">
                                    </span></div>
                                    <div class="date">
                                        <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x">{review['createtime']}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="va-middle hide-sm">
                        <div class="pull-right">
                            <div class="va-middle show-inline-block">
                                <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:16px;">
                                    <button class="btn-link btn-link--icon" type="button"><span><i class="icon icon-flag"></i>
                                    <span class="link-icon__text"><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-color_muted_10k87om-o_O-inline_g86r3e"><span>举报</span></span>
                                        </span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <button class="btn btn-default btn-small helpful-btn"><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-inline_g86r3e"><i class="icon icon-thumbs-up text-muted"></i><div class="helpful-btn-text text-muted">有用</div><div class="helpful-btn-count helpful-btn-count-regular"><div class="">1</div></div></span></button>
                        </div>
                    </  div>
                </div>
            </div>
            <div class="">
                <div class="col-sm-12">
                    <div class="">
                        <div data-review-id="141404283" class="review-text space-top-2">
                            <div class="react-expandable expanded">
                                <div class="expandable-content expandable-content--hasDlsReducedText" style="">
                                    <div class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x">
                                    {review['content']}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <hr>
            </div>
        </div>
    </div>

{/foreach}