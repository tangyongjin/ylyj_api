
        <div class="col-lg-4" ">
            <div class="book-it js-book-it">
                <div class="book-it__container js-book-it-container" style="top: 0px;border: 1px solid">
                    <div>
                        <div>
                            <!-- react-empty: 7 -->
                            <div class=""><span><div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="book-it__price"><div class="row" aria-labelledby="book-it-price-string book-it-rate-type"><div class="col-sm-12"><div class="book-it__price-amount text-special"><span><span></span><span id="book-it-price-string"><span><span class="priceAmountWrapper_17axpax"><span class="text_5mbkop-o_O-size_large_16mhv7y-o_O-weight_bold_153t78d-o_O-color_inverse_1lslapz-o_O-inline_g86r3e"><span><!-- react-text: 244 -->￥552<!-- /react-text --></span></span>
                                </span><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-color_inverse_1lslapz-o_O-inline_g86r3e"><span>每晚</span></span>
                                </span>
                                </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <meta content="CNY" itemprop="priceCurrency">
                <meta content="552" itemprop="price">
                </span>
                <form method="post" action="/payments/book?hosting_id=5116533">
                    <div class="panel book-it-panel">
                        <div class="panel-body panel-light">
                            <div class="row row-condensed space-3">
                                <div class="col-md-12">
                                    <div class="row row-condensed">
                                        <div class="book-it__col">
                                            <label class="book-it__label book-it__fields" for="datespan-checkin">
                                                <div class="text_5mbkop-o_O-size_small_1gg2mc"><span>入住</span></div>
                                            </label>
                                            <input class="checkin date-picker book-it__fields ui-datepicker-target" id="datespan-checkin" name="checkin" aria-describedby="datespan-checkin-description" value="" placeholder="年-月-日" type="text"><span class="screen-reader-only" id="datespan-checkin-description" aria-hidden="true">请以年-月-日的格式输入您的入住日期</span></div>
                                        <div class="book-it__col">
                                            <label class="book-it__label book-it__fields" for="datespan-checkout">
                                                <div class="text_5mbkop-o_O-size_small_1gg2mc"><span>退房</span></div>
                                            </label>
                                            <input class="checkout date-picker book-it__fields ui-datepicker-target" id="datespan-checkout" name="checkout" aria-describedby="datespan-checkout-description" value="" placeholder="年-月-日" type="text">
                                        </div><span class="screen-reader-only" id="datespan-checkout-description" aria-hidden="true">请以年-月-日的格式输入您的退房日期</span></div>
                                </div>
                                <div class="row row-condensed">
                                    <div class="col-md-12 space-top-3">
                                        <div class="GuestPickerContainer book-it__fields">
                                            <label for="number_of_guests_5116533" class="book-it__label">
                                                <div class="text_5mbkop-o_O-size_small_1gg2mc"><span>房客</span></div>
                                            </label>
                                            <div class="GuestPickerTrigger">
                                                <button class="GuestPickerTrigger__button" type="button">
                                                    <div class="guest-label"><span class="guest-label__text guest-label__text-guests"><span>1位房客</span></span>
                                                    </div>
                                                    <div class="dropdown-icon">
                                                        <svg viewBox="0 0 18 18" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:12px;width:12px;">
                                                            <path fill-rule="evenodd" d="M16.291 4.295a1 1 0 1 1 1.414 1.415l-8 7.995a1 1 0 0 1-1.414 0l-8-7.995a1 1 0 1 1 1.414-1.415l7.293 7.29 7.293-7.29z" />
                                                        </svg>
                                                    </div>
                                                </button>
                                                <div>
                                                    <div class="GuestPicker hide">
                                                        <div style="margin-top:16px;margin-bottom:16px;margin-left:0;margin-right:0;">
                                                            <div style="margin-top:16px;margin-bottom:16px;margin-left:0;margin-right:0;">
                                                                <fieldset class="container_z8lbtc">
                                                                    <legend class="visuallyHidden_1m8bb6v"><span>请设置成人数量。</span></legend>
                                                                    <div class="containerFallback_1cmo568-o_O-container_1s8su0z">
                                                                        <div class="beforeFallback_1iti0ju-o_O-before_t0tx82">
                                                                            <div class="table_qtix31">
                                                                                <div id="StepIncrementerRow-title-GuestCountFilter-GuestPicker-p3-book_it-adults">
                                                                                    <div class="text_5mbkop-o_O-size_regular_x6m9oi"><span>成人</span></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- react-text: 55 -->
                                                                        <!-- /react-text -->
                                                                        <div class="afterFallback_1iti0ju">
                                                                            <div class="table_qtix31-o_O-buttons_y5lyrw">
                                                                                <div class="middleAlignedCell_17u98ky-o_O-left_e4x16a">
                                                                                    <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="减" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-adults" disabled=""><span class="icon_16b32nl-o_O-icon_1wy8x1d-o_O-icon_small_abrv9k"><svg viewBox="0 0 24 24" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><rect width="12" height="2" x="6" y="11" rx="1"/></svg></span></button>
                                                                                </div>
                                                                                <div id="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-adults" role="region" aria-live="polite" aria-label="1名成人" class="middleAlignedCell_17u98ky-o_O-center_12to336">
                                                                                    <div class="text_5mbkop-o_O-size_regular_x6m9oi">1</div>
                                                                                </div>
                                                                                <div class="middleAlignedCell_17u98ky-o_O-right_140n9qh">
                                                                                    <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="加" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-adults"><span class="icon_16b32nl-o_O-icon_11jdm7g-o_O-icon_small_abrv9k"><svg viewBox="0 0 24 24" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><rect width="12" height="2" x="6" y="11" rx="1"/><rect width="2" height="12" x="11" y="6" rx="1"/></svg></span></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <div style="margin-top:24px;margin-bottom:24px;margin-left:0;margin-right:0;">
                                                                <fieldset class="container_z8lbtc">
                                                                    <legend class="visuallyHidden_1m8bb6v"><span>请设置儿童数量。</span></legend>
                                                                    <div class="containerFallback_1cmo568-o_O-container_1s8su0z">
                                                                        <div class="beforeFallback_1iti0ju-o_O-before_t0tx82">
                                                                            <div class="table_qtix31">
                                                                                <div id="StepIncrementerRow-title-GuestCountFilter-GuestPicker-p3-book_it-children">
                                                                                    <div class="text_5mbkop-o_O-size_regular_x6m9oi"><span>儿童</span></div>
                                                                                </div>
                                                                                <div id="StepIncrementerRow-subtitle-GuestCountFilter-GuestPicker-p3-book_it-children" class="subtitle_ac6rph">
                                                                                    <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x"><span>2 - 12岁</span></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- react-text: 84 -->
                                                                        <!-- /react-text -->
                                                                        <div class="afterFallback_1iti0ju">
                                                                            <div class="table_qtix31-o_O-buttons_y5lyrw">
                                                                                <div class="middleAlignedCell_17u98ky-o_O-left_e4x16a">
                                                                                    <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="减" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-children" disabled=""><span class="icon_16b32nl-o_O-icon_1wy8x1d-o_O-icon_small_abrv9k"><svg viewBox="0 0 24 24" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><rect width="12" height="2" x="6" y="11" rx="1"/></svg></span></button>
                                                                                </div>
                                                                                <div id="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-children" role="region" aria-live="polite" aria-label="0名儿童" class="middleAlignedCell_17u98ky-o_O-center_12to336">
                                                                                    <div class="text_5mbkop-o_O-size_regular_x6m9oi">0</div>
                                                                                </div>
                                                                                <div class="middleAlignedCell_17u98ky-o_O-right_140n9qh">
                                                                                    <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="加" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-children"><span class="icon_16b32nl-o_O-icon_11jdm7g-o_O-icon_small_abrv9k"><svg viewBox="0 0 24 24" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><rect width="12" height="2" x="6" y="11" rx="1"/><rect width="2" height="12" x="11" y="6" rx="1"/></svg></span></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <div style="margin-top:24px;margin-bottom:24px;margin-left:0;margin-right:0;">
                                                                <fieldset class="container_z8lbtc">
                                                                    <legend class="visuallyHidden_1m8bb6v"><span>请设置婴幼儿数量。</span></legend>
                                                                    <div class="containerFallback_1cmo568-o_O-container_1s8su0z">
                                                                        <div class="beforeFallback_1iti0ju-o_O-before_t0tx82">
                                                                            <div class="table_qtix31">
                                                                                <div id="StepIncrementerRow-title-GuestCountFilter-GuestPicker-p3-book_it-infants">
                                                                                    <div class="text_5mbkop-o_O-size_regular_x6m9oi"><span>婴幼儿</span></div>
                                                                                </div>
                                                                                <div id="StepIncrementerRow-subtitle-GuestCountFilter-GuestPicker-p3-book_it-infants" class="subtitle_ac6rph">
                                                                                    <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x"><span>2岁以下</span></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- react-text: 113 -->
                                                                        <!-- /react-text -->
                                                                        <div class="afterFallback_1iti0ju">
                                                                            <div class="table_qtix31-o_O-buttons_y5lyrw">
                                                                                <div class="middleAlignedCell_17u98ky-o_O-left_e4x16a">
                                                                                    <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="减" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-infants" disabled=""><span class="icon_16b32nl-o_O-icon_1wy8x1d-o_O-icon_small_abrv9k"><svg viewBox="0 0 24 24" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><rect width="12" height="2" x="6" y="11" rx="1"/></svg></span></button>
                                                                                </div>
                                                                                <div id="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-infants" role="region" aria-live="polite" aria-label="0名婴幼儿" class="middleAlignedCell_17u98ky-o_O-center_12to336">
                                                                                    <div class="text_5mbkop-o_O-size_regular_x6m9oi">0</div>
                                                                                </div>
                                                                                <div class="middleAlignedCell_17u98ky-o_O-right_140n9qh">
                                                                                    <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="加" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p3-book_it-infants"><span class="icon_16b32nl-o_O-icon_11jdm7g-o_O-icon_small_abrv9k"><svg viewBox="0 0 24 24" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><rect width="12" height="2" x="6" y="11" rx="1"/><rect width="2" height="12" x="11" y="6" rx="1"/></svg></span></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <div>
                                                                <div style="margin-top:0;margin-bottom:16px;margin-left:0;margin-right:0;">
                                                                    <!-- react-text: 131 -->
                                                                    <!-- /react-text -->
                                                                    <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x"><span>最多3位房客，婴幼儿不计算在内。</span></div>
                                                                    <!-- react-text: 134 -->
                                                                    <!-- /react-text -->
                                                                </div>
                                                                <div class="containerFallback_1cmo568-o_O-container_1s8su0z">
                                                                    <div class="beforeFallback_1iti0ju-o_O-before_t0tx82"></div>
                                                                    <!-- react-text: 137 -->
                                                                    <!-- /react-text -->
                                                                    <div class="afterFallback_1iti0ju">
                                                                        <div class="text_5mbkop-o_O-size_regular_x6m9oi">
                                                                            <button aria-disabled="false" class="component_9w5i1l-o_O-component_button_r8o91c" type="button"><span>关闭</span></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input name="number_of_guests" value="1" type="hidden">
                                            <input name="number_of_adults" value="1" type="hidden">
                                            <input name="number_of_children" value="0" type="hidden">
                                            <input name="number_of_infants" value="0" type="hidden">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- react-empty: 249 -->
                            <!-- react-empty: 146 -->
                            <div class="book-it__btn-width">
                                <button type="submit" class="btn btn-primary btn-large btn-block">
                                    <div class="text_5mbkop-o_O-size_regular_x6m9oi-o_O-weight_bold_153t78d-o_O-color_inverse_1lslapz"><span>申请预订</span></div>
                                </button>
                                <div>
                                    <div class="messageContainer_12to336">
                                        <div style="margin-top:8px;margin-bottom:0;margin-left:0;margin-right:0;"><small class="text_5mbkop-o_O-size_micro_16wifzf-o_O-color_muted_10k87om"><span>您暂时不会被收费</span></small></div>
                                    </div>
                                </div>
                            </div>
                            <div class="book-it__message-container js-book-it-message recent_views">
                                <hr aria-hidden="true">
                                <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x">
                                    <div class="listing-desirability">
                                        <div class="icon-background-container icon-recent-views">
                                            <div class="book-it__message-text"><strong class="headline-text">这个房源是很多人的心仪之所。</strong>
                                                <div class="media space-top-1">过去一周浏览量超过500次。</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="subnav-container">
            <div class="subnav book-it show-md" data-sticky="true" data-transition-at="#details" aria-hidden="true">
                <div class="page-container-responsive">
                    <div class="pull-left text-contrast subnav-element">
                        <div class="va-container va-container-v">
                            <div class="va-middle"><strong><div><span class="h3 text-contrast price-amount"><span>￥552</span></span><span class="listing-price__per-night hide-sm"><span>每晚</span></span></div></strong></div>
                        </div>
                    </div>
                    <div class="tablet-bookit-btn-container js-bookit-btn-container pull-right">
                        <button type="button" class="btn btn-primary btn-large"><span>申请预订</span></button>
                    </div>
                </div>
            </div>
        </div>
        <noscript></noscript>
        <div class="panel wishlist-panel space-top-6 space-md-3 hide-sm">
            <div class="container_7xdft6">
                <label class="label_1rshu0b" for="SaveToListButton">
                    <input class="input_1m8bb6v" id="SaveToListButton" value="on" type="checkbox">
                    <div class="container_qtix31" style="margin: 0px auto;">
                        <div class="child_17u98ky">
                            <div style="margin: 0px 8px 0px 0px;">
                                <svg viewBox="0 0 32 32" fill="transparent" fill-opacity="1" stroke="#767676" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" style="height: 16px; width: 16px; display: block;">
                                    <path d="M23.993 2.75c-.296 0-.597.017-.898.051-1.14.131-2.288.513-3.408 1.136-1.23.682-2.41 1.621-3.688 2.936-1.28-1.316-2.458-2.254-3.687-2.937-1.12-.622-2.268-1.004-3.41-1.135a7.955 7.955 0 0 0-.896-.051C6.123 2.75.75 4.289.75 11.128c0 7.862 12.238 16.334 14.693 17.952a1.004 1.004 0 0 0 1.113 0c2.454-1.618 14.693-10.09 14.693-17.952 0-6.84-5.374-8.378-7.256-8.378" />
                                </svg>
                            </div>
                        </div>
                        <div class="child_17u98ky">
                            <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_bold_153t78d"><span>保存到心愿单</span></div>
                        </div>
                    </div>
                </label>
                <div class="savedCount_lfdzuq">
                    <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x"><span>10095位旅行者保存了该房源</span></div>
                </div>
                <!-- react-empty: 201 -->
            </div>
            <div>
                <div class="text-center space-2">
                    <div>
                        <div>
                            <div class="iconShareButtons_9kqfyx">
                                <button aria-label="wechat" class="container_1rp5252" type="button" style="padding: 0px;">
                                    <svg viewBox="0 0 32 32" aria-hidden="true" focusable="false" style="display: block; fill: rgb(72, 72, 72); height: 18px; width: 18px;">
                                        <path fill-rule="evenodd" d="M25.088 19.472c-.621 0-1.11-.526-1.084-1.163.022-.594.518-1.078 1.096-1.075.619.007 1.108.536 1.086 1.173a1.083 1.083 0 0 1-1.098 1.065zm-6.887.01a1.12 1.12 0 0 1-1.11-1.082 1.134 1.134 0 0 1 1.12-1.162c.624 0 1.126.532 1.104 1.169a1.115 1.115 0 0 1-1.114 1.074zm-1.305-5.54c-4.74 3.15-5.325 9.046-1.229 12.895 2.154 2.023 4.768 2.848 7.684 2.542.956-.1 1.92-.635 2.835-.539.918.098 1.782.734 2.723 1.16-.427-1.743-.441-2.474.476-3.3 3.16-2.847 3.463-7.348.872-10.738-2.97-3.888-9.15-4.822-13.36-2.02zM7.161 11.28A1.324 1.324 0 0 1 5.86 9.896a1.338 1.338 0 0 1 1.385-1.305c.729.022 1.344.672 1.315 1.39a1.36 1.36 0 0 1-1.399 1.298zm9.888-1.36c.007.766-.565 1.357-1.32 1.367a1.349 1.349 0 0 1-1.377-1.326c-.01-.723.608-1.357 1.34-1.37a1.329 1.329 0 0 1 1.357 1.328zm5.184 1.288a9.07 9.07 0 0 0-1.846-4.757C17.505 2.632 12.176 1.028 7.437 2.6c-3.472 1.152-6.029 3.36-7.07 7-1.126 3.937.379 7.763 3.946 10.315.557.4.7.958.518 1.568-.246.832-.448 1.48-.661 2.468 1.173-.743 2.162-1.412 3.197-2.003.31-.179.745-.224 1.113-.192 1.062.091 2.122.25 3.283.396-.269-3.167.701-5.722 2.82-7.806 2.095-2.065 4.68-2.979 7.65-3.138z" />
                                    </svg>
                                </button>
                            </div>
                            <div class="iconShareButtons_9kqfyx">
                                <button aria-label="weibo" class="container_1rp5252" type="button" style="padding: 0px;">
                                    <svg viewBox="0 0 32 32" aria-hidden="true" focusable="false" style="display: block; fill: rgb(72, 72, 72); height: 18px; width: 18px;">
                                        <path fill-rule="evenodd" d="M30.85 14.193c-.153.613-.851.98-1.459.766-.638-.215-.972-.858-.76-1.501.67-2.143.274-4.045-1.214-5.7-1.367-1.56-3.497-2.173-5.472-1.775-.637.153-1.245-.275-1.367-.888-.152-.644.274-1.286.881-1.41 2.765-.612 5.684.307 7.72 2.512 1.974 2.236 2.55 5.331 1.67 7.996zm-4.77-5.175c-.945-1.106-2.403-1.534-3.737-1.253-.516.123-.88.612-.79 1.195.122.581.638.918 1.216.796.698-.154 1.306.061 1.823.612.485.582.607 1.225.394 1.899-.152.581.152 1.133.669 1.286a1.016 1.016 0 0 0 1.245-.673c.456-1.443.182-2.73-.82-3.862zm-9.333 13.419c-1.005 2.33-3.952 3.616-6.503 2.786-2.43-.765-3.527-3.215-2.43-5.423 1.063-2.113 3.797-3.307 6.2-2.694 2.52.642 3.739 3 2.733 5.33zm-5.133-1.806c-.82-.338-1.793 0-2.279.765s-.242 1.683.517 2.051c.76.368 1.85 0 2.338-.765.487-.826.244-1.715-.576-2.051zm1.944-.8c-.76-.337-1.368.796-.638 1.071.334.154.638.062.88-.306.152-.336.092-.643-.242-.765zm-.03 6.832c-5.077.52-9.454-1.776-9.785-5.178-.335-3.339 3.493-6.465 8.57-6.986 5.103-.49 9.45 1.84 9.784 5.178.335 3.339-3.554 6.496-8.57 6.986zM23.647 15.51c-.183-.03-.304-.092-.393-.125-.152-.093-.272-.276-.12-.644.515-1.317.546-2.357.03-3.063-1.064-1.438-3.68-1.378-6.808-.06l-.243.092c-.335.122-.669.03-.517-.43.517-1.656.395-2.85-.303-3.616-.885-.857-2.282-.888-4.226-.122C7.145 9.103.641 14.894.641 20.287c0 1.626.637 3.096 1.914 4.352C5.079 27.214 9.21 28.5 13.497 28.5c3.98 0 7.75-1.225 10.09-2.94 2.31-1.715 3.769-3.86 3.769-5.821 0-2.116-1.674-3.587-3.709-4.23z" />
                                    </svg>
                                </button>
                            </div>
                            <div class="iconShareButtons_9kqfyx">
                                <button aria-label="email" class="container_1rp5252" type="button" style="padding: 0px;">
                                    <svg viewBox="0 0 32 32" aria-hidden="true" focusable="false" style="display: block; fill: rgb(72, 72, 72); height: 18px; width: 18px;">
                                        <path fill-rule="evenodd" d="M17.417 18.994c.136-.117.86-.764 2.082-1.859L29.93 25.8H2.17l10.349-8.673c1.24 1.097 1.976 1.741 2.12 1.852.83.651 1.934.63 2.778.015M29.31 8.327l-4.829 4.336c-1.507 1.354-2.796 2.51-3.855 3.46l10.354 8.6c.01.009.014.02.024.029V6.938c0-.04-.019-.073-.023-.111a3.733 3.733 0 0 0-.082.073L29.31 8.327M4.116 7.63a5347.74 5347.74 0 0 0 4.692 4.188c3.943 3.516 6.513 5.793 6.75 5.974a.76.76 0 0 0 .923.03c.21-.183 2.82-2.524 7.005-6.281L28.31 7.21 29.657 6H2.292l.291.26 1.533 1.37m3.659 5.278a4436.652 4436.652 0 0 1-4.658-4.16c-.556-.495-1.071-.956-1.533-1.37l-.57-.51c-.002.025-.014.045-.014.07v17.886l10.384-8.703c-.999-.888-2.197-1.954-3.609-3.213" />
                                    </svg>
                                </button>
                            </div>
                            <div class="iconShareButtons_9kqfyx">
                                <button aria-label="更多分享选项" class="container_1rp5252" type="button" style="padding: 0px;">
                                    <svg viewBox="0 0 12 4" aria-hidden="true" focusable="false" style="display: block; fill: rgb(72, 72, 72); height: 18px; width: 18px;">
                                        <path fill-rule="evenodd" d="M10.5 3.5a1.5 1.5 0 1 0-.001-3.001A1.5 1.5 0 0 0 10.5 3.5zM6 3.5A1.5 1.5 0 1 0 5.999.499 1.5 1.5 0 0 0 6 3.5zm-4.5 0A1.5 1.5 0 1 0 1.499.499 1.5 1.5 0 0 0 1.5 3.5z" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <!-- react-empty: 222 -->
                        <!-- react-empty: 223 -->
                        <div>
                            <!-- react-empty: 225 -->
                        </div>
                        <!-- react-empty: 226 -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="other-actions text-center space-top-3 space-3">
        <div>
            <div class="flag-widget text-small"><a href="#" class="link-reset" title="举报此房源" data-prevent-default="true"><i class="icon icon-flag h4"></i><!-- react-text: 177 --> <!-- /react-text --><span>举报此房源</span></a></div>
            <!-- react-empty: 179 -->
        </div>
    </div>
</div>
</div>
</div>
<div class="mobile-bookit-btn-container js-bookit-btn-container hide-md hide-lg panel-btn-fixed-sm">
    <div class="panel-btn-sm">
        <button class="btn btn-primary btn-block btn-large js-book-it-sm-trigger"><span>申请预订</span></button>
    </div>
    <div class="bookit-message__friendly-booking text-center text-muted"><span>您暂时不会被收费</span></div>
</div>
