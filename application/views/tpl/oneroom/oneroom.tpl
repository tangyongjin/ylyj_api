<link rel="stylesheet" href="http://47.92.72.19/bnbfiles/oneroom.css" type="text/css" />
<div id="og_pro_photo_prompt" class="container"></div>
<div id="room" itemscope="" itemtype="http://schema.org/Product">
    <style data-aphrodite=""></style>
    <div data-hypernova-key="p3header_listing_alertbundlejs">
        <div class="show-sm page-container-responsive header-listing-alert" data-react-checksum="-1790491940">
            <div class="row">
                <div class="col-sm-12"></div>
            </div>
        </div>
    </div>
 
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <?php
        $detail['piclist'][0]['class']='active';
        $str= $this->parser->parse('tpl/oneroom/bnboneroompiclist.tpl',array('piclist'=>$detail['piclist']),true);   
        echo $str;
    ?>
  </div>


  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<span id="sTop"></span>
<?php
//$this->view('tpl/oneroom/bnboneroombook.tpl');  
$this->view('tpl/oneroom/bnboneroombook1.tpl');   
?>
 
        <div class="modal" role="dialog" aria-hidden="true" id="book-it-sm-modal" style="">
            <div class="modal-table">
                <div class="modal-cell">
                    <div class="modal-content">
                        <div class="panel-header">
                            <a href="#" class="modal-close" data-behavior="modal-close">
              ×
              <span class="screen-reader-only">关闭</span>
            </a> 预订
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="room__dls">
            <div id="summary" class="panel room-section no-border">
                <div class="page-container-responsive">
                    <div class="row">
                        <div data-hypernova-key="p3indexbundlejs">
                            <div class="col-lg-8">
                                <?php
                                  

                                  // debug($detail);
                                  //die;

                                  $str= $this->parser->parse('tpl/oneroom/bnboneroomsummary.tpl',$detail,true);   
                                  echo $str;
                                ?>

                            </div>
                        </div>
                        <div data-hypernova-key="p3book_itbundlejs" >
                            <div>
                                <?php
                                    //$this->view('tpl/oneroom/bnboneroombook.tpl');  
                                    //$this->view('tpl/oneroom/bnboneroombook1.tpl');   
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-hypernova-key="listingbundlejs">
                <div>
                    <div>
                        <!-- react-empty: 3 -->
                        <!-- react-empty: 4 -->
                        <div>
                            <?php   

                             // debug($detail);
                              $str= $this->parser->parse('tpl/oneroom/bnboneroomdetails.tpl',$detail,true);   
                              echo $str;

                              //$this->view('tpl/oneroom/bnboneroomdetails.tpl');  
                            ?>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div id="reviews">
                                
                                <?php
                                  $str= $this->parser->parse('tpl/oneroom/bnbroomreview.tpl',array('roomid'=>$roomid,  
                                  'comments_html'=>$comments_html,
                                  'comment_counter'=>$comment_counter,
                                  'totalpages'=>$totalpages),true);   
                                  echo $str;
                                ?>

                            </div>
                        </div>
                    </div>
                    <div>
                        <!-- react-empty: 1159 -->
                        <!-- react-empty: 1160 -->
                        <div>
                            <div id="host-profile" class="hostProfile_19p0w87">
                                <?php
                                  $this->view('tpl/oneroom/bnbroomhost.tpl');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div id="about-home" class="webkit-render-fix">
                    
                        <div class="page-container-responsive space-top-8 space-8">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div>
                                        <h4 class="space-2 text-center-sm"><span>房源介绍</span></h4></div>
                                    <div>
                                        <p><span>当您住在爱彼迎房源的时候，您住的是别人的家。</span></p>
                                        <hr class="space-4 space-top-2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4 space-3">
                                    <div>
                                        <div class="space-1"><span class="text-muted"><span>这是Shanghigh的房源</span></span>
                                        </div>
                                        <div class="container_1fi2pe"><span data-behavior="tooltip" data-position="bottom" aria-label="Shanghigh"><div class="container_e296pg" style="height:48px;width:48px;display:block;"><a href="/users/show/26439805" target="_blank" aria-label="Shanghigh" class="imageClickContainer_110nrr2"><img class="image_12r18es" src="https://z2.muscache.com/im/pictures/9224a699-7ee6-4d9e-8bda-81749d627652.jpg?aki_policy=profile_x_medium" alt="Shanghigh" title="Shanghigh" width="48" height="48"></a></div></span></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4 space-3">
                                    <div>
                                        <div class="space-1"><span class="text-muted"><span>Shanghigh帮忙出租。</span></span>
                                        </div>
                                        <div class="container_1fi2pe"><span data-behavior="tooltip" data-position="bottom" aria-label="Shanghigh"><div class="container_e296pg" style="height:48px;width:48px;display:block;"><a href="/users/show/92365725" target="_blank" aria-label="Shanghigh" class="imageClickContainer_110nrr2"><img class="image_12r18es" src="https://z2.muscache.com/im/pictures/4020369b-abef-49a6-8365-d026b8da6850.jpg?aki_policy=profile_x_medium" alt="Shanghigh" title="Shanghigh" width="48" height="48"></a></div></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <!-- react-empty: 1274 -->
                        <!-- react-empty: 1275 -->
                        <div>
                            <div class="space-top-4">
                                <div id="neighborhood" class="room-section">
                                    <?php
                                        $this->view('tpl/oneroom/oneroommap.tpl');  
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-hypernova-key="p3footerbundlejs">
                <div>
                    <div style="margin-top:24px;margin-bottom:36px;margin-left:0;margin-right:0;">
                    </div>
                    <!-- react-empty: 67 -->
                </div>
            </div>
        </div>

</div>
