
                        <div class="row hide-md" id="row_toplist">
                            <div>
                                <!-- react-empty: 4 -->
                                <div class="container_12x817u" style="padding-top: 0 !important">
                                    <div class="sticky-nav">
                                        <div>
                                            <div class="innerContainer_pz9fna">
                                                <div>
                                                    <div class="tabs_1xal37k1" style=";border-bottom: 1px solid #DBDBDB !important;position: relative !important;">
                                                    <a class="text_x6m9oi-o_O-tab_n1jmyo-o_O-tab_selected_cwuxid" href="#summary"><span>综述</span></a><a class="text_x6m9oi-o_O-tab_n1jmyo" href="#reviews"><span>评价</span></a><a class="text_x6m9oi-o_O-tab_n1jmyo" href="#host-profile"><span>房东</span></a><a class="text_x6m9oi-o_O-tab_n1jmyo" href="#neighborhood"><span>位置</span></a></div>
                                                    <div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function(){
                                let rowToplist = $('#row_toplist');
                                let myCarousel=$('#myCarousel');
                                let line = $('#sTop');
                                let placeOrder = $('.place_an_order');


                                $(document).scroll(function(){
                                    let rTop = rowToplist[0].getBoundingClientRect().top
                                    let lineTop = line[0].getBoundingClientRect().top
                                    let myCarouselTop = myCarousel[0].getBoundingClientRect().top
                                    let placeOrderTop = placeOrder[0].getBoundingClientRect().top
                                    
                                    //console.log(lineTop)
                                    let sTop = document.documentElement.clientHeight
                                    //console.log(sTop)
                                    if (rTop<=0) {
                                        rowToplist.css({
                                            'position':'fixed',
                                            'top':'0',
                                            'z-index':'999',
                                            'background-color':'#fff',
                                            'width':'708px'
                                        })
                                        placeOrder.css({
                                            'position':'fixed',
                                            'top':'0'
                                        })

                                    }
                                    if (lineTop>=0) {
                                        rowToplist.css({
                                            'position':'',
                                            'top':'0',
                                        })
                                        placeOrder.css({
                                            'position':'absolute',
                                            'top':''
                                        })
                                    }

                                })
                                // window.onscroll=()=>{
                                //     let rTop = rowToplist[0].offsetTop;
                                    
                                //     let sTop = document.body.scrollTop;
                                //     let result = mTop-sTop;
                                //     if (result <= 0) {
                                //         rowToplist.css({
                                //             'position':'fixed',
                                //             'top':'0'
                                //         })
                                //     }
                                // }
                            })
                        </script>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="section-container">
                                    <div class="summary-container">
                                        <div class="row">
                                            <div class="hide-sm col-md-10"><span class="hide"><div itemscope="" itemtype="http://schema.org/BreadcrumbList"><div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x"><span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><div itemprop="item" class="inlineBlock_36rlri"><a href="/s/中国" aria-disabled="false" class="component_9w5i1l-o_O-footer_10k87om"><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><span itemprop="name">中国</span></span>
                                                </a>
                                            </div>
                                            <div class="inlineBlock_36rlri" style="margin-top:0;margin-bottom:0;margin-left:8px;margin-right:8px;"><small class="text_5mbkop-o_O-size_micro_16wifzf-o_O-weight_light_1nmzz2x">&gt;</small></div>
                                            </span><span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><div itemprop="item" class="inlineBlock_36rlri"><a href="/s/Shanghai-Shi--中国" aria-disabled="false" class="component_9w5i1l-o_O-footer_10k87om"><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><span itemprop="name">Shanghai Shi</span></span>
                                            </a>
                                        </div>
                                        <div class="inlineBlock_36rlri" style="margin-top:0;margin-bottom:0;margin-left:8px;margin-right:8px;"><small class="text_5mbkop-o_O-size_micro_16wifzf-o_O-weight_light_1nmzz2x">&gt;</small></div>
                                        </span><span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><div itemprop="item" class="inlineBlock_36rlri"><a href="/s/上海--中国" aria-disabled="false" class="component_9w5i1l-o_O-footer_10k87om"><span class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x-o_O-inline_g86r3e"><span itemprop="name">上海</span></span>
                                        </a>
                                    </div>
                                    </span>
                                </div>
                            </div>
                            </span>
                            <div itemprop="name" class="overflow summary-title-bottom-spacing title-2" id="listing_name">{roomtitle}</div>
                            <div>
                                <div id="display-address" class="text-muted" data-location="上海, Shanghai Shi, 中国"><a href="#neighborhood" class="link-reset">
                                中国,{city}</a><span> &nbsp; </span>
                                    <a href="#reviews" class="link-reset show-inline-block">
                                        <div class="star-rating-wrapper" itemscope="" itemprop="aggregateRating" itemtype="http://schema.org/AggregateRating" aria-label="在187位房客评价中，平均评分是 5 星（满分为 5 星）" role="img">
                                            <div class="star-rating" itemprop="ratingValue" content="5"><span role="img" aria-label="评分是5（满分为5）"><span class="star_1c8pmxm-o_O-star_colorBabu_1qcsgvk-o_O-star_sizeSmall_efbia6"><svg viewBox="0 0 1000 1000" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><path d="M971.5 379.5c9 28 2 50-20 67L725.4 618.6l87 280.1c11 39-18 75-54 75-12 0-23-4-33-12l-226.1-172-226.1 172.1c-25 17-59 12-78-12-12-16-15-33-8-51l86-278.1L46.1 446.5c-21-17-28-39-19-67 8-24 29-40 52-40h280.1l87-278.1c7-23 28-39 52-39 25 0 47 17 54 41l87 276.1h280.1c23.2 0 44.2 16 52.2 40z"/></svg></span><span class="star_1c8pmxm-o_O-star_colorBabu_1qcsgvk-o_O-star_sizeSmall_efbia6"><svg viewBox="0 0 1000 1000" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><path d="M971.5 379.5c9 28 2 50-20 67L725.4 618.6l87 280.1c11 39-18 75-54 75-12 0-23-4-33-12l-226.1-172-226.1 172.1c-25 17-59 12-78-12-12-16-15-33-8-51l86-278.1L46.1 446.5c-21-17-28-39-19-67 8-24 29-40 52-40h280.1l87-278.1c7-23 28-39 52-39 25 0 47 17 54 41l87 276.1h280.1c23.2 0 44.2 16 52.2 40z"/></svg></span><span class="star_1c8pmxm-o_O-star_colorBabu_1qcsgvk-o_O-star_sizeSmall_efbia6"><svg viewBox="0 0 1000 1000" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><path d="M971.5 379.5c9 28 2 50-20 67L725.4 618.6l87 280.1c11 39-18 75-54 75-12 0-23-4-33-12l-226.1-172-226.1 172.1c-25 17-59 12-78-12-12-16-15-33-8-51l86-278.1L46.1 446.5c-21-17-28-39-19-67 8-24 29-40 52-40h280.1l87-278.1c7-23 28-39 52-39 25 0 47 17 54 41l87 276.1h280.1c23.2 0 44.2 16 52.2 40z"/></svg></span><span class="star_1c8pmxm-o_O-star_colorBabu_1qcsgvk-o_O-star_sizeSmall_efbia6"><svg viewBox="0 0 1000 1000" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><path d="M971.5 379.5c9 28 2 50-20 67L725.4 618.6l87 280.1c11 39-18 75-54 75-12 0-23-4-33-12l-226.1-172-226.1 172.1c-25 17-59 12-78-12-12-16-15-33-8-51l86-278.1L46.1 446.5c-21-17-28-39-19-67 8-24 29-40 52-40h280.1l87-278.1c7-23 28-39 52-39 25 0 47 17 54 41l87 276.1h280.1c23.2 0 44.2 16 52.2 40z"/></svg></span><span class="star_1c8pmxm-o_O-star_colorBabu_1qcsgvk-o_O-star_sizeSmall_efbia6"><svg viewBox="0 0 1000 1000" aria-hidden="true" focusable="false" style="display:block;fill:currentColor;height:1em;width:1em;"><path d="M971.5 379.5c9 28 2 50-20 67L725.4 618.6l87 280.1c11 39-18 75-54 75-12 0-23-4-33-12l-226.1-172-226.1 172.1c-25 17-59 12-78-12-12-16-15-33-8-51l86-278.1L46.1 446.5c-21-17-28-39-19-67 8-24 29-40 52-40h280.1l87-278.1c7-23 28-39 52-39 25 0 47 17 54 41l87 276.1h280.1c23.2 0 44.2 16 52.2 40z"/></svg></span></span>
                                            </div>
                                            <!-- react-text: 74 -->
                                            <!-- /react-text --><span class="h6"><small><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-weight_light_1nmzz2x-o_O-color_muted_10k87om-o_O-inline_g86r3e"><span><span>{comment_counter}条评价</span><span itemprop="reviewCount" content="187"></span></span>
                                            </span>
                                            </small>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="hide-md hide-lg"> </div>
                        <div class="hide-sm text-center col-md-2 space-top-2 text-muted">
                            <div>
                                
                                <!-- <div class="superhost-photo-container">
                                    <div class="media-photo-badge">
                                        <a href="#host-profile" rel="nofollow" class="media-photo media-round"><img alt="Shanghigh是房东。" data-pin-nopin="true" src="https://z2.muscache.com/im/pictures/9224a699-7ee6-4d9e-8bda-81749d627652.jpg?aki_policy=profile_x_medium" title="Shanghigh的用户个人资料" width="64" height="64"></a><img src="https://z0.muscache.com/airbnb/static/badges/superhost_photo_badge-a38e6a7d2afe0e01146ce910da3915f5.png" class="superhost-photo-badge superhost-photo-badge" alt="Shanghigh是超赞房东。"></div>
                                </div>
 -->
                            </div>
                        
                            <!-- <div><a aria-hidden="true" class="link-reset text-wrap text-small" href="#host-profile" tabindex="-1">Shanghigh</a></div> -->
                        
                        </div>

                    </div>


                    <div class="row row-condensed text-muted text-center hide-sm" style="border-bottom: 1px solid #eee;
                    padding-bottom: 16px">
                        <hr class="icon-row-divider">
                        <div>
                            <div class="col-sm-3 icon--small-margin"><i class="icon icon-entire-place icon-size" aria-hidden="true"></i>
                                <br><span class="text-small">整套房子/公寓</span></div>
                            <div class="col-sm-3 icon--small-margin"><i class="icon icon-group icon-size" aria-hidden="true"></i>
                                <br><span class="text-small">{capacity}位房客</span></div>
                            <div class="col-sm-3 icon--small-margin"><i class="icon icon-rooms icon-size" aria-hidden="true"></i>
                                <br><span class="text-small">单间</span></div>
                            <div class="col-sm-3 icon--small-margin"><i class="icon icon-double-bed icon-size" aria-hidden="true"></i>
                                <br><span class="text-small">{bednum}张床</span></div>
                        </div>

                    </div>
                </div>
                <!-- <hr class="icon-row-divider"> -->
            </div>
        </div>
    </div>
