
                <div id="details" class="details-section webkit-render-fix">
                    <div class="page-container-responsive">
                        <div class="row_2h22gn">
                            <div class="column_1rnz84d-o_O-column-lg-8_1gc3mvd">
                                <div class="js-details-column">
                                    <!-- react-empty: 11 -->
                                    <div style="margin-top:8px;margin-bottom:0;margin-left:0;margin-right:0;">
                                        <h4 class="space-4 section-title"><span>关于此房源</span></h4>
                                        <div class="simple-format-container">
                                            <p><span>
                                            {roomdesc}
                                            ( *请仔细查看说明和照片然后预定房间。我们写的说明和上传的照片都是真实的)
                                            </span></p>
                                        </div>
                                        <div class="row row-condensed">
                                            <div class="contact-host-div col-12 top-spacing-3">
                                                <div><span class="text_5mbkop-o_O-size_reduced_1oti1ib-o_O-inline_g86r3e"><button aria-disabled="false" class="component_9w5i1l-o_O-component_button_r8o91c" type="button"><span>联系房东</span></button>
                                                    </span>
                                                    <!-- react-empty: 24 -->
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-3 text-muted about-this-listing-section-title">
                                                <div><span>房源</span></div>
                                            </div>
                                            <div class="col-md-9 bottom-spacing-negative-2">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="bottom-spacing-2">
                                                            <div>
                                                                <!-- react-text: 35 -->可住：
                                                                <!-- /react-text -->
                                                                <!-- react-text: 36 -->
                                                                <!-- /react-text --><strong>{capacity}</strong></div>
                                                        </div>
                                                        <div class="bottom-spacing-2">
                                                            <div>
                                                                <!-- react-text: 40 -->卫生间：
                                                                <!-- /react-text -->
                                                                <!-- react-text: 41 -->
                                                                <!-- /react-text --><strong>{washroomnum}</strong></div>
                                                        </div>
                                                        <div class="bottom-spacing-2">
                                                            <div>
                                                                <!-- react-text: 45 -->卧室：
                                                                <!-- /react-text -->
                                                                <!-- react-text: 46 -->
                                                                <!-- /react-text --><strong>单间</strong></div>
                                                        </div>
                                                        <div class="bottom-spacing-2">
                                                            <div>
                                                                <!-- react-text: 50 -->床铺：
                                                                <!-- /react-text -->
                                                                <!-- react-text: 51 -->
                                                                <!-- /react-text --><strong>1</strong></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="bottom-spacing-2">
                                                            <div>
                                                                <!-- react-text: 56 -->入住时间：
                                                                <!-- /react-text -->
                                                                <!-- react-text: 57 -->
                                                                <!-- /react-text --><strong>15:00后</strong></div>
                                                        </div>
                                                        <div class="bottom-spacing-2">
                                                            <div>
                                                                <!-- react-text: 61 -->退房时间：
                                                                <!-- /react-text -->
                                                                <!-- react-text: 62 -->
                                                                <!-- /react-text --><strong>13:00</strong></div>
                                                        </div>
                                                        <div class="bottom-spacing-2">
                                                            <div>
                                                                <a href="/s/%E4%B8%8A%E6%B5%B7--%E4%B8%AD%E5%9B%BD" class="link-reset">
                                                                    <!-- react-text: 67 -->房源类型：
                                                                    <!-- /react-text -->
                                                                    <!-- react-text: 68 -->
                                                                    <!-- /react-text --><strong>公寓</strong></a>
                                                            </div>
                                                        </div>
                                                        <div class="bottom-spacing-2">
                                                            <div>
                                                                <!-- react-text: 72 -->房间类型：
                                                                <!-- /react-text -->
                                                                <!-- react-text: 73 -->
                                                                <!-- /react-text --><strong>整套房子/公寓</strong></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 bottom-spacing-2"><strong><a href="#house-rules" class="react-house-rules-trigger" data-prevent-default="true"><span>《房屋守则》</span></a></strong></div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="col-md-9 col-md-offset-3">
                                        <div class="row amenities">
                                            <div class="col-md-3 text-muted about-this-listing-section-title">
                                                <div><span>便利设施</span></div>
                                            </div>
                                            <div class="col-md-9 expandable">
                                                <div class="expandable-content-summary top-spacing-negative-1">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-paw"></i><!-- react-text: 93 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 95 --><a href="/s/上海--中国?amenity=pet-friendly" class="link-reset">允许携带宠物</a></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-elevator"></i><!-- react-text: 101 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span>电梯</span></div>
                                                            </div>
                                                            <button class="expandable-trigger-more btn-link btn-link--bold" type="button"><span>查看更多</span></button>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-internet"></i><!-- react-text: 110 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span>网络</span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-family"></i><!-- react-text: 116 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><a href="/s/上海--中国?amenity=family-friendly" class="link-reset">欢迎家庭/儿童入住</a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="expandable-content expandable-content-full">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-paw"></i><!-- react-text: 1305 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 1307 --><strong><a href="/s/上海--中国?amenity=pet-friendly" class="link-reset">允许携带宠物</a></strong></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-elevator"></i><!-- react-text: 1314 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><strong>电梯</strong></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted">
                                                                    <del aria-hidden="true">免费停车位</del>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-internet"></i><!-- react-text: 1323 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><strong>网络</strong></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted"><span><!-- react-empty: 1328 --><del aria-hidden="true">游泳池</del></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted">
                                                                    <del aria-hidden="true">室内壁炉</del>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-family"></i><!-- react-text: 1337 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><strong><a href="/s/上海--中国?amenity=family-friendly" class="link-reset">欢迎家庭/儿童入住</a></strong></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-intercom"></i><!-- react-text: 1344 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><strong>门禁/楼宇对讲机</strong></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted"><span><!-- react-empty: 1349 --><del aria-hidden="true">适合举办活动</del></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted">
                                                                    <del aria-hidden="true">允许吸烟</del>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-accessible"></i><!-- react-text: 1358 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 1360 --><strong>无障碍设施</strong></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted">
                                                                    <del aria-hidden="true">有线电视</del>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted"><span><!-- react-empty: 1368 --><del aria-hidden="true">烘干机</del></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-meal"></i><!-- react-text: 1374 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 1376 --><strong>厨房</strong></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-wifi"></i><!-- react-text: 1382 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 1384 --><strong>无线网络</strong></span></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted">
                                                                    <del aria-hidden="true">按摩浴缸</del>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted">
                                                                    <del aria-hidden="true">门卫</del>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-hair-dryer"></i><!-- react-text: 1397 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><strong>吹风机</strong></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted">
                                                                    <del aria-hidden="true">熨斗</del>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-essentials"></i><!-- react-text: 1406 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 1408 --><strong>生活必需品</strong></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-heating"></i><!-- react-text: 1414 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 1416 --><strong>暖气</strong></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-hangers"></i><!-- react-text: 1422 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><strong>衣架</strong></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-washer"></i><!-- react-text: 1428 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 1430 --><strong>洗衣机</strong></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-air-conditioning"></i><!-- react-text: 1436 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><strong>空调</strong></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-shampoo"></i><!-- react-text: 1442 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><strong>洗发水</strong></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted"><span><!-- react-empty: 1447 --><del aria-hidden="true">健身房</del></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted"><span><!-- react-empty: 1452 --><del aria-hidden="true">早餐</del></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2"><span aria-hidden="true"><i class="icon h3 icon-laptop"></i><!-- react-text: 1458 -->&nbsp;&nbsp;&nbsp;<!-- /react-text --></span><span><!-- react-empty: 1460 --><strong>书桌／工作区域</strong></span></div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted">
                                                                    <del aria-hidden="true">电视</del>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="space-1 bottom-spacing-2 text-muted"><span><!-- react-empty: 1468 --><del aria-hidden="true">独立入口</del></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="col-md-9 col-md-offset-3">
                                        <div class="row">
                                            <div class="col-md-3 text-muted about-this-listing-section-title">
                                                <div><span>收费相关</span></div>
                                            </div>
                                            <div class="col-md-9">
                                                <div style="margin-top:0;margin-bottom:-16px;margin-left:0;margin-right:0;">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="bottom-spacing-2">
                                                                <div>
                                                                    <!-- react-text: 129 -->额外房客：
                                                                    <!-- /react-text -->
                                                                    <!-- react-text: 130 -->
                                                                    <!-- /react-text --><strong>￥80 / 晚 多于2位房客</strong></div>
                                                            </div>
                                                            <div class="bottom-spacing-2">
                                                                <div>
                                                                    <!-- react-text: 134 -->押金：
                                                                    <!-- /react-text -->
                                                                    <!-- react-text: 135 -->
                                                                    <!-- /react-text --><strong>￥700</strong></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="bottom-spacing-2">
                                                                <div>
                                                                    <!-- react-text: 140 -->满1周立减:
                                                                    <!-- /react-text -->
                                                                    <!-- react-text: 141 -->
                                                                    <!-- /react-text --><strong>5%</strong></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="margin: 24px 0px 0px;">
                                                    <div class="text-small"><strong><span>始终通过爱彼迎进行交流</span></strong>
                                                        <div><span>为了保护您的付款安全，请绝对不要在爱彼迎站外或应用外转账或交流。</span>
                                                            <!-- react-text: 1476 -->
                                                            <!-- /react-text --><strong><a href="/help/article/199" target="_blank" rel="noopener noreferer" aria-disabled="false" class="component_9w5i1l"><span>了解更多</span></a></strong></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="col-md-9 col-md-offset-3">
                                        <div class="row description" id="description">
                                            <div class="col-md-3 text-muted about-this-listing-section-title">
                                                <div><span>描述</span></div>
                                            </div>
                                            <div class="col-md-9">
                                                <div>
                                                    <p><strong class="description-section-title"><span>床位安排</span></strong></p>
                                                    <div class="space-6 space-top-2">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="bedroom-config-icon-container"><i class="icon icon-size-2 bedroom-config-icon icon-king-bed"></i></div>
                                                                    <div><strong class="description-section-title">共用空间</strong></div>
                                                                    <div class="text-muted">1张特大号床</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="react-expandable">
                                                        <div class="expandable-content expandable-content-long">
                                                            <div>
                                                                <p class=""><strong class="description-section-title"><span>房源</span></strong></p>
                                                                <div class="simple-format-container">
                                                                    <p><span>
                                                                    
                                                                    </span></p>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <p class="section-title-top-spacing"><strong class="description-section-title"><span>房客使用权限</span></strong></p>
                                                                <div class="simple-format-container">
                                                                    <p><span>- 打印服务- 洗衣机- 吹风机- 冰箱- 加湿器- 音箱- 微波- 空调- 取暖器- 送风机- 毛巾- 牙膏 (*牙刷需要自己准备)- 洗发水,润肤液</span></p>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <p class="section-title-top-spacing"><strong class="description-section-title"><span>与房客的互动</span></strong></p>
                                                                <div class="simple-format-container">
                                                                    <p><span>客户第一天来的时候基本上等客人在房间。</span></p>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <p class="section-title-top-spacing"><strong class="description-section-title"><span>街区</span></strong></p>
                                                                <div class="simple-format-container">
                                                                    <p><span>欢迎来上海,我们位于北外滩。走路5分钟到外滩,走路15分钟到南京东路步行街。</span></p>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <p class="section-title-top-spacing"><strong class="description-section-title"><span>出行</span></strong></p>
                                                                <div class="simple-format-container">
                                                                    <p><span>我们楼前面有一条路上很多小杂货店和超市。容易购买生活必需品。到了晚上,附近路上有很多摆摊卖烧烤。</span></p>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <p class="section-title-top-spacing"><strong class="description-section-title"><span>其他注意事项</span></strong></p>
                                                                <div class="simple-format-container">
                                                                    <p><span>* 房间里是穿鞋子生活形式。这一点请注意。* 房间地址和我们联系方式是您订房好之后可以看到在airbnb上。</span></p>
                                                                </div>
                                                            </div>
                                                            <div class="expandable-indicator"></div>
                                                        </div><span class="react-expandable-trigger-more"><button class="btn-link btn-link--bold" type="button"><span>查看更多</span></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="col-md-9 col-md-offset-3">
                                        <div class="row react-house-rules" id="house-rules">
                                            <div class="col-md-3">
                                                <div class="text-muted about-this-listing-section-title"><span>《房屋守则》</span></div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="structured_house_rules">
                                                    <div class="row col-sm-12"><span>禁止吸烟</span></div>
                                                    <div class="row col-sm-12 space-top-1"><span>不允许举办派对和活动</span></div>
                                                    <div class="row col-sm-12 space-top-1"><span>入住时间是15:00后</span></div>
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                            <hr class="structured_house_rules__hr">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="react-expandable expanded">
                                                    <div class="expandable-content" style="">
                                                        <div>
                                                            <div class="simple-format-container">
                                                                <p><span>1 .宿舍内禁止吸烟。2 .我们5天以上住的提供一次换床单。3 .宿舍里为了以防万一的火灾具备了灭火器。 使用随时注意安全。</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="expandable-indicator"></div>
                                                    </div><span class="react-expandable-trigger-more"><button class="btn-link btn-link--bold" type="button"><span>查看更多</span></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="col-md-9 col-md-offset-3">
                                        <div class="row">
                                            <div class="col-md-3 text-muted about-this-listing-section-title"><span>预订取消政策</span></div>
                                            <div class="col-md-9">
                                                <p>
                                                    <!-- react-text: 230 -->
                                                    <!-- /react-text --><strong><span>严格</span></strong>
                                                    <!-- react-text: 233 -->
                                                    <!-- /react-text -->
                                                </p>
                                                <p>
                                                    <!-- react-text: 235 -->
                                                    <!-- /react-text --><span>距离旅程开始7天之前取消预订可获预订费用的50%及服务费的退款。</span>
                                                    <!-- react-text: 237 -->
                                                    <!-- /react-text -->
                                                </p>
                                                <div><a href="/home/cancellation_policies#strict" target="_blank" rel="noopener"><strong><!-- react-text: 241 --> <!-- /react-text --><span>查看详情</span><!-- react-text: 243 --> <!-- /react-text --></strong></a></div>
                                            </div>
                                        </div>
                                        <hr class="col-md-9 col-md-offset-3">
                                        <div class="row">
                                            <div class="col-md-3 text-muted about-this-listing-section-title">
                                                <div><span>安全设施</span></div>
                                            </div>
                                            <div class="col-md-9">
                                                <div style="margin-top:0;margin-bottom:-16px;margin-left:0;margin-right:0;">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div>
                                                                <div>
                                                                    <div class="space-1 bottom-spacing-2"><span>烟雾报警器</span></div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div>
                                                                    <div class="space-1 bottom-spacing-2"><span>一氧化碳报警器</span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div>
                                                                <div>
                                                                    <div class="space-1 bottom-spacing-2"><span>急救包</span></div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div>
                                                                    <div class="space-1 bottom-spacing-2"><span>灭火器</span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="col-md-9 col-md-offset-3">
                                        <div class="row">
                                            <div class="col-md-3 text-muted about-this-listing-section-title">
                                                <div><span>可订状态</span></div>
                                            </div>
                                            <div class="col-md-9 bottom-spacing-negative-2">
                                                <div class="row">
                                                    <div class="bottom-spacing-2 col-md-6">最短住宿晚数<strong>1晚</strong>。</div>
                                                    <div class="bottom-spacing-2 col-md-6"><a class="view-calendar" href="#"><strong><span>查看日历</span></strong></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            