<html xmlns:fb="http://ogp.me/ns/fb#" lang="zh"><!--<![endif]-->
<head>
	<?php
	$this->view('public/bnb.meta.html');
	?>

</head>

<body class="with-new-header has-epcot-header p1-header-view has_scrolled_below_fold"><a class="screen-reader-only screen-reader-only-focusable skip-to-content" data-hook="skip-to-content" href="#site-content">
	跳至内容
</a>
<span class="screen-reader-only">
	爱彼迎
</span>
<div id="smart-banner" class="smart-banner" aria-hidden="true"></div>

<?php
$this->view('public/bnb.login.header.php');

?>
<main id="site-content" role="main" tabindex="-1">



	<div class="subnav hide-print">
		<div class="page-container-responsive">
			<ul class="subnav-list">
				<li>
					<a href="/users/edit" aria-selected="true" class="subnav-item" id="user-profile-item">个人资料</a>
				</li>
			</ul>
		</div>
	</div>


	<div id="notification-area"></div>
	<div class="page-container-responsive space-top-4 space-4">
		<div class="row">
			<div class="col-md-3 space-sm-4" >
				<div class="sidenav">
					<ul class="sidenav-list">
						<li>
							<a href="http://47.92.72.19/user/personalData" aria-selected="false" class="sidenav-item">编辑个人资料</a>
						</li>
						<li>
							<a href="http://47.92.72.19/user/personalPhoto"" aria-selected="false" class="sidenav-item">照片和视频</a>
						</li>
						<li>
							<a href="#" aria-selected="false" class="sidenav-item">信任和验证</a>
						</li>
						<li>
							<a href="http://47.92.72.19/user/personalEvaluate" aria-selected="true" class="sidenav-item">评价</a>
						</li>
						<li>
							<a href="#" aria-selected="false" class="sidenav-item">推荐语</a>
						</li>
						<li>
						</li>
					</ul>
				</div>


				<a href="http://47.92.72.19/user/personalInfo" class="btn btn-block space-top-4" style="border: 1px solid rgb(196,196,196)">查看个人资料</a>
			</div>
			<div class="col-md-9" >

				<ul class="tabs" role="tablist" id="tab_evaluate">
					<li>
						<span class="tab-item"  role="tab" aria-controls="received" aria-selected="true" id="tab_evaluate_read" style="cursor: pointer;">
							对您的评价
						</span>
					</li>
					<li>
						<div  class="tab-item" role="tab" aria-controls="sent" aria-selected="false" id="tab_evaluate_write" style="cursor: pointer;">
							您写的评价
						</span>
					</li>
				</ul>

				<div id="dashboard-content" >

					<div id="reviews" class="space-top-4"  >
						<div class="tab-panel" role="tabpanel" aria-hidden="false" id="received">
							<div class="panel" style="margin-bottom:0;border: 1px solid #dce0e0;border-radius: 0">
								<div class="panel-header">
									过往评价
								</div>
								<div class="panel-body">
									<p class="text-lead">
										<?php echo '预订的住宿结束后才能在爱彼迎网站上撰写评价。您收到的评价将出现在这里以及您公开的个人资料中。' ;?>
									</p>

									<ul class="list-layout reviews-list space-top-4">
										<li class="reviews-list-item">
											<?php echo '尚未有人对您进行评价。'; ?>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="tab-panel" role="tabpanel" aria-hidden="true" id="sent">
							<div class="panel" style="margin-bottom:0;border: 1px solid #dce0e0;border-radius: 0">
								<div class="panel-header">
									撰写评论
								</div>
								<div class="panel-body">
									<ul class="list-layout reviews-list">
										<li class="reviews-list-item">
											<?php echo '目前没有写评价的需要。该是预订另一个旅程的时候了' ;?>
										</li>
									</ul>
								</div>
							</div>

							<div class="panel space-top-4" style="margin-bottom:0;border: 1px solid #dce0e0;border-radius: 0">
								<div class="panel-header">
									您过往写的评价
								</div>
								<div class="panel-body">
								</div>
							</div>

						</div>
					</div>





				</div>
			</div>
		</div>
	</div>

</main>

<div id="footer-promo-mount"></div>
<div id="footer" class="container-brand-light footer-surround footer-container">
	<?php  $this->view('public/bnbfooter.html');  ?>
</div>
<div id="fb-root"></div>
<div>
	<div data-reactroot=""><span></span></div>
</div>

</body>
</html>