<html xmlns:fb="http://ogp.me/ns/fb#" lang="zh"><!--<![endif]-->
<head>
  <?php
  $this->view('public/bnb.meta.html');
  ?>

</head>

<body class="with-new-header has-epcot-header p1-header-view has_scrolled_below_fold"><a class="screen-reader-only screen-reader-only-focusable skip-to-content" data-hook="skip-to-content" href="#site-content">
  跳至内容
</a>
<span class="screen-reader-only">
  爱彼迎
</span>
<div id="smart-banner" class="smart-banner" aria-hidden="true"></div>

<?php
$this->view('public/bnb.login.header.php');

?>
<main id="site-content" role="main" tabindex="-1">



  <div class="subnav hide-print">
    <div class="page-container-responsive">
      <ul class="subnav-list">
      <!-- <li>
        <a href="/dashboard" aria-selected="false" class="subnav-item" id="dashboard-item">控制面板</a>
      </li>
      <li>
        <a href="/inbox" aria-selected="false" class="subnav-item" id="inbox-item">收件箱</a>
      </li>
      <li>
        <a href="/rooms" aria-selected="false" class="subnav-item cohosting-ml-tooltip-trigger" id="rooms-item">您的房源</a>
      </li>
      <li>
           <a href="/trips/upcoming" aria-selected="false" class="subnav-item" id="your-trips-item">您的旅程</a>
         </li> -->
         <li>
          <a href="/users/edit" aria-selected="true" class="subnav-item" id="user-profile-item">个人资料</a>
        </li>
      <!-- <li>
        <a href="/account" aria-selected="false" class="subnav-item" id="account-item">账号</a>
      </li>
        <li>
          <a href="/invite?r=32" class="subnav-item" id="invite-friends-item">旅行基金</a>
        </li> -->
      </ul>
    </div>
  </div>


  <div id="notification-area"></div>
  <div class="page-container-responsive space-top-4 space-4">
    <div class="row">
      <div class="col-md-3 space-sm-4">
        <div class="sidenav">
          <ul class="sidenav-list">
            <li>
              <a href="http://47.92.72.19/user/personalDate" aria-selected="true" class="sidenav-item">编辑个人资料</a>
            </li>
            <li>
              <a href="https://zh.airbnb.com/users/edit/138244998?section=media" aria-selected="false" class="sidenav-item">照片和视频</a>
            </li>
            <li>
              <a href="https://zh.airbnb.com/users/edit_verification/138244998" aria-selected="false" class="sidenav-item">信任和验证</a>
            </li>
            <li>
              <a href="https://zh.airbnb.com/users/reviews/138244998" aria-selected="false" class="sidenav-item">评价</a>
            </li>
            <li>
              <a href="https://zh.airbnb.com/users/references/138244998" aria-selected="false" class="sidenav-item">推荐语</a>
            </li>
            <li>
            </li>
          </ul>
        </div>


        <a href="https://zh.airbnb.com/users/show/138244998" class="btn btn-block space-top-4">查看个人资料</a>
      </div>
      <div class="col-md-9">

        <div id="dashboard-content">


          <form accept-charset="UTF-8" action="/update/138244998" data-hook="update-profile-form" enctype="multipart/form-data" id="update_form" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"><input name="authenticity_token" type="hidden" value="V4$.airbnb.com$vfWlhOnrc3s$xpopJ53XhKpHOowCz3JlktVqv2CkYsIuy7ivCoCX-Lk="></div>







            <div class="panel space-4">
              <div class="panel-header">
                必填
              </div>
              <div class="panel-body">
                <div class="row row-condensed space-4">
                  <label class="text-right col-sm-3" for="user_first_name">
                    姓名 
                  </label>
                  <div class="col-sm-9">
                    <input id="user_first_name" name="user[first_name]" size="30" type="text" value="<?php echo $realname;?>">
                  </div>
                </div>

                <div class="row row-condensed space-4">
                  <label class="text-right col-sm-3" for="user_nick_name">
                    昵称 
                  </label>
                  <div class="col-sm-9">

                    <input id="user_last_name" name="user[last_name]" size="30" type="text" value="">

                    <div class="text-muted space-top-1">您的公开个人资料将仅显示您的昵称。申请预订时，房东会看到您的真实姓名。</div>
                  </div>
                </div>

                <div class="row row-condensed space-4">
                  <label class="text-right col-sm-3" for="user_sex">
                    我是 <i class="icon icon-lock icon-ebisu" data-behavior="tooltip" aria-label="保密"></i>
                  </label>
                  <div class="col-sm-9">

                    <div class="select">
                      <select id="user_sex" name="user[sex]"><option value="" selected="selected">性别</option>
                        <option value="Male" >男性</option>
                        <option value="Female">女性</option>
                        <option value="Other">其他</option></select>
                      </div>

                      <div class="text-muted space-top-1">此数据仅用于分析，我们不会透露给其它用户。</div>
                    </div>
                  </div>

                  <div class="row row-condensed space-4">
                    <label class="text-right col-sm-3" for="user_birthdate">
                      出生日期 <i class="icon icon-lock icon-ebisu" data-behavior="tooltip" aria-label="保密"></i>
                    </label>
                    <div class="col-sm-9">
                      <input type="text" name="birthdate" value="01/01/1999" style="width: 25%" />
                      <!-- <fieldset>
                        <legend class="screen-reader-only">生日</legend>
                        <div class="select">
                          <select aria-label="月" id="user_birthdate_2i" name="user[birthdate(2i)]">
                            <option selected="selected" value="1">一月</option>
                            <option value="2">二月</option>
                            <option value="3">三月</option>
                            <option value="4">四月</option>
                            <option value="5">五月</option>
                            <option value="6">六月</option>
                            <option value="7">七月</option>
                            <option value="8">八月</option>
                            <option value="9">九月</option>
                            <option value="10">十月</option>
                            <option value="11">十一月</option>
                            <option value="12">十二月</option>
                          </select>

                        </div>
                        <div class="select">
                          <select aria-label="日" id="user_birthdate_3i" name="user[birthdate(3i)]">
                            <option selected="selected" value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                          </select>

                        </div>
                        <div class="select">
                          <select aria-label="年" id="user_birthdate_1i" name="user[birthdate(1i)]">
                            <option selected="selected" value="1999">1999</option>
                            <option value="1998">1998</option>
                            <option value="1997">1997</option>
                            <option value="1996">1996</option>
                            <option value="1995">1995</option>
                            <option value="1994">1994</option>
                            <option value="1993">1993</option>
                            <option value="1992">1992</option>
                            <option value="1991">1991</option>
                            <option value="1990">1990</option>
                            <option value="1989">1989</option>
                            <option value="1988">1988</option>
                            <option value="1987">1987</option>
                            <option value="1986">1986</option>
                            <option value="1985">1985</option>
                            <option value="1984">1984</option>
                            <option value="1983">1983</option>
                            <option value="1982">1982</option>
                            <option value="1981">1981</option>
                            <option value="1980">1980</option>
                            <option value="1979">1979</option>
                            <option value="1978">1978</option>
                            <option value="1977">1977</option>
                            <option value="1976">1976</option>
                            <option value="1975">1975</option>
                            <option value="1974">1974</option>
                            <option value="1973">1973</option>
                            <option value="1972">1972</option>
                            <option value="1971">1971</option>
                            <option value="1970">1970</option>
                            <option value="1969">1969</option>
                            <option value="1968">1968</option>
                            <option value="1967">1967</option>
                            <option value="1966">1966</option>
                            <option value="1965">1965</option>
                            <option value="1964">1964</option>
                            <option value="1963">1963</option>
                            <option value="1962">1962</option>
                            <option value="1961">1961</option>
                            <option value="1960">1960</option>
                            <option value="1959">1959</option>
                            <option value="1958">1958</option>
                            <option value="1957">1957</option>
                            <option value="1956">1956</option>
                            <option value="1955">1955</option>
                            <option value="1954">1954</option>
                            <option value="1953">1953</option>
                            <option value="1952">1952</option>
                            <option value="1951">1951</option>
                            <option value="1950">1950</option>
                            <option value="1949">1949</option>
                            <option value="1948">1948</option>
                            <option value="1947">1947</option>
                            <option value="1946">1946</option>
                            <option value="1945">1945</option>
                            <option value="1944">1944</option>
                            <option value="1943">1943</option>
                            <option value="1942">1942</option>
                            <option value="1941">1941</option>
                            <option value="1940">1940</option>
                            <option value="1939">1939</option>
                            <option value="1938">1938</option>
                            <option value="1937">1937</option>
                            <option value="1936">1936</option>
                            <option value="1935">1935</option>
                            <option value="1934">1934</option>
                            <option value="1933">1933</option>
                            <option value="1932">1932</option>
                            <option value="1931">1931</option>
                            <option value="1930">1930</option>
                            <option value="1929">1929</option>
                            <option value="1928">1928</option>
                            <option value="1927">1927</option>
                            <option value="1926">1926</option>
                            <option value="1925">1925</option>
                            <option value="1924">1924</option>
                            <option value="1923">1923</option>
                            <option value="1922">1922</option>
                            <option value="1921">1921</option>
                            <option value="1920">1920</option>
                            <option value="1919">1919</option>
                            <option value="1918">1918</option>
                            <option value="1917">1917</option>
                          </select>

                        </div>
                      </fieldset> -->

                      <div class="text-muted space-top-1">您来到这个世界的神奇日子。此数据仅作分析用途，我们不会透露给其它用户。</div>
                    </div>
                  </div>

          <!-- <div class="row row-condensed space-4">
        <label class="text-right col-sm-3" for="user_email">
          电子邮件地址 <i class="icon icon-lock icon-ebisu" data-behavior="tooltip" aria-label="保密"></i>
        </label>
        <div class="col-sm-9">
          
        <input id="user_email" name="user[email]" size="30" type="text" value="960713393@qq.com">

          <div class="text-muted space-top-1">我们不会向其他爱彼迎用户透露您的个人邮箱地址。<a href="/help/article/694" target="blank">了解更多</a>。</div>
        </div>
      </div> -->
<div class="row row-condensed space-4">
        <label class="text-right col-sm-3" for="user_phone">
          手机号 <i class="icon icon-lock icon-ebisu" data-behavior="tooltip" aria-label="保密"></i>
        </label>
        <div class="col-sm-9">
          
      <div class="clearfix" id="phone-number">
        <div class="phone-numbers-container">
  <div class="phone-numbers-hide-during-verify">
    <div class="no-phone-numbers">
      <p>未输入手机号</p>
    </div>
    <table class="phone-numbers-table" cellspacing="0" cellpadding="0" style="display: none;">
      <tbody>
      <tr class="verified" data-id="78974946" data-number="8618622464594" data-country="CN">
        <th class="edit-profile-confirmable-field__label">
          +86 *** **** 4594
        </th>
        <td>
          <span class="verified">
            <i class="icon icon-ok"></i>
            已确认
          </span>
        </td>
          <td class="remove-container">
            <a href="/phone_numbers/delete/78974946" data-authenticity-token="V4$.airbnb.com$vfWlhOnrc3s$xpopJ53XhKpHOowCz3JlktVqv2CkYsIuy7ivCoCX-Lk=" class="remove" title="删除">
              <i class="icon icon-remove"></i>
            </a>
          </td>
      </tr>
    </tbody>
    </table>
 

      <a class="add link-icon" href="#">
        <i class="icon icon-add"></i>
        <span class="link-icon__text">
          添加手机号
        </span>
      </a>
  </div>

  <div class="phone-number-verify-widget" style="display: block;">
  <p class="pnaw-verification-error"></p>
  <div class="pnaw-step1">
    <div class="phone-number-input-widget" id="phone-number-input-widget-93758e11">
  <label for="phone_country">选择一个国家:</label>
  <div class="select">
    <select id="phone_country" name="phone_country">
      <option value="CN" data-prefix="86" selected="selected">中国</option></select>
  </div>
  <label for="phone_number">添加手机号：</label>
  <div class="pniw-number-container clearfix">
    <div class="pniw-number-prefix">+86</div>
    <input type="tel" class="pniw-number" id="phone_number">
  </div>
  <input type="hidden" data-role="phone_number" name="phone" value="86">
  <input type="hidden" name="user_id" value="138244998">
</div>

    <div class="pnaw-verify-container">
      <a class="btn btn-primary" href="#" rel="sms">通过短信验证</a>
      <a class="cancel" rel="cancel" href="#" style="display: none;">取消</a>
      <a class="why-verify" href="/help/article/277" target="_blank">为什么需要验证？</a>
    </div>
  </div>
  <div class="pnaw-step2">
    <p class="message"></p>
    <label for="phone_number_verification">请输入4位数验证码：</label>
    <input type="text" pattern="[0-9]*" id="phone_number_verification">

    <div class="pnaw-verify-container">
      <a class="btn btn-primary" href="#" rel="verify">
        验证
      </a>
      <a class="cancel" rel="cancel" href="#">
        取消
      </a>
    </div>
    <p class="cancel-message">如果您没有收到验证码，请点击取消，然后尝试电话验证。</p>
  </div>
</div>
</div>

      </div>

          <div class="text-muted space-top-1">只有在您和另一名爱彼迎用户确认预订时，此资料才会被分享。这是我们帮助大家联系彼此的方式。</div>
        </div>
      </div>

<div class="row row-condensed space-4">
  <label class="text-right col-sm-3" for="user_profile_info_preferred_language">
    首选语言 
  </label>
  <div class="col-sm-9">

    <div class="select">
      <select id="user_profile_info_preferred_language" name="user[preferred_locale]">

        <option value="zh" selected="selected">中文 (简体)</option>
      </select>
    </div>

    <div class="text-muted space-top-1">我们将会使用此语言给您发消息。</div>
  </div>
</div>

<div class="row row-condensed space-4">
  <label class="text-right col-sm-3" for="user_profile_info_preferred_currency">
    首选货币 
  </label>
  <div class="col-sm-9">

    <div class="select">
      <select id="user_profile_info_preferred_currency" name="user[native_currency]">

        <option value="CNY" selected="selected">CNY</option>
      </select>
    </div>

    <div class="text-muted space-top-1">我们将会使用此货币为您显示价格。</div>
  </div>
</div>

<div class="row row-condensed space-4">
  <label class="text-right col-sm-3" for="user_profile_info_current_city">
    您住的地方 
  </label>
  <div class="col-sm-9">

    <input id="user_profile_info_current_city" name="user_profile_info[current_city]" placeholder="例如，法国巴黎 / 纽约布鲁克林 / 伊利诺伊州芝加哥" size="30" type="text" value="">


  </div>
</div>

<div class="row row-condensed space-4">
  <label class="text-right col-sm-3" for="user_profile_info_about">
    自我介绍 
  </label>
  <div class="col-sm-9">

    <textarea cols="40" id="user_profile_info_about" name="user_profile_info[about]" rows="5"></textarea>

    <div class="text-muted space-top-1">一路一居是建立在相互关系上的，请帮助其他人了解您。<br><br> 告诉大家您喜欢什么，您离开了哪几样东西就不能活？分享您最喜爱的旅游目的地丶书籍丶电影丶电视节目丶音乐以及食物。<br><br>告诉他们您会是个什么样的房东或房客，您旅游的风格是怎样的？<br><br>向大家介绍一下自己吧，您有什么人生格言吗？</div>
  </div>
</div>
</div>
</div>




<button type="submit" class="btn btn-primary btn-large">
  保存
</button>
</form>


<div class="modal reauth-modal" role="dialog" aria-hidden="true">
  <div class="modal-table">
    <div class="modal-cell">
      <div class="modal-content signup">

        <div id="reauthenticate_container">
          <form accept-charset="UTF-8" action="?" id="reauth_form" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"><input name="authenticity_token" type="hidden" value="V4$.airbnb.com$vfWlhOnrc3s$xpopJ53XhKpHOowCz3JlktVqv2CkYsIuy7ivCoCX-Lk="></div>
            <h3 class="panel-header panel-header-gray">
              确认密码以继续操作
            </h3>
            <div class="panel-header alert alert-header alert-with-icon alert-danger" hidden="">
             <i class="icon icon-alert-alt alert-icon"></i>
             <span class="reauth-error"></span>
           </div>
           <div class="panel-padding panel-body">
            <div class="textInput text-input-container" id="inputEmail">
              <input name="id" type="hidden" value="138244998">
              <input class="decorative-input" id="confirm_password" name="password" placeholder="密码" type="password">
              <div class="clearfix control-group space-top-2 text-right">
                <a href="/forgot_password?email=960713393%40qq.com" class="forgot-password">忘记密码？</a>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">
              确认密码
            </button>
          </div>
        </form></div>

      </div>
    </div>
  </div>
</div>


</div>
</div>
</div>
</div>
</main>
<script type="text/javascript">
$(function() {
    $('input[name="birthdate"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    }, 
    function(start, end, label) {
        var years = moment().diff(start, 'years');
        //alert("You are " + years + " years old.");
    });
});
</script>
</body>
</html>