<html xmlns:fb="http://ogp.me/ns/fb#" lang="zh"><!--<![endif]-->
<head>
	<?php
	$this->view('public/bnb.meta.html');
	?>

</head>

<body class="with-new-header has-epcot-header p1-header-view has_scrolled_below_fold"><a class="screen-reader-only screen-reader-only-focusable skip-to-content" data-hook="skip-to-content" href="#site-content">
	跳至内容
</a>
<span class="screen-reader-only">
	爱彼迎
</span>
<div id="smart-banner" class="smart-banner" aria-hidden="true"></div>

<?php
$this->view('public/bnb.login.header.php');

?>

<main id="site-content" role="main" tabindex="-1">



	<div class="subnav hide-print">
		<div class="page-container-responsive">
			<ul class="subnav-list">
				<li>
					<a href="/users/edit" aria-selected="true" class="subnav-item" id="user-profile-item">个人资料</a>
				</li>
			</ul>
		</div>
	</div>


	<div id="notification-area"></div>
	<div class="page-container-responsive space-top-4 space-4">
		<div class="row">
			<div class="col-md-3 space-sm-4">
				<div class="sidenav">
					<ul class="sidenav-list">
						<li>
							<a href="http://47.92.72.19/user/personalData" aria-selected="false" class="sidenav-item">编辑个人资料</a>
						</li>
						<li>
							<a href="http://47.92.72.19/user/personalPhoto"" aria-selected="true" class="sidenav-item">照片和视频</a>
						</li>
						<li>
							<a href="#" aria-selected="false" class="sidenav-item">信任和验证</a>
						</li>
						<li>
							<a href="http://47.92.72.19/user/personalEvaluate" aria-selected="false" class="sidenav-item">评价</a>
						</li>
						<li>
							<a href="#" aria-selected="false" class="sidenav-item">推荐语</a>
						</li>
						<li>
						</li>
					</ul>
				</div>


				<a href="http://47.92.72.19/user/personalInfo" class="btn btn-block space-top-4" style="border: 1px solid rgb(196,196,196)">查看个人资料</a>
			</div>
			<div class="col-md-9">

				<div id="dashboard-content">

					<div class="panel space-4" style="border: 1px solid #dce0e0;border-radius: 0">
						<div class="panel-header">
							个人头像
						</div>
						<div class="panel-body photos-section" >
							<div class="row">
								<div class="col-lg-4 text-center">
									<div class="profile_pic_container picture-main space-sm-2 space-md-2" data-picture-id="336274025">
										<div class="media-photo profile-pic-background" >
											<img alt="<?php echo  $realname;?>" height="225" src="http://47.92.72.19/<?php echo $avatr;?>"  id="personal_photo_square" width="225">
										</div>
										<div class="media-photo media-round"  >
											<img alt="<?php echo $realname;?>" height="225" src="http://47.92.72.19/<?php echo $avatr;?>"  width="225" id="personal_photo_round">
										</div>
										<!-- <div  class="picture-tile-delete overlay-btn" data-behavior="tooltip" aria-label="删除" style="position: absolute;top: 2px;right: 2px;width: 24px;height: 24px;background: #000;padding-top: 3px;border: 0;border-radius: 2px;opacity:0.7;cursor: pointer;" >
											<i class="icon icon-trash" style="color: #fff;opacity: 1;"></i>
										</div> -->
									</div>
								</div>
								<div class="col-lg-8">
									<ul class="list-layout picture-tiles clearfix ui-sortable"></ul>

									<p>
										清晰的正面脸部照片是房东和房客互相认识对方的重要途径。您能想象把自己的房子租给一只猫吗？
										请确保使用能够清楚显示您面部的照片，并确认其中不包含任何您不希望其他房东或房客看到的个人或敏感信息。
									</p>

									<div class="row row-condensed">
										<div class="col-md-6 space-sm-2 js-take-photo">
											<button class="btn btn-block btn-large" style="border: 1px solid rgb(196,196,196)">
												用网络摄像头拍一张照片
											</button>
										</div>
										<div class="col-md-6">
										<input class="btn btn-block btn-large" type="file" style="border: 1px solid rgb(196,196,196);display: none;" id="file_personal_photo" value="从您的计算机上传照片">
											<span class="file-input-container">
												<form accept-charset="UTF-8" action="https://zh.airbnb.com/users/ajax_image_upload" enctype="multipart/form-data" id="ajax_upload_form" method="post" name="ajax_upload_form" target="upload_frame"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"><input name="authenticity_token" type="hidden" value="V4$.airbnb.com$2We7W8CIKsQ$Nq9mispBsj67H2DeQr-oreBVS8BcEW-IyoqhAGV1qaM="></div>
													<!-- <input id="user_id" name="user_id" type="hidden" value="138244998">
													<input id="callback" name="callback" type="hidden" value="EditProfile">
													<input accept="image/*" capture="camera" id="user_profile_pic"  tabindex="0" type="file"> -->
													<input type = "file" id = "user_profile_pic" name = "file" />
													<label class="btn btn-block btn-large" type="file" style="border: 1px solid rgb(196,196,196)" id="send_personal_photo">从您的计算机上传照片</label>
												</form>              
												<!-- <iframe id="upload_frame" name="upload_frame" style="display:none;"></iframe> -->
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>




		</div>
	</div>
</div>
</div>

</main>
<div id="footer-promo-mount"></div>
<div id="footer" class="container-brand-light footer-surround footer-container">
	<?php  $this->view('public/bnbfooter.html');  ?>
</div>
<div id="fb-root"></div>
<div>
	<div data-reactroot=""><span></span></div>
</div>

</body>
</html>