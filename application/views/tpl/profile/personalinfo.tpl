<html xmlns:fb="http://ogp.me/ns/fb#" lang="zh"><!--<![endif]-->
<head>
	<?php
	$this->view('public/bnb.meta.html');
	?>

</head>

<body class="with-new-header has-epcot-header p1-header-view has_scrolled_below_fold"><a class="screen-reader-only screen-reader-only-focusable skip-to-content" data-hook="skip-to-content" href="#site-content">
	跳至内容
</a>
<span class="screen-reader-only">
	爱彼迎
</span>
<div id="smart-banner" class="smart-banner" aria-hidden="true"></div>

<?php
$this->view('public/bnb.login.header.php');

?>

<main id="site-content" role="main" tabindex="-1">


	<div id="recommendation_container" class="clearfix container">
	</div>




	<div class="page-container page-container-responsive space-top-4 space-8">
		<div class="row">
			<div class="col-lg-3 col-md-4 hide-sm">
				<div id="user" class="space-4 ">
					<div>
						<div class="space-2" id="user-media-container">
							<div id="slideshow" class="slideshow">
								<div class="slideshow-preload"></div>
								<ul class="slideshow-images" style="height: 235.5px;">
									<li class="active media-photo media-photo-block">
										<img alt="<?php echo  $realname;?>" class="img-responsive" height="225" src="http://47.92.72.19/<?php echo $avatr;?>" width="225">
									</li>
									<li class="media-photo media-photo-block"></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="panel space-4" style="border: 1px solid #dce0e0;border-radius: 0">
					<div class="panel-header">
						已验证的信息
					</div>
					<div class="panel-body">
						<ul class="list-unstyled space-3" title="已验证的信息">
							<li class="row row-table space-2">
								<div class="col-12 col-middle">
									电子邮件地址
								</div>
								<div class="col-3 col-middle">
									<i class="icon icon-ok h3" aria-hidden="true"></i>
									<span class="screen-reader-only">已验证</span>
								</div>
							</li>
							<li class="row row-table space-2">
								<div class="col-12 col-middle">
									手机号
								</div>
								<div class="col-3 col-middle">
									<i class="icon icon-ok h3" aria-hidden="true"></i>
									<span class="screen-reader-only">已验证</span>
								</div>
							</li>
						</ul>

						<a href="https://zh.airbnb.com/users/edit_verification">
							验证更多信息
						</a>
					</div>
				</div>


			</div>

			<div class="col-lg-9 col-md-8 col-sm-12">
				<div class="row space-4">
					<div class="col-sm-4 show-sm">
						<div class="media-photo media-photo-block media-round">
							
						</div>
					</div>
					<div class="col-sm-8 col-md-12 col-lg-12">

						<h1>
						<?php echo '你好我是 '.$realname; ?>
						</h1>
						<div class="h5 space-top-2">
							<a href="#" class="link-reset">CN</a>
							·
							<span class="text-normal">
								<?php echo '注册时间：2017年7月'; ?>
							</span>
						</div>
						<!-- <div class="text-muted space-top-2">
							<style data-aphrodite="data-aphrodite"></style>
							<div data-hypernova-key="user_flag_controlbundlejs" data-hypernova-id="42748a48-1605-4933-95b6-cb7bcbed8b22">
								<div data-reactroot="" data-reactid="1" data-react-checksum="1200781566">
									<div class="flag-widget text-small" data-reactid="2">
										<button class="btn-link btn-link--reset btn-link--icon" type="button" title="举报此用户" data-reactid="3">
											<i class="icon icon-flag h4" data-reactid="4"></i>
											<span data-reactid="5">
												<span class="link-icon__text" data-reactid="7"><span data-reactid="8">举报此用户</span></span>
											</span>
										</button>
									</div>
								</div>
							</div>

						</div> -->
						<div class="edit_profile_container space-3">
							<a href="http://47.92.72.19/user/personalData">编辑个人资料</a></div>
						</div>
					</div>
					<div class="space-top-2">
						<p></p>
					</div>

					<style data-aphrodite="data-aphrodite"></style>



					<div class="social_connections_and_reviews">

					</div>
				</div>
			</div>
		</div>




		<div id="staged-photos"></div>

	</main>


	<div id="footer-promo-mount"></div>
	<div id="footer" class="container-brand-light footer-surround footer-container">
		<?php  $this->view('public/bnbfooter.html');  ?>
	</div>
	<div id="fb-root"></div>
	<div>
		<div data-reactroot=""><span></span></div>
	</div>
</body>
</html>