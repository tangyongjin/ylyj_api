<div>
<span style="font-size:0;"></span><div><div class="SearchForm row" role="search"><form action="/s" method="get" style="margin-bottom: 0">
<div class="SearchForm__inputs-wrapper col-md-12">
<div class="SearchForm__row row"><div class="SearchForm__location">
    <label for="search-location" class="SearchForm__label"><span>地点</span></label>
    <div>
        <div>
            <input class="LocationInput input-large" name="location" placeholder="目的地，城市，地址" autocomplete="off" id="search-location" value="" type="text">
            <input type="hidden" id="province" value="">
            <input type="hidden" id="city" value="">
        </div>
        <div class="DropDownWrapper col-md-12"></div>
    </div>
</div>

<script>
    let locationinput = $('.LocationInput');

    //locationinput.click(()=>{alert(111)})

    $.ajax({
            cache: true,
            type: "POST",
            url:'http://47.92.72.19/room/citylist',
            data:'',
            async: false,
            error: function(request) {
                console.log('error')
            },
            success: function(data) {
                var cityPicker = new IIInsomniaCityPicker({
                    data: data,
                    target: '.LocationInput',
                    valType: 'k-v',
                    hideCityInput: '#city',
                    hideProvinceInput: '#province',
                    callback: function(city_id){
            //alert(city_id);
                }
                
            });
                cityPicker.init();

            }
        });
    // var cityPicker = new IIInsomniaCityPicker({
    //     data: cityData,
    //     target: '.LocationInput',
    //     valType: 'k-v',
    //     hideCityInput: '#city',
    //     hideProvinceInput: '#province',
    //     callback: function(city_id){
    //         //alert(city_id);
    //     }
    // });

    //cityPicker.init();
</script>
<div class="SearchForm__dates text-left">
    <label for="startDate" class="SearchForm__label"><span>时间</span></label>
    <input type="text" name="datefilter" value=""  style="border:0;outline:none;color: #767676;" placeholder="入住日期 - 退房日期" />
    <div class="DateRangePicker">
    
        <!-- <div>
            <div class="DateRangePickerInput">
                <div class="DateInput">
                    <input aria-label="入住日期" class="DateInput__input needsclick" id="startDate" name="startDate" value="" placeholder="入住日期" autocomplete="off" aria-describedby="DateInput__screen-reader-message-startDate" type="text">
                    <p id="DateInput__screen-reader-message-startDate" class="screen-reader-only">按住向下箭头键来和日历交互并选择日期。按住问号来获取更改日期的键盘快捷键。</p>
                    <div class="DateInput__display-text">入住日期</div>
                </div>
                <div class="DateRangePickerInput__arrow" aria-hidden="true" role="presentation"></div>
                <div class="DateInput">
                    <input aria-label="退房日期" class="DateInput__input needsclick" id="endDate" name="endDate" value="" placeholder="退房日期" autocomplete="off" aria-describedby="DateInput__screen-reader-message-endDate" type="text">
                    <p id="DateInput__screen-reader-message-endDate" class="screen-reader-only">按住向下箭头键来和日历交互并选择日期。按住问号来获取更改日期的键盘快捷键。
                    </p>
                    <div class="DateInput__display-text">退房日期</div>
                </div>
                <button type="button" aria-label="清除日期" class="DateRangePickerInput__clear-dates DateRangePickerInput__clear-dates--hide">
                    <div class="DateRangePickerInput__close-icon">
                    </div>
                </button>
            </div>
        </div> -->
    </div>
</div>
<script>
let datefilter = $('input[name="datefilter"]')

    datefilter.daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: '取消',
                daysOfWeek: [
                "日",
                "一",
                "二",
                "三",
                "四",
                "五",
                "六"
                ],
                monthNames: [
                "一月",
                "二月",
                "三月",
                "四月",
                "五月",
                "六月",
                "七月",
                "八月",
                "九月",
                "十月",
                "十一月",
                "十二月"
                ]
            }
        });
        datefilter.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        });

        datefilter.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
</script>
<div class="SearchForm__guests text-left">
    <label for="how-many-guests" class="SearchForm__label"><span>房客</span></label>
    <div class="guest-label">
                <input  class="input-guest" type="text" name="" placeholder="1位房客" style="border:0;outline:none;"" onKeyUp="value=value.replace(/[^\d]/g,'') " onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))">
            </div>
    <div class="GuestPickerTrigger">
        <button class="GuestPickerTrigger__button" type="button">
            <!-- <div class="guest-label"><span class="guest-label__text guest-label__text-guests"><span>1位房客</span></span>
            </div> -->
            
            <div class="dropdown-icon"></div>
        </button>
        <div>
            <div class="GuestPicker hide pointer">
                <div style="margin-top:16px;margin-bottom:16px;margin-left:0;margin-right:0;">
                    <div style="margin-top:16px;margin-bottom:16px;margin-left:0;margin-right:0;">
                        <fieldset class="container_z8lbtc">
                            <legend class="visuallyHidden_1m8bb6v">
                                <span>请设置成人数量。</span></legend>
                            <div class="containerFallback_1cmo568-o_O-container_1s8su0z">
                                <div class="beforeFallback_1iti0ju-o_O-before_t0tx82">
                                    <div class="table_qtix31">
                                        <div id="StepIncrementerRow-title-GuestCountFilter-GuestPicker-p1-search_form-adults">
                                            <div class="text_5mbkop-o_O-size_regular_x6m9oi"><span>成人</span></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- react-text: 68 -->
                                <!-- /react-text -->
                                <div class="afterFallback_1iti0ju">
                                    <div class="table_qtix31-o_O-buttons_y5lyrw">
                                        <div class="middleAlignedCell_17u98ky-o_O-left_e4x16a">
                                            <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="减" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-adults" disabled=""><span class="icon_16b32nl-o_O-icon_1wy8x1d-o_O-icon_small_abrv9k">
</span></button>
                                        </div>
                                        <div id="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-adults" role="region" aria-live="polite" aria-label="1名成人" class="middleAlignedCell_17u98ky-o_O-center_12to336">
                                            <div class="text_5mbkop-o_O-size_regular_x6m9oi">1</div>
                                        </div>
                                        <div class="middleAlignedCell_17u98ky-o_O-right_140n9qh">
                                            <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="加" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-adults"><span class="icon_16b32nl-o_O-icon_11jdm7g-o_O-icon_small_abrv9k">

</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div style="margin-top:24px;margin-bottom:24px;margin-left:0;margin-right:0;">
                        <fieldset class="container_z8lbtc">
                            <legend class="visuallyHidden_1m8bb6v"><span>请设置儿童数量。</span></legend>
                            <div class="containerFallback_1cmo568-o_O-container_1s8su0z">
                                <div class="beforeFallback_1iti0ju-o_O-before_t0tx82">
                                    <div class="table_qtix31">
                                        <div id="StepIncrementerRow-title-GuestCountFilter-GuestPicker-p1-search_form-children">
                                            <div class="text_5mbkop-o_O-size_regular_x6m9oi"><span>儿童</span></div>
                                        </div>
                                        <div id="StepIncrementerRow-subtitle-GuestCountFilter-GuestPicker-p1-search_form-children" class="subtitle_ac6rph">
                                            <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x"><span>2 - 12岁</span></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- react-text: 97 -->
                                <!-- /react-text -->
                                <div class="afterFallback_1iti0ju">
                                    <div class="table_qtix31-o_O-buttons_y5lyrw">
                                        <div class="middleAlignedCell_17u98ky-o_O-left_e4x16a">
                                            <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="减" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-children" disabled=""><span class="icon_16b32nl-o_O-icon_1wy8x1d-o_O-icon_small_abrv9k">


</span></button>
                                        </div>
                                        <div id="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-children" role="region" aria-live="polite" aria-label="0名儿童" class="middleAlignedCell_17u98ky-o_O-center_12to336">
                                            <div class="text_5mbkop-o_O-size_regular_x6m9oi">0</div>
                                        </div>
                                        <div class="middleAlignedCell_17u98ky-o_O-right_140n9qh">
                                            <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="加" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-children"><span class="icon_16b32nl-o_O-icon_11jdm7g-o_O-icon_small_abrv9k">


</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div style="margin-top:24px;margin-bottom:24px;margin-left:0;margin-right:0;">
                        <fieldset class="container_z8lbtc">
                            <legend class="visuallyHidden_1m8bb6v"><span>请设置婴幼儿数量。</span></legend>
                            <div class="containerFallback_1cmo568-o_O-container_1s8su0z">
                                <div class="beforeFallback_1iti0ju-o_O-before_t0tx82">
                                    <div class="table_qtix31">
                                        <div id="StepIncrementerRow-title-GuestCountFilter-GuestPicker-p1-search_form-infants">
                                            <div class="text_5mbkop-o_O-size_regular_x6m9oi"><span>婴幼儿</span></div>
                                        </div>
                                        <div id="StepIncrementerRow-subtitle-GuestCountFilter-GuestPicker-p1-search_form-infants" class="subtitle_ac6rph">
                                            <div class="text_5mbkop-o_O-size_small_1gg2mc-o_O-weight_light_1nmzz2x"><span>2岁以下</span></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- react-text: 126 -->
                                <!-- /react-text -->
                                <div class="afterFallback_1iti0ju">
                                    <div class="table_qtix31-o_O-buttons_y5lyrw">
                                        <div class="middleAlignedCell_17u98ky-o_O-left_e4x16a">
                                            <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="减" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-infants" disabled=""><span class="icon_16b32nl-o_O-icon_1wy8x1d-o_O-icon_small_abrv9k">


</span></button>
                                        </div>
                                        <div id="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-infants" role="region" aria-live="polite" aria-label="0名婴幼儿" class="middleAlignedCell_17u98ky-o_O-center_12to336">
                                            <div class="text_5mbkop-o_O-size_regular_x6m9oi">0</div>
                                        </div>
                                        <div class="middleAlignedCell_17u98ky-o_O-right_140n9qh">
                                            <button class="button_qq3sll-o_O-button_1ju6z0l-o_O-button_small_1h534dv-o_O-button_light_1a24k02" type="button" aria-label="加" aria-controls="StepIncrementerRow-value-GuestCountFilter-GuestPicker-p1-search_form-infants"><span class="icon_16b32nl-o_O-icon_11jdm7g-o_O-icon_small_abrv9k">

</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div>
                        <div class="containerFallback_1cmo568-o_O-container_1s8su0z">
                            <div class="beforeFallback_1iti0ju-o_O-before_t0tx82"></div>
                            <!-- react-text: 145 -->
                            <!-- /react-text -->
                            <div class="afterFallback_1iti0ju">
                                <div class="text_5mbkop-o_O-size_regular_x6m9oi">
                                    <button aria-disabled="false" class="component_9w5i1l-o_O-component_button_r8o91c" type="button"><span>关闭</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="SearchForm__submit">
    <button type="button" class="btn btn-primary btn-large btn-block " id="btn-search" style="border-color: #ff5a5f;background-color: #ff5a5f;color: #fff;">
        <div>
            <div class="hide-lg">
            </div><span class="SearchForm__submit-text hide-md"><span>搜索</span></span>
        </div>
    </button>
</div>
</div></div><input name="source" value="bb" type="hidden"></form></div></div><!-- react-empty: 318 -->
</div>




<script>
    let btnSearch = $('#btn-search'),
    inputGuest = $('.input-guest')

    btnSearch.click(()=>{

        //let searchurl='http://www.yiluyiju.com/room/searchRoom';
        let searchData = {}
        locationinput.val()==''?searchData.location='0':searchData.location=locationinput.val();
        // datefilter.val()==''?searchData.startDate='0':searchData.startDate=datefilter.val().split(' ')[0].replace(/\//g,'-')
        // datefilter.val()==''?searchData.endDate='0':searchData.endDate=datefilter.val().split(' ')[0].replace(/\//g,'-')

        datefilter.val()==''?searchData.startDate='0':searchData.startDate=new Date(Date.parse(datefilter.val().split(' ')[0].replace(/-/g,  "/"))).getTime()/1000;
        datefilter.val()==''?searchData.endDate='0':searchData.endDate=new Date(Date.parse(datefilter.val().split(' ')[2].replace(/-/g,  "/"))).getTime()/1000;

        inputGuest.val() ===''?searchData.guestNumber = 1:searchData.guestNumber = inputGuest.val()
        window.open('http://47.92.72.19/room/search/' + searchData.location +'/' + searchData.startDate + '/' + searchData.endDate + '/' + searchData.guestNumber);
    })


</script>