{foreach xrooms as oneroom}
<?php
//debug($oneroom['comment_info']['comment_counter']);
//die;
?>


  <div class="cardRoom">
    <a href="/room/detail/{oneroom['pid']}" class="link-reset" target="_blank">
        <div class="container">
            <div class="image_e296pg">
                <div class="carouselContainer">
                    <div class="container_18q6tiq" style="padding-top: 66.6667%; background: rgb(216, 216, 216) none repeat scroll 0% 0%;">
                        <div class="children">
                            <div class="container" style="width: 100%; height: 100%;">
                                <div class="image-background" style="width: 100%; height: 100%; background-image: url({oneroom['roompic']});">
                                    
                                </div>
                            </div>
                            <div>
                                <div class="navigation_10isrgg">
                                    <div class="container_sm"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottomRow">
                <div class="twoLineTitle"><span class="text_bold">
<!-- react-text: 1224 -->￥{oneroom['price_day']}<!-- /react-text -->
<!-- react-text: 1225 --> 
<!-- /react-text -->
</span><span class="text_5mbkop">
<!-- react-text: 1227 -->{oneroom['city']} - {oneroom['roomtitle']}<!-- /react-text -->
</span></div>


                <div style="margin: 4px 0px 0px;">
                    <div class="inlineBlock" style="margin: 0px 8px 0px 0px;">
                        <span class="inlineBlockMiddle">
        <div><span class="starRatingContainer"><span role="img" aria-label="评分是5（满分为5）">
        <span class="sstar_sizeSmall"></span><span class="sstar_sizeSmall"></span>
                        <span class="sstar_sizeSmall"></span>
                        <span class="sstar_sizeSmall"></span><span class="sstar_sizeSmall"></span></span>
                        </span><span aria-label="172条评价"><span class="text_counter"><span>{oneroom['comment_info']['comment_counter']}条评价</span></span>
                        </span>
                    </div>
                    </span>
                </div>
            </div>
        </div>
</div>
</a>
</div>


{/foreach}

 