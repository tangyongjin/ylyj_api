<html lang="zh">
<head>
<?php  
   $this->view('public/bnb.meta.html');
?>

</head>
  
  <body class="with-new-header has-epcot-header p1-header-view has_scrolled_below_fold"><a class="screen-reader-only screen-reader-only-focusable skip-to-content" data-hook="skip-to-content" href="#site-content">
      跳至内容
    </a>
<span class="screen-reader-only">
      爱彼迎
    </span>
<div id="smart-banner" class="smart-banner" aria-hidden="true"></div>

<?php  
   
   if(isset($loggedin)){
          $this->view('public/bnb.login.header.php');
   }else
   {
         $this->view('public/bnb.header.php');
   }
    
?>

<div id="search-modal">
    <div data-reactroot=""></div>
</div>
<style data-aphrodite=""></style>
<div data-hypernova-key="signup_login_dlsauth_modalsbundlejs">
    <!-- react-empty: 1 -->
</div>
<img src="/pg_pixel" style="position: absolute; height: 0; width: 0; top: -999px; left: -999px">
<div id="field-guide-container"> 

<?php
 $this->view('public/bnb.help.php');
?>


</div>
<div class="flash-container"></div>
<main id="site-content" role="main" tabindex="-1">
    <div data-hypernova-key="p1page_containerbundlejs">
        <div class="page_e296pg" data-reactroot="">
            <!-- react-empty: 2 -->
            <div class="pageContainer_pheyz5-o_O-pageContainer_verticalSpacingTop_141lvco" style="background-color:transparent;">
                <div class="textHeaderContainer_peyti4">

                 
                    <h1 class="textHeader_8yxs9w"><span class="textHeaderTitle_153t78d-o_O-textHeader_rausch_hp6jb4">一路一居。 </span>
<!-- react-text: 7 -->旅行中，像当地人一样生活。<!-- /react-text -->
</h1>
                </div>
                <div style="margin: 0px 0px 16px;">
                    <button aria-disabled="false" type="button" class="container_71fudf-o_O-container_rounded_sa8zo9-o_O-container_notBlock_1xdomts-o_O-container_sizeRegular_6fka15-o_O-container_1hd6v8d">
                        <span class="text_veo4lx-o_O-text_sizeRegular_15o3clg-o_O-text_1x93kzx"><span>点击领取￥200优惠券</span></span>
                    </button>
                </div>
            </div>
            <div class="pageContainer_pheyz5-o_O-pageContainer_verticalSpacingTop_141lvco" style="background-color:#ffffff;">
                <?php
                  $this->view('tpl/homepage/bnb.search.php');
                ?>
                    <div class="adaptableContainer_6q560o">
                        <div>
                            <div>
                                <div></div>
                                <div><span style="font-size: 0px;"></span>
                                    <div>
                                        <div style="margin: 48px 0px;">
                                            <div class="cnyContainer_1p3do20">
                                                <div class="moduleHeader_zmn36q"><span>热门目的地</span></div>
                                                <div class="citiesTabs_1mk0p35">
                                                    <div>
                                                        <div role="tablist" class="tabs_1xal37k">
                                                            <div class="container">
                                                                <div class="containerWrapper_hideHorizonta">
                                                                    <div class="containerOuter" style="margin-bottom: -15px;">
                                                                        <div class="containerInner" style="margin-bottom: 0px; padding-bottom: 0px;">
                                                                            <button class="text_x6m9oi-o_O-tab_n1jmyo-o_O-tab_small_1o9wu0-o_O-tab_selected_cwuxid" aria-selected="true" type="button">上海</button>
                                                                            <button class="text_button" role="tab" aria-selected="false" type="button">东京</button>
                                                                            <button class="text_button" role="tab" aria-selected="false" type="button">曼谷</button>
                                                                            <button class="text_button" role="tab" aria-selected="false" type="button">大阪</button>
                                                                            <button class="text_button" role="tab" aria-selected="false" type="button">台北</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    <button type="button" class="button_transition" aria-hidden="true" style="opacity: 0; visibility: hidden;">
                                                                        <div class="iconContainer_769vv0-o_O-iconContainer_backwards_w1faho">
                                                                            <div class="icon_qplzdw">
                                                                            </div>
                                                                        </div>
                                                                        <div class="gradient_3cikar-o_O-gradient_backwards_hbkrlm"></div>
                                                                    </button>
                                                                    <button type="button" class="button_forwards" aria-hidden="true" style="opacity: 0; visibility: hidden;">
                                                                        <div class="gradient_3cikar-o_O-gradient_forwards_17ipsx"></div>
                                                                        <div class="iconContainer_769vv0-o_O-iconContainer_forwards_1sqnphj">
                                                                            <div class="icon_qplzdw">
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div></div>
                                                    </div>
                                                </div>
                                                
                                                <div class="listingsContainer_3hmsj">
                                                    <ul id="apartment_room">
                                                        <?php  
                                                            $this->view('tpl/homepage/apartment6.tpl');
                                                        ?>
                                                    </ul>
                                                    <div class="container">
                                                        <!-- react-empty: 1206 -->
                                                        <div class="cardSliderContainer_l7lh6d">
                                                            <div class="cardSlider_disableCarousel">
                                                                <?php  
                                                                   //$this->view('tpl/homepage/apartment6.tpl');
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="buttonContainer_48hmcm">
                                                        <button aria-disabled="false" type="button" class="container_rounded">
                                                            <span class="text_veo4lx-o_O-text_sizeSmall_1gb9qjw-o_O-text_1ku16io"><span>查看更多上海房源</span></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php  
                                          $this->view('tpl/homepage/bnb.trustbanner.tpl');
                                 ?>
                            </div>
                            <div>
                                <div>
                                    <div>
                                        <div style="margin: 48px 0px;"></div>
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        <div style="margin: 48px 0px;"><span style="font-size: 0px;"></span>
                                            <div class="headerContainer_91eznd"></div>
                                            <div class="container_e296pg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- react-empty: 495 -->
                        </div>
                    </div>
            </div>
        </div>
    </div>
</main>
<div id="footer-promo-mount"></div>
<div id="footer" class="container-brand-light footer-surround footer-container">
    <?php  $this->view('public/bnbfooter.html');  ?>
</div>
<div id="fb-root"></div>
<div>
    <div data-reactroot=""><span></span></div>
</div>
</body></html>
