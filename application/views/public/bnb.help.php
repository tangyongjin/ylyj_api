
    <div data-reactroot="">
        <div class="field-guide panel help-side-panel" tabindex="-1" role="region" aria-label="爱彼迎帮助">
            <div>
                <div class="side-panel-header text-center"><a href="/help" class="link-reset"><span>爱彼迎帮助</span></a>
                    <div class="pull-right">
                        <button aria-label="关闭" class="container_1rp5252-o_O-inline_36rlri" type="button" style="padding: 0px;">
                            <svg viewBox="0 0 24 24" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 18px; width: 18px;">
                                <path fill-rule="evenodd" d="M23.25 24a.744.744 0 0 1-.53-.22L12 13.062 1.28 23.782a.75.75 0 0 1-1.06-1.06L10.94 12 .22 1.28A.75.75 0 1 1 1.28.22L12 10.94 22.72.22a.749.749 0 1 1 1.06 1.06L13.062 12l10.72 10.72a.749.749 0 0 1-.53 1.28" />
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="main-content">
                    <div class="text-left search-container mobile-autosuggest">
                        <div>
                            <div class="search-input-container">
                                <div class="search-input-inner-container">
                                    <div class="icon-search-container">
                                        <svg viewBox="0 0 24 24" aria-hidden="true" focusable="false" style="display: block; fill: rgb(118, 118, 118); height: 25px; width: 25px;">
                                            <path fill-rule="evenodd" d="M23.53 22.47l-6.807-6.808A9.455 9.455 0 0 0 19 9.5 9.5 9.5 0 1 0 9.5 19c2.353 0 4.502-.86 6.162-2.277l6.808 6.807a.75.75 0 0 0 1.06-1.06zM9.5 17.5a8 8 0 1 1 0-16 8 8 0 0 1 0 16z" />
                                        </svg>
                                    </div>
                                    <input aria-label="搜索帮助文章" class="search-input dls-search-input" name="q" autocomplete="off" maxlength="1024" value="" placeholder="提问或使用关键词搜索" type="text">
                                    <input name="sid" value="67d50e8b-a89d-40e8-8615-83b210ed0246" type="hidden">
                                </div>
                            </div>
                            <div class="search-results-container text-dark-gray">
                                <div style="margin: 0px;">
                                    <div class="search-results loading"></div>
                                </div>
                                <div class="expandable-indicator"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="help-link-bottom"><a href="/help" data-prevent-default="true"><span>帮助中心</span></a><a class="pull-right" href="/help/feedback"><span>提供反馈</span></a></div>
            </div>
        </div>
    </div>
