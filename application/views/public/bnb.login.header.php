
<header class="topbar" style="border-bottom: 1px solid #dce0e0">
    <div class="topbody clear">
        <div class="title">
            <a href="http://47.92.72.19/">
                <svg id="icon-house" viewBox="0 0 1024 1024" width="34px" height="34px" fill="#d81e06">
                    <path d="M939.648 547.328 532.352 140.032C525.76 133.44 516.992 130.56 508.288 130.944 499.712 130.56 490.944 133.44 484.288 140.032L77.056 547.328c-12.48 12.48-12.48 32.768 0 45.248s32.768 12.48 45.248 0l385.984-385.984 386.048 385.984c12.48 12.48 32.768 12.48 45.248 0S952.128 559.808 939.648 547.328z"></path>
                    <path d="M667.072 834.368c-15.488 0-27.84-11.2-30.72-25.792l0-37.824 0-64.064c0-35.392-28.608-64-64-64l-128 0c-35.392 0-64 28.608-64 64l0 64.064 0 38.72c-3.2 14.336-15.36 25.216-30.656 25.216l-33.024 0.384L284.288 835.072c-17.664 0-32-14.336-32-32L252.288 579.2 252.16 579.2c0-0.192 0.128-0.32 0.128-0.512 0-17.664-14.336-32-32-32s-32 14.336-32 32c0 0.192 0.128 0.32 0.128 0.512L188.288 579.2l0 255.872c0 35.392 28.608 64 64 64 0 0 107.776-0.384 128.32-0.384 34.816 0 62.784-27.84 63.68-62.464 0-0.576 0.32-1.024 0.32-1.536s-0.32-1.024-0.32-1.536l0-94.464c0-17.664 14.336-32 32-32l64 0c17.664 0 32 14.336 32 32l0.32 96c0 35.392 28.608 64 64 64l127.68 0.384c35.392 0 64-28.608 64-64L828.288 579.2 828.16 579.2c0-0.192 0.128-0.32 0.128-0.512 0-17.664-14.336-32-32-32s-32 14.336-32 32c0 0.192 0.128 0.32 0.128 0.512l-0.128 0 0 223.872c0 17.664-14.336 32-32 32l-31.744 0"></path>
                </svg>
            </a>
        </div>
        <ul class="toplist" style="margin-top: 10px;">
            <li><span class="toplist_span" id="header_app">手机端</span></li>
            <li><span class="toplist_span" id="header_help">帮助</span></li>
            <li><span class="toplist_span" id="header_order">订单</span></li>
            <li>
                <span class="toplist_span toplist_img">
                    <img src="http://47.92.72.19/<?php echo $avatr;?>" alt="" id="header_img"/>
                    <!-- <img src="http://47.92.72.19/images/users/u9999.gif" alt="" id="header_img"/> -->
                    
                </span>
                <div class="container_personal_info">
                    <div class="content_cover"></div>
                    <div class="content_personal_info">
                        <ul>
                            <li><div class="content_border header_personal_data"><span class="">个人资料</span></div></li>
                            <li><div class="content_border header_signout"><span class="">退出</span></div></li>
                        </ul>
                    </div>
                    <svg class="fang_yyjois" style="right: 28px;z-index: 100">
                        <path d="M0,10 20,10 10,0z" class="fangShape_1s2t3cl"></path>
                        <path d="M0,10 10,0 20,10" class="fangStroke_tjp8vr"></path>
                    </svg>
                </div>
            </li>
        </ul>
    </div>
</header>
<script>
   
        signout = $('.header_signout');
        signout.click(function(e){
            //alert(1)
            e.stopPropagation();
            var url='http://47.92.72.19/user/logout';
            $.ajax({
                cache: true,
                type: "POST",
                url:url,
                data:' ',
                async: false,
                error: function(request) {
                    console.log('error')
                },
                success: function(data) {
                    console.log('ok')
                    window.location.reload();
                }
            });
         //window.location.reload();
        })
        personaldata = $('.header_personal_data')
        personaldata.click(function(e){
            e.stopPropagation();
            window.location.href='http://47.92.72.19/user/personalData';
        })
</script>
