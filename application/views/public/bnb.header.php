<header class="topbar">
    <div class="topbody clear">
        <div class="title">
        <a href="http://47.92.72.19/">
            <svg id="icon-house" viewBox="0 0 1024 1024" width="34px" height="34px" fill="#d81e06"><path d="M939.648 547.328 532.352 140.032C525.76 133.44 516.992 130.56 508.288 130.944 499.712 130.56 490.944 133.44 484.288 140.032L77.056 547.328c-12.48 12.48-12.48 32.768 0 45.248s32.768 12.48 45.248 0l385.984-385.984 386.048 385.984c12.48 12.48 32.768 12.48 45.248 0S952.128 559.808 939.648 547.328z"></path>
            <path d="M667.072 834.368c-15.488 0-27.84-11.2-30.72-25.792l0-37.824 0-64.064c0-35.392-28.608-64-64-64l-128 0c-35.392 0-64 28.608-64 64l0 64.064 0 38.72c-3.2 14.336-15.36 25.216-30.656 25.216l-33.024 0.384L284.288 835.072c-17.664 0-32-14.336-32-32L252.288 579.2 252.16 579.2c0-0.192 0.128-0.32 0.128-0.512 0-17.664-14.336-32-32-32s-32 14.336-32 32c0 0.192 0.128 0.32 0.128 0.512L188.288 579.2l0 255.872c0 35.392 28.608 64 64 64 0 0 107.776-0.384 128.32-0.384 34.816 0 62.784-27.84 63.68-62.464 0-0.576 0.32-1.024 0.32-1.536s-0.32-1.024-0.32-1.536l0-94.464c0-17.664 14.336-32 32-32l64 0c17.664 0 32 14.336 32 32l0.32 96c0 35.392 28.608 64 64 64l127.68 0.384c35.392 0 64-28.608 64-64L828.288 579.2 828.16 579.2c0-0.192 0.128-0.32 0.128-0.512 0-17.664-14.336-32-32-32s-32 14.336-32 32c0 0.192 0.128 0.32 0.128 0.512l-0.128 0 0 223.872c0 17.664-14.336 32-32 32l-31.744 0"></path></svg>
        </a>
        </div>
        <ul class="toplist">
            <li><span class="toplist_span" id="header_app">手机端</span></li>
            <li><span class="toplist_span" id="header_help">帮助</span></li>
            <li><span class="toplist_span" id="header_signup">注册</span></li>
            <li><span class="toplist_span" id="header_signin">登录</span></li>
        </ul>
    </div>

    <div class="main">
                <div class="cover"></div>
                <div class="box">
                    <div class="content">
                        <a href="http://www.baidu.com" style="text-decoration: none;">                      
                            <button class="btn icon-btn btn-block btn-large signup-login-form__btn-xl space-1 btn-wechat btn-block signup-login-form__btn-xl space-1 btn-large" name="button" type="submit" >
                                <span class="icon-container">
                                    <i class="icon icon-wechat"></i>
                                </span>
                                <span class="text-container">使用微信帐号注册</span>
                            </button>
                        </a>
                        <form>
                            <button class="btn icon-btn btn-block btn-large signup-login-form__btn-xl space-1 btn-alipay btn-block signup-login-form__btn-xl space-1 btn-large" name="button" type="submit">
                                <span class="icon-container">
                                    <i class="icon icon-alipay"></i>
                                </span>
                                <span class="text-container">使用支付宝注册</span>
                            </button>
                        </form>
                            <button class="btn icon-btn btn-block btn-large signup-login-form__btn-xl space-1 btn-phone btn-block signup-login-form__btn-xl space-1 btn-large" name="button" type="submit">
                                <span class="text-container">使用手机号注册</span>
                            </button>
                    </div>
                </div>
            </div>
            <div class="phone-signup">
                <div class="signup-cover"></div>
                <div class="signup-list">
                    <div class="phone-signup-form">
                        <form accept-charset="UTF-8" action="/create" class="signup-form-phone" data-action="Signup" id="user_new"
                        method="post">
                        <div style="margin:0;padding:0;display:inline"></div>
                            <div class="phone-number-signup-form-fields">
                                <div class="header-with-phone-signup-option space-3">
                                    <h4>用电话号码注册
                                    </h4>
                                </div>
                                <div class="control-group space-1">
                                    <!-- <div class="row row-condensed">
                                        <div class="col-sm-12 input-addon"> -->
                                            <input type="tel" class="input-stem decorative-input input-mobile" name="national_number"
                                            aria-label="手机号" placeholder="手机号" value="">
                                            <div class="phone-check-signup">请输入手机号</div>
                                        <!-- </div>
                                    </div> -->
                                </div>
                                <div class="control-group space-1" id="check-num">
                                    <input type="text" name="check_num" aria-label="验证码" placeholder="验证码"
                                    class="check_num" value="">
                                    <input class="btn-send-check-num" type="button" name="" value="发送验证码"/>
                                </div>
                                <div class="control-group space-1">
                                    <input type="text" name="first_name" aria-label="姓名" placeholder="姓名"
                                    class="decorative-input" value="">
                                </div>
                                <div class="control-group space-1 ">
                                    <input type="text" name="random_number" aria-label="随机数" placeholder="随机数" value="" class="random_number">
                                </div>

                                <div class="control-group space-1">
                                    <input type="password" data-hook="user_password" name="password" aria-label="密码"
                                    placeholder="密码(6~12位数字和字母组成)" class="decorative-input inspectletIgnore input-password">
                                    <div class="password-check-signup"></div>
                                    <div>
                                        <input  type="button" id="btn-signup" 
                                        class="btn btn-primary btn-block signup-login-form__btn-xl btn-large finished-phone-signup-fields" value="注册">
                                    </input>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            <div class="phone-signin">
                <div class="signin-cover"></div>
                <div class="signin-list">
                        <a style="text-decoration: none;">                      
                            <button class="btn icon-btn btn-block btn-large signup-login-form__btn-xl space-1 btn-wechat btn-block signup-login-form__btn-xl space-1 btn-large" name="button" type="submit" >
                                <span class="icon-container">
                                    <i class="icon icon-wechat"></i>
                                </span>
                                <span class="text-container">使用微信帐号登录</span>
                            </button>
                        </a>
                        <a>
                            <button class="btn icon-btn btn-block btn-large signup-login-form__btn-xl space-1 btn-alipay btn-block signup-login-form__btn-xl space-1 btn-large" name="button" type="submit">
                                <span class="icon-container">
                                    <i class="icon icon-alipay"></i>
                                </span>
                                <span class="text-container">使用支付宝登录</span>
                            </button>
                        </a>
                            <!-- <button class="btn icon-btn btn-block btn-large signup-login-form__btn-xl space-1 btn-phone btn-block signup-login-form__btn-xl space-1 btn-large" name="button" type="submit">
                                <span class="text-container">使用手机号登录</span>
                            </button> -->
                            <div class="signup-or-separator">
                                <span class="h6 signup-or-separator--text">或</span>
                                <hr>
                            </div>
                            <div class="phone-signup-form">
                                <form accept-charset="UTF-8" action="/create" class="signin-form-phone" data-action="Signip" id="user_login"
                                method="post">
                                <div class="phone-number-signup-form-fields">
                                    <div class="control-group space-1">
                                        <input type="tel" class="input-stem decorative-input input-mobile-signin" name="national_number"
                                        aria-label="手机号" placeholder="手机号" value="">
                                        <div class="phone-check-signin">请输入手机号</div>
                                    </div>
                                    <div class="control-group space-1">
                                        <input type="password" data-hook="user_password" name="password" aria-label="密码"
                                        placeholder="密码(6~12位数字和字母组成)" class="decorative-input inspectletIgnore input-password-signin">
                                        <div class="password-check-signin">请输入密码</div>
                                        <div>
                                            <input  type="button" id="btn-signin" 
                                            class="btn btn-primary btn-block signup-login-form__btn-xl btn-large finished-phone-signup-fields" value="登录"/>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
            </div>
</header>