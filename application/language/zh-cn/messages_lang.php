<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['task']='任务';
$lang['task_msg']='任务说明';
$lang['state']='状态';
$lang['percent_completed']='完成率';

$lang['task_appfolder']='创建目录';
$lang['task_table_tpl']='生成模板数据库文件';
$lang['task_db']='创建数据库';
$lang['task_config']='生成配置文件';




$lang['create_folder_and_unziping']='创建目录,解压';
$lang['get_tpl_sql']='生成模板SQL';
$lang['create_db_and_tables']='生成数据库及表格';
$lang['write_cfg_file']='写配置文件';

$lang['start']='开始';
$lang['end']='完成';


?>