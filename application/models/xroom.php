<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xroom  extends CI_Model {
    
     public function __construct()
    {

    }



    public function getPageSize(){
           return 3;
    }


    
 
     
    public function allrooms($cityid,$index,$pagesize)
    {


    	    $start=intval(  ($index - 1 )  * $pagesize);
          $sql="select  *  from   room_source  where cityid='$cityid' limit  $start   , $pagesize ";
          
          $rooms=$this->db->query($sql)->result_array();

          
          $pointer=1;        
          foreach ($rooms as $key => $one_room) {


              // debug($one_room['ownerid']);
              $hostdetail= $this->xhost->hostdetail( $one_room['ownerid']  );
              // debug($hostdetail );

              $rooms[$key]['citycategeory']= 'city_'.$cityid;
              $rooms[$key]['hosterimg']=$hostdetail['headimageurl'];


              $city_row=$this->db->query("select cityname from room_city_list where cityid='$cityid' ")->row_array();
              $cityname=$city_row['cityname'];

              $rooms[$key]['cityname']=$cityname ;
          
              $rooms[$key]['roompic']=$this->mainpic($one_room['pid']);
              $rooms[$key]['comment_info']=$this->countercomment($one_room['pid']);

              if( ($pointer==1)||($pointer==2)||($pointer==4)||($pointer==5) ){
                 $rooms[$key]['licss']= 'mb20 mr20';
              }else
              {
                 $rooms[$key]['licss']= 'mb20';
              }

              $pointer++;       
          }

		  return $rooms;
    }



     
    public function roomdetail($roomid)
    {
          $sql="select * from room_source where pid=$roomid";
          $row=$this->db->query($sql)->row_array();

          $row['roompic']=$this->mainpic($roomid);
          $row['piclist']=$this->allpic($roomid);
          $row['comment_info']=$this->countercomment($roomid);
          
          $rate=$this->getrank($roomid);
          $row['rank']=$rate;
          $cityid=$row['cityid'];

          $city_row=$this->db->query("select cityname from room_city_list where cityid='$cityid' ")->row_array();
          $cityname=$city_row['cityname'];
          $row['cityname']=$cityname;



          $hostid=$this->gethostidbyroom($roomid);    

          $hostdetail= $this->xhost->hostdetail($hostid);
          $row['hostdetail']=$hostdetail;
          return $row;
    }

    public function getrank($roomid){

          $sql = "select rank  from room_comments where roomid=$roomid";
          $row1=$this->db->query($sql)->result_array();
          
          $resu=0;
          foreach ($row1 as $row )
          {
               $resu+=$row['rank'];
              
          }
          $num =count($row1,0);

          if($num==0){
            return 0;
          }else
          {
            return $resu/$num;
          }

          
    }


    public function gethostidbyroom($roomid){
         $sql="select ownerid from room_source where pid=$roomid";
         $row=$this->db->query($sql)->row_array();
         $hostid=-1;
         
         if( $row && array_key_exists('ownerid', $row) ){
          $hostid= $row['ownerid'];
         }

         return $hostid;


    }


    public function getlockno($roomid){

         $roomdetail= $this->xroom->roomdetail($roomid); 
         $lockno=$roomdetail['lockdeviceid']; 
         return $lockno;
    }
    


    public function mainpic($roomid)
    {
 
          $sql=" select covername as  roompic   from room_source_pic  where sourceid=$roomid and root=1 limit 1";


          // echo $sql;
          $row=$this->db->query($sql)->row_array();
          
          if( empty($row) ){
            return  $this->defaultmainpic();
          }else
          {
            return   $row['roompic'];
          }
    }


  public function allpic($roomid)
    {

      $sql="select  covername  as  roompic from room_source_pic  where sourceid=$roomid" ;
      $row=$this->db->query($sql)->result_array();
      return $row;
    }

    
  public function countercomment($roomid)
    {

          $pagesize=$this->getPageSize();  
           
          $sql=" select  count(pid) as  commentcounter    from room_comments  where roomid=$roomid ";
          $row=$this->db->query($sql)->row_array();
          
          $totalpages=ceil($row['commentcounter']/$pagesize) ;
          
          return array(
            'totalpages' =>$totalpages,
            'comment_counter' =>$row['commentcounter']
            );
    }
    


  public function defaultmainpic()
    {
   
          return  $this->config->item('base_url')."/php/images/rooms/default/roomdefault.jpg";
    
    }
    

     
    
  public function getcomments($roomid,$pageno=1)
    {
           
           $pagesize=$this->getPageSize();  
           $start=intval(  ($pageno - 1 )  * $pagesize);
           
           $sql="select realname,  u.pid as userid,covername as avatr, c.pid ,roomid,   userid, c.createtime , content     from   room_comments c,
           room_user u  where roomid=$roomid and  c.userid=u.pid limit $start,$pagesize   ";
           //$sql="select  *  from   room_comments  where roomid=$roomid limit $start,$pagesize ";

           $comments=$this->db->query($sql)->result_array();
         
           return $comments;
    }



    public function check_one_room($roomid,$startdate,$enddate  ){


          $sql="select * from room_order_history where sourceid=$roomid   order by  pid desc" ;


          $all_orders=$this->db->query($sql)->result_array();
          
          foreach ($all_orders as $key => $one_order) {
           
             $o_start=strtotime($one_order['startdate']);
             $o_end=strtotime($one_order['enddate']);

             if ($o_start<=$startdate&&$startdate<=$o_end) {
               return false;
             } 
             if($o_start<=$enddate&&$enddate<=$o_end){
              return false;
             }
             if ($startdate<=$o_start&&$o_end<=$enddate) {
              return false;
             }
          } 
          return true;
    }

     
    public function getavailablerooms($location,$startdate,$enddate,$guest,$page){
      
      if($location == 'all'){
           $sql="select * from room_source where capacity>=$guest" ;
       }else{
        $sql = "select * from room_source where capacity>=$guest and cityid='$location'";
      }

       // get all rooms


      logtext($sql);

      $availablerooms=array();

      $all_rooms =$this->db->query($sql)->result_array();

        foreach ($all_rooms as $key => $one_room) {

             if(  $this->check_one_room( $one_room['pid'] , $startdate,$enddate  )  ){

                    $one_room['comment_counter']=20;




                    $hostdetail= $this->xhost->hostdetail( $one_room['ownerid']  );
              // debug($hostdetail );


                    $one_room['hosterimg']=$hostdetail['headimageurl'];



                    $one_room['roompic']=$this->mainpic($one_room['pid']);

                    $availablerooms[]=$one_room;
             }
        }
      return $availablerooms;
    }

  }
    
 


?>