 <?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Xorder extends CI_Model
{
    
    //酒店行业时间,第一下午2点到第二天12点.
    
    public function hoteltime($orderdetail)
    {
        
        $start = $orderdetail['startdate'];
        $end   = $orderdetail['enddate'];
        return array(
            'start' => date("Y-m-d H:i", strtotime($start) + 3600 * 14),
            'end' => date("Y-m-d H:i", strtotime($end) + 3600 * 12)
        );
    }
    
    
    
    function orderdetail($orderid)
    {
       
        $sql = "  select  room_order_history.* ,roomtitle,address,roomdesc
              from room_order_history, room_source 
              where sourceid=room_source.pid and room_order_history.pid=$orderid ";

        logtext($sql);      
        $orderdetail= $this->db->query($sql)->row_array();
        return $orderdetail;
    }


    function infoByseqno($seqno){
         
           $this->db->where('seqno',$seqno);
           $row= $this->db->get('room_order_history',1 )->row_array();
           
         
           $orderpid=$row['pid'];

           $order_detail=$this->orderdetail($orderpid);
           $order_detail=$this->fix_orderdetail($order_detail);


           //是否显示退款按钮, none 为不显示
           $order_detail['refundbutton']='none';  
           $order_detail['pay_info']=$this->orderpayinfo($orderpid);
           return $order_detail ;
   }
   


   function  trans_state_text($orderstate){


         $arr['paybutton']='block';  
         $arr['orderstate_text']='未知';  
         $arr['refundbutton']='none';  

         if(  $orderstate=='ini'){
                    $arr['paybutton']='block';  
                    $arr['orderstate_text']='未付款';  
                    $arr['refundbutton']='none';  
                            
           }


           if(  $orderstate =='cancel'){
                    $arr['paybutton']='none';  
                    $arr['orderstate_text']='已取消';  
                    $arr['refundbutton']='none'; 
           }


           if(  $orderstate =='success'){
                    $arr['paybutton']='none';  
                    $arr['orderstate_text']='成功';  
                    $arr['refundbutton']='none'; 
           }


           if(  $orderstate =='usend'){
                    $arr['paybutton']='none';  
                    $arr['orderstate_text']='入住成功';  
                    $arr['refundbutton']='none'; 
           }


          if(  $orderstate=='refunded')
           {

                    $arr['paybutton']='none';  
                    $arr['orderstate_text']='已经退款';  
                    $arr['refundbutton']='false'; 
           }

          return $arr;

   }


   function fix_orderdetail($orderdetail){


             $arr=$this->trans_state_text(  $orderdetail['orderstate']   );
             $orderdetail['orderstate_text']=$arr['orderstate_text'];
             $orderdetail['paybutton']=$arr['paybutton'];
             $orderdetail['buttons'] =$this->getbuttons($orderdetail['orderstate']);
             return $orderdetail;


   } 



   function getbuttons($state){
    
    
      if($state=='ini'){

          $buttons=array(
          array('btn_text'=>'付款','url'=>'webredir'),
          array('btn_text'=>'取消','url'=>'order/cancel')
          // array('btn_text'=>'退款','url'=>'order/refund')
        );
      }

      if($state=='success'){

          $buttons=array(
          array('btn_text'=>'重发密码','url'=>'order/resendpwd'),
          array('btn_text'=>'退订','url'=>'weixin/wxrefund'),
          array('btn_text'=>'评价','url'=>'evaluate')
          );
      }

      if($state=='usend'){

          $buttons=array(
          array('btn_text'=>'评价','url'=>'evaluate')
          );
      }

      if($state=='cancel'){
          $buttons=array();
      }

      if($state=='refunded'){
          $buttons=array();
      }



      return $buttons;
    
   }
   
    function getorderidbyseqno($seqno){
        $sql = "select  pid  from room_order_history where  seqno='$seqno' ";
        $orderdetail= $this->db->query($sql)->row_array();
        return $orderdetail['pid'];
    }
    

    function orderdetailbyseqno($seqno)
    {
        
        $sql = "  select  room_order_history.* ,roomtitle,address,roomdesc
              from room_order_history, room_source 
              where sourceid=room_source.pid and room_order_history.seqno='$seqno' ";
        return $this->db->query($sql)->row_array();
    }
    



    //订单付款情况
    function orderpayinfo($orderid)
    {
        $payinfo                 = array();
        $payinfo['paymethod']    = 'weixin';
        $payinfo['out_trade_no'] = 'sjdlfjaslfjsdljflsfjdl';
        $payinfo['paydate']      = '2017--9-1 12:33:33';
        return $payinfo;
    }
    
    
    
    function updatestate($orderid, $state)
    {
        $this->db->where('pid', $orderid);
        $this->db->update('room_order_history', array(
            'orderstate' => $state
        ));
    }
    
    
    function orderPostProcess($orderid, $orderstate)
    {
     
        $this->xorder->updatestate($orderid, $orderstate);
        $orderdetail = $this->xorder->orderdetail($orderid);

        $roomsourceid    = $orderdetail['sourceid'];
        $lockno      = $this->xroom->getlockno($roomsourceid);
        
        $userpid=$orderdetail['userid'];
        $userinfo=$this->xuser->guestdetail($userpid);
        $mobile      = $userinfo['mobile'];
        $hoteltime   = $this->hoteltime($orderdetail);
        
        $pwd         = randpwd();

        $msg         = $this->xmobile->get_tpl('featurepwd', array(
            'room_pwd' => $pwd,
            'start' => $hoteltime['start'],
            'end' => $hoteltime['end']
            
        ));


        
        $this->xmobile->send_text_sms($mobile, $msg,$orderid);
        
        //加临时密码
        $args = array(
            'lock_no' => $lockno,
            'password' => $pwd,
            'mobile' => $mobile
        );
        $this->xlocker->add_pwd($args);
    }

    function getroomprice($args){

        return 0.01;

        $roomid=$args['roomid'];

        $sql = "select price_day from room_source where pid=$roomid ";
        //计算订单的天数
        $startdate=strtotime($args['starttime']);
        $enddate=strtotime($args['endtime']);
        $days=round(($enddate-$startdate)/3600/24) ;
        $price=$this->db->query($sql)->row_array();

        return $price['price_day']*$days;

    }


}


?> 