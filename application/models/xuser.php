<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Xuser extends CI_Model {

	public function __construct() {

	}

	public function check_mobile($mobile) {

		$mobile = preg_replace(array("/^[0\+]86/", "/[ \-+()]/"), "", $mobile);

		if (strlen($mobile) > 0) {
			$mobile = preg_replace(array("/^[0\+]86/", "/[ \-+()]/si"), "", $mobile);
		}
		return preg_match("/^(\s?\d{2,4}[\-]?)?1[3|4|5|6|7|8]\d{9}$/", $mobile);
	}

	public function savepersoninfo($userinfo){


		$data     = array(
			'wxnickname'     => $userinfo['username']
			
		);
		if($userinfo['sex']!=''){
			$data['gender']=$userinfo['sex'];
		}
		$this->db->where('pid', $userinfo['userid']);
		$this->db->update('room_user', $data);


	}

	function setsession($user_info, $transaction_id) {

		if ($transaction_id) {
			session_id($transaction_id);
		}

		session_start();
		$ret['code'] = 0;
		$ret['sid']  = session_id();

		$_SESSION['name']   = $user_info['realname'];
		$_SESSION['mobile'] = $user_info['mobile'];
		$_SESSION['avatr']  = $user_info['covername'];
		$_SESSION['pid']    = $user_info['pid'];
		$ret['pid']         = $user_info['pid'];
		$ret['token_s']     = $this->xdes->javaDesEncrypt($ret['sid'], null);
		$ret['token_t']     = $this->xdes->javaDesEncrypt(time(), null);
		$ret['havemobile']  = $this->check_mobile($user_info['mobile']);//手机号是否绑定
		$ret['covername']   = $user_info['covername'];
		return $ret;

	}

	public function getuseridbymobile($mobile) {

		$sql = "select  pid  from room_user where mobile='$mobile' ";
		$row = $this->db->query($sql)->row_array();
		return $row['pid'];
	}

	public function getuseridbyunionid($unionid) {

		$sql = "select  pid  from room_user where wx_unionid='$unionid' ";
		$row = $this->db->query($sql)->row_array();
		return $row['pid'];
	}

	public function getrelatedidbyunionid($unionid) {
		//mobileuserid
		$sql = "select  mobileuserid  from room_user where wx_unionid='$unionid' ";
		$row = $this->db->query($sql)->row_array();
		return $row['mobileuserid'];
	}

	public function getmobilebyunionid($unionid) {

		$wx_userpid = $this->getuseridbyunionid($unionid);

		$mob = $this->getusermobile($wx_userpid);
		if ($mob) {
			return $mob;
		}

		$userid = $this->getrelatedidbyunionid($unionid);
		$mob    = $this->getusermobile($userid);
		if ($mob) {
			return $mob;
		}
	}

	public function getpairuidbyunionid($unionid) {

		$sql = "select  pid,mobileuserid    from room_user where wx_unionid='$unionid' ";

		$row          = $this->db->query($sql)->row_array();
		$pid          = $row['pid'];
		$mobileuserid = $row['mobileuserid'];
		if (intval($mobileuserid) > 0) {
			return $pid.",".$mobileuserid;
		} else {
			return $pid;
		}
	}

	public function getusermobile($userid) {

		$sql  = "select mobile from room_user where  pid=$userid ";
		$info = $this->db->query($sql)->row_array();
		if ($info) {
			return $info['mobile'];
		} else {
			return null;
		}
	}

	public function guestdetail($guestid) {

		$sql = "select  pid as id , realname as username, realname as nickname,wxnickname,realname,mobile,wx_unionid,wx_openid,covername,mobileuserid,
          covername   as headimageurl,1 as serverid,'valid' as estate,createtime,
          lastvisittime,0 as fastpaystate,realname as shownickname, 86 as  areacode,
          86 as otherareacode    from room_user where pid=$guestid";

		logtext($sql);

		$info = $this->db->query($sql)->row_array();
		if (strlen($info['mobile']) < 10) {
			if (intval($info['mobileuserid']) > 0) {
				$info['mobile'] = $this->xuser->getusermobile($info['mobileuserid']);
			}
		}

		logtext($info);

		// $info['username']=    ($info['username']) ;

		return $info;
	}

	public function save_double_user($userinfo) {

		if (!$userinfo) {
			die("userinfo not set");

		}

		if (!$userinfo['unionid']) {
			die("unionid not set");
		}

		$this->db->trans_begin();

		// 用户表 room_user_weixin
		$nickname = $userinfo['nickname'];
		$data     = array(
			'openid'     => $userinfo['openid'],
			'nickname'   => $nickname,
			'gender'     => $userinfo['sex'],
			'headimgurl' => $userinfo['headimgurl'],
			'unionid'    => $userinfo['unionid'],
		);

		$this->db->insert('room_user_weixin', $data);
		$dbmsg = $this->db->_error_message();

		// 用户表 room_user

		$userdata = array(
			'usertype'   => 'guest',
			'realname'   => $nickname,
			'wxnickname' => $nickname,
			'gender'     => $userinfo['sex'],
			'createtime' => date("Y-m-d H:i:s", time()),
			'covername'  => $userinfo['headimgurl'],
			'wx_openid'  => $userinfo['openid'],
			'wx_unionid' => $userinfo['unionid'],

		);

		$this->db->insert('room_user', $userdata);
		$insert_id  = $this->db->insert_id();
		$updatedata = array(
			'userid' => $insert_id,
		);

		$this->db->where('unionid', $userinfo['unionid']);
		$this->db->update('room_user_weixin', $updatedata);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}

}

?>