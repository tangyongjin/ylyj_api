 <?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Xlocker extends CI_Model
{
    
    
    
    private $app_secret = 'awAeIDyw'; //密钥
    
    private $channel_code = '18600089281'; //运营商代码,渠道代码
    private $password = 'netmeeting1559'; //密码
    
    private $pwd_len = 7; //密码长度
    
    
    
    
    
    // KDBOSS宽带计费测试系统
    // 10.6.4.35   朱房间锁
    // 10.9.0.72  果颖房间锁
    // 果颖手机号 18010036732 
    //18622464594  靳凯博电话
    // https://lm.huohetech.com/ 管理后台地址
    // private $channel_code = 'gj_4093_3863'; //运营商代码,渠道代码
    // private $password = 'richtech8830'; //密码
    // 如何看日志:    http://47.92.72.19/showlog/
    
    // 推送接收地址: http://47.92.72.19/receiver/guojia
    // 短信模板 
    /*
    [果加智能]欢迎入住,您的开门密码是******，
    有效期自2017年08月09日11:39:12至2017年08月10日11:39:12止，
    在门锁按键输入“密码+#”后，便可开门。感谢您使用果加互联网智能锁。
    */
    
    private $version = '1.0';
    private $page_size = 10;
    private $url = 'http://ops.huohetech.com:80/';
    
    function randomnumber($len = null)
    {
        
        // $digits = $this->pwd_len;
        
        if (is_null($len)) {
            $len = $this->pwd_len;
        }
        return str_pad(rand(0, pow(10, $len) - 1), $len, '0', STR_PAD_LEFT);
    }
    
    
    
    function buildheader($arr)
    {
        
        $ret      = array();
        $all_keys = array_keys($arr);
        
        foreach ($all_keys as $key => $one_key) {
            
            $value = $arr[$one_key];
            $str   = $one_key . ':' . $value;
            $ret[] = $str;
        }
        return $ret;
    }
    
    
    
    function test_1()
    {
        
        header("Access-Control-Allow-Origin: * ");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        
        
        $args = getargs();
        
        $args['return_code'] = 0;
        $args['return_msg']  = '开锁成功';
        jsonoutput($args);
        
    }
    
    
    
    function create_uuid($prefix = "")
    {
        $str  = md5(uniqid(mt_rand(), true));
        $uuid = substr($str, 0, 8);
        $uuid .= substr($str, 8, 4);
        $uuid .= substr($str, 12, 4);
        $uuid .= substr($str, 20, 12);
        return $prefix . $uuid;
    }
    
    
    
    function getEncryptPwd($password = null)
    {
        
        if (is_null($password)) {
            $password = $this->password;
        }
        $key     = $this->app_secret;
        $pwd_des = $this->javaDesEncrypt($password, $key);
        return $pwd_des;
        
        
    }
    
    
    function gettoken()
    {
        header("Content-type: text/html; charset=utf-8");
        
        $s_id       = $this->create_uuid();
        $version    = $this->version;
        $header_arr = array(
            'version' => $version,
            's_id' => $s_id
        );
        
        $url    = $this->url . '/login';
        $params = array(
            'account' => $this->channel_code,
            'password' => $this->getEncryptPwd()
        );
        
        
        
        $resp = $this->huoHePost($url, $header_arr, $params);
        // debug($resp);die;
        
        return array(
            'access_token' => $resp['data']['access_token'],
            's_id' => $s_id
        );
    }
    
    
    
    
    function getcommonheader()
    {
        
        $header            = $this->gettoken();
        $header['version'] = $this->version;
        return $header;
        
        
    }
    
    function nodelist()
    {
        $header_arr = $this->getcommonheader();
        $url        = $this->url . '/node/list';
        
        $params = array(
            'page_size' => $this->page_size,
            'current_page' => 1
        );
        
        $resp = $this->huoHePost($url, $header_arr, $params);
        debug($resp);
    }
    
    
    
    function nodeview()
    {
        
        $url        = $this->url . '/node/view';
        $header_arr = $this->getcommonheader();
        
        $params = array(
            'node_no' => '200.1.62.128'
        );
        
        $resp = $this->huoHePost($url, $header_arr, $params);
        debug($resp);
    }
    
    
    
    function locklist()
    {
        
        $url        = $this->url . '/lock/list';
        $header_arr = $this->getcommonheader();
        $params     = array(
            'page_size' => $this->page_size,
            'current_page' => 1
        );
        
        echo "参数: <br/>";
        debug(array(
            'header' => $header_arr,
            'params' => $params
            
        ));
        
        
        $resp = $this->huoHePost($url, $header_arr, $params);
        debug($resp);
    }
    
    
    
    
    function remote_open()
    {
        
        $url        = $this->url . '/lock/remote_open';
        $header_arr = $this->getcommonheader();
        $params     = array(
            'pwd_user_mobile' => '18010036732',
            'lock_no' => '10.6.4.35'
        );
        
        echo "参数: <br/>";
        debug(array(
            'header' => $header_arr,
            'params' => $params
        ));
        
        
        $resp = $this->huoHePost($url, $header_arr, $params);
        debug($resp);
    }
    
    
    
    
    
    function lockview()
    {
        
        $url = $this->url . '/lock/view';
        
        
        $header_arr = $this->getcommonheader();
        
        $lock_no = '10.9.0.72';
        
        $params = array(
            'lock_no' => $lock_no
        );
        
        $resp = $this->huoHePost($url, $header_arr, $params);
        debug($resp);
    }
    
    
    
    //查看功能密码
    function view_func_pwd()
    {
        
        $url        = $this->url . '/pwd/view_func';
        $header_arr = $this->getcommonheader();
        $lock_no    = '10.9.0.72';
        
        $params = array(
            'lock_no' => $lock_no
        );
        
        $resp = $this->huoHePost($url, $header_arr, $params);
        
        // $mcrypt_
        
        echo $resp['data']['pwd_text'];
        debug($resp);
    }
    
    
    
    
    
    function add_pwd($args)
    {
        
        
        $lock_no       = $args['lock_no'];
        $pwd_plaintext = $args['password'];
        $mobile        = $args['mobile'];
        
        
        $url        = $this->url . '/pwd/add';
        $header_arr = $this->getcommonheader();
        $pwd_text   = $this->javaDesEncrypt($pwd_plaintext, $this->app_secret);
        
        $dt_start = time() * 1000; //毫秒
        
        $dt_end = (time() + (24 * 60 * 60)) * 1000; //毫秒
        
        $params = array(
            'lock_no' => $lock_no,
            'pwd_text' => $pwd_text,
            'similarity_check' => true,
            'pwd_user_mobile' => $mobile,
            'pwd_user_name' => '高光',
            'description' => 'api',
            'extra' => 'api2',
            'valid_time_start' => $dt_start,
            'valid_time_end' => $dt_end
        );
        
        
        $header_arr['version'] = '1.1';
        $resp = $this->huoHePost($url, $header_arr, $params);
        
    }
    
    
    
    
    function huoHePost($url, array $header_arr, array $params)
    {
        
        $data = json_encode($params, JSON_UNESCAPED_UNICODE);
        
        $header = $this->buildheader($header_arr);
        
        $header[] = 'Content-Type: application/json; charset=utf-8';
        $header[] = 'Content-Length: ' . strlen($data);
        
        $ch      = curl_init();
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HEADER => false, // 启用时会将头文件的信息作为数据流输出
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, // 强制使用 HTTP/1.1
            CURLOPT_RETURNTRANSFER => true, //将curl_exec()获取的信息以字符串返回
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
        );
        
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        if ($response !== false) {
            curl_close($ch);
            return json_decode($response, true);
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new Exception("curl出错，错误码:{$error}");
        }
    }
    
    
    
    function javaDesEncrypt($dat, $key)
    {
        $block   = mcrypt_get_block_size(MCRYPT_DES, MCRYPT_MODE_ECB);
        $len     = strlen($dat);
        $padding = $block - ($len % $block);
        $dat .= str_repeat(chr($padding), $padding);
        return bin2hex(mcrypt_encrypt(MCRYPT_DES, $key, $dat, MCRYPT_MODE_ECB));
    }
    
    
    
    function javaDesDecrypt($dat, $key)
    {
        $str = hex2bin($dat);
        $str = mcrypt_decrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB);
        $pad = ord($str[($len = strlen($str)) - 1]);
        return substr($str, 0, strlen($str) - $pad);
    }
    
    
    
    
}



?> 