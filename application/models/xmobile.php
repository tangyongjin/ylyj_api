 <?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Xmobile extends CI_Model
{
    
    public function __construct()
    {
        
    }
    
    
    
    public function check_mobile_exists($mob)
    {
        
        $sql    = "select  *  from  room_user  where mobile=$mob and usertype='guest'";
        $query  = $this->db->query($sql);
        $exists = 0;
        if ($query->num_rows() > 0) {
            $exists = 1;
        }
        return $exists;
    }

    public function getmobile($pid)
    {
        
        $sql    = "select  * from  room_user  where pid=$pid ";
        $query  = $this->db->query($sql)->row_array();
       
        return $query;
    }

    public function save_pwd($pid,$pwd){

        $salt          = substr(uniqid(rand()), -6);
        $pwd_with_salt = md5(md5($pwd) . $salt);
        $data    =  array(
                'salt'=>$salt,
                'password' =>$pwd_with_salt
            );
        $this->db->where('pid', $pid);
        $this->db->update('room_user', $data);
        
        $error_code = $this->db->_error_number();
        if ($error_code == 0) {
            return 0;
        }
        if ($error_code !== 0) {
            return 1;
        }

    }
    
    public function check_verify_code($mob, $transactionid, $code)
    {
        
        $sql = "select * from room_mobile_code where transactionid='$transactionid' and mobile='$mob' and code='$code'";
        logtext($sql);
        $query  = $this->db->query($sql);
        $exists = 1;
        if ($query->num_rows() == 0) {
            $exists = 0;
        }
        return $exists;
    }
    
    
    public function get_tpl($action_type, $args)
    {
        
        if ($action_type == 'register') {
            $str = "【一路一居】您的验证码是%s";
            
            $msg = sprintf($str, $args['code']);
            return $msg;
        }
        
        if ($action_type == 'bind') {

            //【一路一居】尊敬的#weixin#，您正在绑定手机号,验证码是#code#,如果您要通过手机号登录,请在个人资料中修改密码
            // $str = "【一路一居】欢迎注册,您的验证码是:%s";

            $str ='【一路一居】尊敬的%s，您正在绑定手机号,验证码是%s,如果您要通过手机号登录,请在个人资料中修改密码';
            $msg = sprintf($str,$args['name'], $args['code']);
            return $msg;
        }
        
        if ($action_type == 'changepassword') {
            $str = "【一路一居】您的验证码是%s";
            $msg = sprintf($str, $args['code']);
            return $msg;
            
        }
        
        if ($action_type == 'featurepwd') {
            $str = "【一路一居】欢迎入住，您的开门密码是%s，有效期自%s至%s止，在门锁输密码后按#键，便可开门。";
            $msg = sprintf($str, $args['room_pwd'], $args['start'], $args['end']);
            return $msg;
        }
        
        
    }
    
    
    public function send_verify_code($mob, $transactionid, $code, $action_type,$name)
    {
        $msg = $this->get_tpl($action_type, array(
            'code' => $code,
            'name'=> $name
        ));
        $status = $this->send_text_sms($mob, $msg,null);
        $this->save_verify_code($mob, $transactionid, $code, $msg);
        
    }
    
    
    public function save_verify_code($mob, $transactionid, $code, $msg)
    {
        $tmp = date('Y-m-d H:i:s', time());
        $sql = "INSERT INTO room_mobile_code (transactionid,mobile,code,createtime,msg) VALUES ('$transactionid','$mob','$code', '$tmp','$msg')";
        logtext($sql);
        $query = $this->db->query($sql);
    }
    
    
    
    //所有发送的短信,都存 room_sms 表格
    public function send_text_sms($phone, $msg,$orderpid)
    {
        
        $url       = 'http://yunpian.com/v1/sms/send.json';
        $post_data = array(
            'apikey' => '0b05f5cadb3c0d3944594f1b3c96549c',
            'mobile' => $phone,
            'text' => $msg
        );
        
        $postdata = http_build_query($post_data);
        
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => $postdata,
                'timeout' => 15 * 60 // 超时时间（单位:s）  
            )
        );
        
        $context = stream_context_create($options);
        $result  = file_get_contents($url, false, $context);
   
        
        
        $tmp   = date('Y-m-d H:i:s', time());
    
        $row=array('mobile'=>$phone,
                   'createtime'=>$tmp,
                   'msg'=>$msg,
                   'sendresult'=>$result,
                   'orderpid'=>$orderpid
                   );

        $this->db->insert('room_sms',$row) ;

      

         // $sql   = "INSERT INTO room_sms (mobile,createtime,msg,sendresult) VALUES ('$phone','$tmp','$msg','$result')";
    
        // $query = $this->db->query($sql);
        return true;
    }
    
    
    public function register($mob, $pwd, $name)
    {
        $salt          = substr(uniqid(rand()), -6);
        $pwd_with_salt = md5(md5($pwd) . $salt);
        
        $defaultnoavatr='http://47.92.72.19/api/images/users/defaultnoavatr.png';

        $sql = "insert into room_user (usertype,realname,mobile,password,salt,covername) values ('guest','$name','$mob','$pwd_with_salt','$salt','$defaultnoavatr')";
        logtext($sql);
        $query      = $this->db->query($sql);
        $error_msg  = $this->db->_error_message();
        $error_code = $this->db->_error_number();
        if ($error_code == 0) {
            return 0;
        }
        if ($error_code !== 0) {
            return 1;
        }
    }
    
    
}
?> 