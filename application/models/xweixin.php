<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xweixin extends CI_Model {
    
    public function __construct(){

        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        
        $this->appid  = "wxcff17cde4c7b60f8";
        $this->appsec = "81d9a2a4e6a37bfd8e59d0952d93c629";
        
        
        $this->token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&";
        
        // 根据code取token
        
        $this->oath_token_url  = "https://api.weixin.qq.com/sns/oauth2/access_token?";
        $this->qrcode_get_url  = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=";
        $this->getuserinfo_url = 'https://api.weixin.qq.com/sns/userinfo?';
        
        
        /*以下为微信支付用*/
        require_once __DIR__ . "/phpqrcode.php";
        require_once APPPATH . "../weixin/pay/lib/WxPay.Api.php";
        require_once APPPATH . "../weixin/pay/" . "log.php";
        require_once APPPATH . "../weixin/pay/" . "WxPay.NativePay.php";
        
        parent::__construct();

    }


       public function getWxPayArgs($order){

        $wxpay_args = new WxPayUnifiedOrder();
        $wxpay_args->SetBody($order['roomtitle']);

        
        // 附加数据  attach    附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用,保存orderid


        $wxpay_args->SetAttach($order['pid']);
        $wxpay_args->SetDetail($order['address']);
        $rand         = WxPayApi::getNonceStr(6);
        $out_trade_no = WxPayConfig::MCHID . date("YmdHis") . '_' . $rand;
        $wxpay_args->SetOut_trade_no($out_trade_no);
        $wxpay_args->SetTotal_fee(100* $order['ordermoney'] );
        //注意时区问题,否则  $expdate 可能太短.
        $wxpay_args->SetTime_start(date("YmdHis"));
        $expdate = date("YmdHis", time() + 60 * 60 * 12);
        $wxpay_args->SetTime_expire($expdate);

        $wxpay_args->SetNotify_url("http://47.92.72.19/api/weixin/wxpay_notify");
        $wxpay_args->SetTrade_type("NATIVE");
        $wxpay_args->SetProduct_id($order['sourceid']);
        
        return $wxpay_args;    


      }


      //一个订单号,系统只生成一条微信付款记录.
      //PC端扫码支付信息
      public function save_qrcode_payinfo($wxpay_args,$result){

        
        
        unset($result['result_code']);
        unset($result['return_code']);
        unset($result['return_msg']);
        unset($result['sign']);
        unset($result['nonce_str']);

     
        $result['insert_date']=date("Y-m-d H:i:s", time());
        
        $result['attach']=   $wxpay_args->GetAttach();
        $result['orderid']=   $wxpay_args->GetAttach();
        $result['total_fee'] =$wxpay_args->GetTotal_fee();
        $result['out_trade_no']= $wxpay_args->GetOut_trade_no();
        $result['time_start'] =$wxpay_args->GetTime_start();
        $result['time_expire']=$wxpay_args->GetTime_expire();
        $result['product_id']=$wxpay_args->GetProduct_id();
        $result['room_order_serial']=$result['room_order_serial'];
        $result['spbill_create_ip']=$_SERVER['REMOTE_ADDR'];

        
          
        if  (db_not_exits('room_wx_order_pay', 'room_order_serial', $result['room_order_serial']  )){
             $this->db->insert('room_wx_order_pay',$result);
        }else{

              $this->db->where('room_order_serial',$result['room_order_serial']);
              $this->db->update('room_wx_order_pay', $result  );
        }  
   
        
   
            
      }


      public function update_payinfo($wx_return){

        $row                   = array();

        $row['bank_type']      = $wx_return['bank_type'];
        $row['fee_type']       = $wx_return['fee_type'];
        $row['cash_fee']       = $wx_return['cash_fee'];
        $row['time_end']       = $wx_return['time_end'];
        $row['is_subscribe']   = $wx_return['is_subscribe'];
        $row['openid']         = $wx_return['openid'];
        $row['total_fee']      = $wx_return['total_fee'];
        $row['result_code']    = $wx_return['result_code'];
        $row['return_code']    = $wx_return['return_code'];
        $row['transaction_id'] = $wx_return['transaction_id'];
        $row['notify_date']    = date("Y-m-d H:i:s");
        
        $this->db->where('out_trade_no', $wx_return['out_trade_no']);
        $this->db->update('room_wx_order_pay', $row);
        $sql = $this->db->last_query();
        logtext($sql);
       
      }


      public function paypic($order){
    
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $wxpay_args=$this->getWxPayArgs($order);
         
        $NativePay = new NativePay();
        $result = $NativePay->GetPayUrl($wxpay_args);
        $result['room_order_serial']=$order['seqno'];
    
        $this->save_qrcode_payinfo($wxpay_args,$result); 
   

          
        ob_clean(); //必须加上,否则有可能出不来图像
        header('Content-type: image/png');
        QRcode::png($result["code_url"] );
    }





    public function gettoken_by_code($auth_code)
    {
        /*
        TODO:
        要考虑 access_token是否过期
        */
        $appid      = $this->appid;
        $secret     = $this->appsec;
        $grant_type = 'authorization_code';
        $weixin_ret = file_get_contents($this->oath_token_url . "appid=$appid&secret=$secret&code=$auth_code&grant_type=authorization_code");
        $weixin_ret_arr = (array) (json_decode($weixin_ret));
        return $weixin_ret_arr;
    }
     

    
    //微信退款
 
 
    public  function  wxrefund($out_trade_no,$fee){

            header("Content-type: text/html; charset=utf-8");

            $total_fee =  $fee;
            $refund_fee = $fee;
            $input = new WxPayRefund();


            $input->SetOut_trade_no($out_trade_no);
            $input->SetTotal_fee($total_fee);
            $input->SetRefund_fee($refund_fee);
            $input->SetOut_refund_no(WxPayConfig::MCHID.date("YmdHis"));
            $input->SetOp_user_id(WxPayConfig::MCHID);
            return WxPayApi::refund($input);
    }
 
    
}

?>