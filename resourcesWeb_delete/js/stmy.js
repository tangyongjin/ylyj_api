(function () {
        var params = {};
        if (document) {
                params.domain = document.domain || '';
                params.url = document.URL || '';
                params.title = document.title || '';
                params.referrer = document.referrer || '';
        }
        params.c_sessionid = getCookie('JSESSIONID') || '';
        params.c_userid = getCookie('MAYIUID') || '';
        c_guestid = getCookie('GUESTID')
        if (c_guestid == null || c_guestid == "") {
                setCookie('GUESTID', getGuestId(), 356 * 50);
        }
        params.c_guestid = getCookie('GUESTID') || '';
        if (navigator) {
                params.language = navigator.language || '';
                params.cookieEnable = navigator.cookieEnabled || '';
                params.appCodeName = navigator.appCodeName || '';
        }
        if (_maq) {
                for (var i in _maq) {
                        switch (_maq[i][0]) {
                                case '_setAccount':
                                        params.account = _maq[i][1]+'_'+getGuestId();
                                        break;
                                default:
                                        break;
                        }
                }
        }

        var args = '';
        for (var i in params) {
                if (args != '') {
                        args += '&';
                }
                args += i + '=' + encodeURIComponent(params[i]);
        }
        var accessArgs  = '&actionType=access';
        var roomIds = document.getElementById("staRoomIds");
        if(roomIds!=null && roomIds.value!=''){
        	args = args + '&roomIds='+roomIds.value;
        }else{
        	args = args + '&roomIds=';
        }
        var img = new Image(1, 1);
        img.src = 'http://mysta.mayi.com/stmy.gif?' + args+accessArgs;
        
        $(document).on('click','.stclick',function(){
        	var clickArgs = '&actionType=click';
        	var clicktag = $(this).attr("clicktag");
        	if(clicktag==null){
        		clicktag = "";
        	}
        	clickArgs  = clickArgs +'&clicktag='+clicktag;
			var clickImg = new Image(1, 1);
        	clickImg.src = "http://mysta.mayi.com/stmy.gif?" + args+clickArgs;
        });
})();

function getGuestId() {
        return Math.floor(Math.random() * 100000).toString() + new Date().getTime().toString();
}

function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) {
                c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1) {
                c_value = null;
        }
        else {
                c_start = c_value.indexOf("=", c_start) + 1;
                var c_end = c_value.indexOf(";", c_start);
                if (c_end == -1) {
                        c_end = c_value.length;
                }
                c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
}