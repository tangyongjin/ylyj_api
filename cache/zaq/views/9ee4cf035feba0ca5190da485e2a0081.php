<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <title>订单列表</title>


    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
     <!--[if IE]>
      <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
      <![endif]-->
    
    <link href="http://www.yiluyiju.com/assets/css/googlefonts.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    
    <link href="http://www.yiluyiju.com/assets/css/essentials.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/layout.css" rel="stylesheet" type="text/css" />

    <link href="http://www.yiluyiju.com/assets/css/header-1.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
    <link href="http://www.yiluyiju.com/assets/css/reset.css" rel="stylesheet" type="text/css" />



    <link href="http://www.yiluyiju.com/assets/js/pickadate.js/lib/themes/default.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/js/pickadate.js/lib/themes/default.date.css" rel="stylesheet" type="text/css" />
    <link href="http://www.yiluyiju.com/assets/js/pickadate.js/lib/themes/default.time.css" rel="stylesheet" type="text/css" />



    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/jquery.min.js"></script>

    <script type="text/javascript" src="http://www.yiluyiju.com/assets/js/bootstrap.min.js"></script> 



  </head>
<body>

     
 <section>
        <div class="container">

          <div class="heading-title heading-line-single text-center">
            <h3>我的订单列表</h3>
          </div>
          
          <?php foreach ( $orders as $order ) : ?>

          <div class="row pricetable-container">
            <div class="col-md-4 col-sm-4 price-table" style="border: 1px solid #D0D6DF;">
              <h3><?php echo $order['roomtitle'] ; ?></h3>
              <p> 
                <?php echo $order['ordermoney'] ; ?>
                
              </p>
              <ul class="pricetable-items  borded">
                  <li class="list-group-item" style="font-size: 14px;">订单号:<?php echo $order['seqno'] ; ?></li>

                  <li class="list-group-item">创建时间:<?php echo $order['createtime'] ; ?></li>
                  
                  <li class="list-group-item">房间:<?php echo $order['roomtitle'] ; ?></li>
                  <li class="list-group-item">地址:<?php echo $order['address'] ; ?></li>
                  
                  <li class="list-group-item">价格:<?php echo $order['ordermoney'] ; ?>元</li>
                  <li class="list-group-item">订单状态:<?php echo $order['orderstate'] ; ?></li>

                 
               
              </ul>
              <a href="#" class="btn btn-primary  btn-sm">订单详情</a>
            </div>
          </div>
          <?php endforeach ; ?>
        </div>
      </section>
</body>
<html>

 